module.exports = {
  verbose: true,
  testMatch: ['**/src/**/tests/**/*.e2e.js'],
  globalSetup: './config/jest/setup.e2e.js',
  globalTeardown: './config/jest/teardown.e2e.js',
  testEnvironment: './config/jest/environment.e2e.js',
};
