const { vibe } = require('farso');

vibe.default('2', mock => {
  mock('search').reply([
    200,
    {
      numFound: 2,
      start: 0,
      facets: {
        Topic: {
          buckets: [
            { val: '0|Tourism trips#TOUR_TRIPS#', count: 2 },
            { val: '0|Industry#IND#', count: 2 },
          ],
        },
        Measure: {
          buckets: [
            { val: '0|GDP#GDP#', count: 2 },
            { val: '0|Investment#INV#', count: 2 },
          ],
        },
      },
      dataflows: [
        {
          id: 'OECD-staging:DF_INBOUND_TOURISM',
          datasourceId: 'OECD-staging',
          name: 'Inbound tourism',
          dataflowId: 'DF_INBOUND_TOURISM',
          description:
            'Inbound tourism comprises the activities of a non-resident visitor within the country of reference.',
          version: '1.0',
          agencyId: 'OECD.CFE',
          indexationDate: '2020-04-02T14:53:47.095Z',
        },
        {
          id: 'OECD-staging:DF_OUTBOUND_TOURISM',
          datasourceId: 'OECD-staging',
          name: 'Outbound tourism',
          dataflowId: 'DF_OUTBOUND_TOURISM',
          version: '1.0',
          agencyId: 'OECD.CFE',
          indexationDate: '2020-04-02T14:53:47.597Z',
        },
      ],
      highlighting: {
        'OECD-staging:DF_INBOUND_TOURISM': {
          Measure: ['0|<em>Investment</em>#INV#'],
          name: ['Inbound <em>tourism</em>'],
        },
        'OECD-staging:DF_OUTBOUND_TOURISM': {
          Measure: ['0|<em>Investment</em>#INV#'],
          name: ['Outbound <em>tourism</em>'],
        },
      },
    },
  ]);
});
