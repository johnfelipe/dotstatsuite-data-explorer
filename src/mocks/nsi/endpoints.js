const { join } = require('ramda');
const { endpoint } = require('farso');

const ids = (id = 'SNA_TABLE1') => ['OECD', id, '1.0'];
const s_ids = join('/');
const c_ids = join(',');

endpoint('sna:structure', { uri: `/rest/dataflow/${s_ids(ids())}`, method: 'get' });
endpoint('sna:data', { uri: `/rest/data/${c_ids(ids())}/:query`, method: 'get' });
endpoint('sna:data:constraint', {
  uri: `/rest/availableconstraint/${c_ids(ids())}/:query`,
  method: 'get',
});
endpoint('sna:metadata', { uri: `/rest/data/dataflow/${s_ids(ids())}/:query`, method: 'get' });
endpoint('sna:metadata:structure', {
  uri: `/rest/metadatastructure/${s_ids(ids('MSD_SNA_TABLE1'))}`,
  method: 'get',
});
