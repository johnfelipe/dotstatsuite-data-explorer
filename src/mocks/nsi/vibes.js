const { vibe } = require('farso');
const snaStructure = require('./mocks/sna-structure.json');
const snaData = require('./mocks/sna-data.json');
const snaContraint = require('./mocks/sna-constraint.json');
const snaMetadata = require('./mocks/sna-metadata.json');
const snaMetadataStructure = require('./mocks/sna-metadata.json');
const snaDataBig = require('./mocks/sna-data-big.json');

vibe.default('sna', mock => {
  mock('sna:structure').reply((_, res) => res.json(snaStructure));
  mock('sna:data').reply((_, res) => res.json(snaData));
  mock('sna:data:constraint').reply((_, res) => res.json(snaContraint));
  mock('sna:metadata').reply((_, res) => res.json(snaMetadata));
  mock('sna:metadata:structure').reply((_, res) => res.json(snaMetadataStructure));
});

vibe('sna:content-range', mock => {
  mock('sna:structure').reply((_, res) => res.json(snaStructure));
  mock('sna:data').reply((_, res) => {
    res.set('content-range', 'values 0-2499/10000');
    return res.json(snaDataBig);
  });
  mock('sna:data:constraint').reply((_, res) => res.json(snaContraint));
});
