const { vibe } = require('farso');
const tenants = require('./tenants.js');
const settings = require('./settings.js');

vibe.default('main', mock => {
  mock('tenants').reply((req, res) => res.json(tenants(process.env.NSI_URL)));
  mock('settings').reply((req, res) => res.json(settings(process.env.SFS_URL)));
});
