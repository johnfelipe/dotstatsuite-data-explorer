module.exports = function(nsiUrl) {
  return {
    oecd: {
      id: 'oecd',
      label: 'oecd',
      default: true,
      spaces: {
        'demo-release': {
          label: 'demo-release',
          hasRangeHeader: true,
          supportsReferencePartial: true,
          hasLastNObservations: false,
          url: nsiUrl,
        },
        'demo-design': {
          label: 'demo-design',
          hasRangeHeader: true,
          supportsReferencePartial: true,
          hasLastNObservations: true,
          url: nsiUrl,
        },
        hybrid: {
          label: 'hybrid',
          url: nsiUrl,
          urlv3: nsiUrl,
          hasRangeHeader: true,
          supportsReferencePartial: true,
          hasLastNObservations: false,
          headers: {
            data: {
              csv: 'application/vnd.sdmx.data+json;version=2.0.0',
              json: 'application/vnd.sdmx.data+json;version=2.0.0',
            },
          },
        },
      },
      datasources: {
        'ds-demo-release': {
          dataSpaceId: 'demo-release',
          indexed: true,
          dataqueries: [
            {
              version: '1.0',
              categorySchemeId: 'OECDCS1',
              agencyId: 'OECD',
            },
          ],
        },
      },
      scopes: {
        de: {
          type: 'de',
          label: 'de',
          spaces: ['demo-design', 'demo-release', 'hybrid'],
          datasources: ['ds-demo-design', 'ds-demo-release'],
        },
      },
    },
  };
};
