import { createSelector } from 'reselect';
import * as R from 'ramda';
import {
  getTerm,
  getConstraints as getRouterConstraints,
  getStart,
  getLocationState,
  getSearchResultNb,
} from './router';
import { search } from '../lib/settings';
import { getVisUrl } from '../utils/router';
import { rejectIrrelevantFacets } from '../lib/search';
import { getSelectedValuesWithPath } from '../utils/used-filter';
//------------------------------------------------------------------------------------------------#0
export const getSearch = R.prop('search');

//------------------------------------------------------------------------------------------------#1
export const getConfig = createSelector(getSearch, R.prop('config'));
export const getFacets = createSelector(getSearch, R.propOr([], 'facets'));
export const getRows = createSelector(getSearch, R.prop('rows'));
export const getExternalResources = createSelector(getSearch, R.prop('externalResources'));

//------------------------------------------------------------------------------------------------#2
export const getConfigFacets = createSelector(getConfig, R.propOr([], 'facets'));

export const getHasNoSearchParams = createSelector(
  getTerm,
  getRouterConstraints,
  (term, constraints) =>
    R.and(R.anyPass([R.isEmpty, R.isNil])(term), R.anyPass([R.isEmpty, R.isNil])(constraints)),
);

export const getConstraints = createSelector(
  getFacets,
  R.pipe(R.filter(R.pipe(R.prop('count'), R.flip(R.gt)(0))), getSelectedValuesWithPath),
);

export const getDataflows = createSelector(
  getSearch,
  getLocationState,
  ({ dataflows = [] }, state) =>
    R.map(dataflow => R.assoc('url', getVisUrl(state, dataflow), dataflow))(dataflows),
);

export const getCurrentRows = createSelector(
  getRows,
  getDataflows,
  // in case of an undefined rows by configs, doesn't work for the last page ...
  (rows, dataflows) => (R.isNil(rows) ? R.length(dataflows) : rows),
);

//------------------------------------------------------------------------------------------------#3
export const getResultsFacets = createSelector(getFacets, getDataflows, (facets, dataflows) => {
  const pinnedFacetIds = R.propOr([], 'pinnedFacetIds', search);
  const excludedFacetIds = R.propOr([], 'excludedFacetIds', search);
  return R.pipe(
    rejectIrrelevantFacets({ count: R.length(dataflows) }),
    R.reduce(
      ([pinned, included], facet) => {
        if (R.includes(facet.id, excludedFacetIds)) return [pinned, included];
        const index = R.indexOf(facet.id, pinnedFacetIds);
        if (R.equals(index, -1)) return [pinned, [...included, facet]];
        return [[...pinned, { ...facet, isPinned: true, index }], included];
      },
      [[], []],
    ),
    ([pinned, included]) => R.concat(R.sortBy(R.prop('index'), pinned), included),
  )(facets);
});

export const getPages = createSelector(
  getCurrentRows,
  getSearchResultNb,
  (rows, searchResultNb) => {
    if (R.isNil(rows)) return;
    return parseInt(searchResultNb / rows) + (searchResultNb % rows !== 0 ? 1 : 0);
  },
);

export const getPage = createSelector(getStart, getCurrentRows, (start, rows) => {
  if (R.isNil(rows)) return;
  return start / rows + 1;
});
