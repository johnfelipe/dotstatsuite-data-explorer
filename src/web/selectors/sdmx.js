import { createSelector } from 'reselect';
import * as R from 'ramda';
import * as SDMXJS from '@sis-cc/dotstatsuite-sdmxjs';
import Set from 'es6-set';
import {
  sdmxFormat,
  getFrequencies,
  parseDateFromSdmxPeriod,
  getFrequencyFromSdmxPeriod,
  getAjustedDate,
} from '../lib/sdmx/frequency';
import { getDateInTheRange } from '../utils/date';
import {
  getDatasource as getSettingsDatasource,
  sdmxRange,
  locales,
  sdmxPeriodBoundaries,
} from '../lib/settings';
import {
  getLastNObservations,
  getLocale,
  getPeriod as getRouterPeriod,
  getHasDataAvailability,
  getDataquery,
  getDataflow,
  getDisplay,
} from './router';
import {
  dataFileRequestArgsWrapper,
  getOnlyHasDataDimensions,
  rawDataRequestArgsWrapper,
} from '../lib/sdmx';
import { cleanDeadBranches } from '../utils/clean-dead-branches';
import { getSelectedValuesWithPath } from '../utils/used-filter';
import { getMultiHierarchicalFilters } from '../lib/sdmx/hierarchical-codelist';

//------------------------------------------------------------------------------------------------#0
const getSdmx = R.prop('sdmx');

//------------------------------------------------------------------------------------------------#1
export const getDimensions = createSelector(getSdmx, R.propOr([], 'dimensions'));
export const getAttributes = createSelector(getSdmx, R.propOr([], 'attributes'));
export const getData = createSelector(getSdmx, R.prop('data'));
export const getDataRange = createSelector(getSdmx, R.prop('range'));
export const getTimePeriodArtefact = createSelector(getSdmx, R.prop('timePeriodArtefact'));
export const getExternalResources = createSelector(getSdmx, R.prop('externalResources'));
export const getExternalReference = createSelector(getSdmx, R.prop('externalReference'));
export const getDataRequestSize = createSelector(getSdmx, R.prop('dataRequestSize'));
export const getConstraints = createSelector(getSdmx, R.prop('constraints'));
export const getHierarchySchemes = createSelector(getSdmx, R.propOr([], 'hierarchySchemes'));
export const getObservationsCount = createSelector(getSdmx, R.prop('observationsCount'));
export const getDataflowDescription = createSelector(getSdmx, R.prop('description'));
export const getDatasetAttributes = createSelector(getSdmx, R.prop('datasetAttributes'));
export const getDataflowName = createSelector(getSdmx, ({ name, data }) =>
  R.isNil(data) ? name : R.path(['structure', 'name'], data),
);
export const getHierarchies = createSelector(getSdmx, R.prop('hierarchies'));

//------------------------------------------------------------------------------------------------#2
// available constraints come from availableContraints request  theses contraints change relatively to the selection.
export const getAvailableConstraints = createSelector(getConstraints, R.prop('available'));
// actual contraints come from structure request theses contraints are fixed.
export const getActualConstraints = createSelector(getConstraints, R.prop('actual'));
export const getValidFrom = createSelector(getConstraints, R.prop('validFrom'));

export const getFrequencyArtefact = createSelector(
  getDimensions,
  getAttributes,
  (dimensions = [], attributes = []) => SDMXJS.getFrequencyArtefact({ dimensions, attributes }),
);

export const getDatasource = createSelector(
  getDataflow,
  getExternalReference,
  (dataflow, externalReference) =>
    R.isNil(externalReference)
      ? getSettingsDatasource(R.prop('datasourceId', dataflow))
      : externalReference.datasource,
);

export const getRawStructureRequestArgs = createSelector(
  getDataflow,
  getLocale,
  (dataflow, locale) => ({
    datasource: getSettingsDatasource(R.prop('datasourceId', dataflow)),
    identifiers: {
      code: R.prop('dataflowId', dataflow),
      ...R.pick(['agencyId', 'version'], dataflow),
    },
    locale,
  }),
);
//------------------------------------------------------------------------------------------------#3
export const getFrequency = createSelector(
  getRouterPeriod,
  getFrequencyArtefact,
  getDataquery,
  (sdmxPeriod, frequencyArtefact, dataquery) =>
    R.pipe(
      R.when(R.isNil, R.always('')),
      R.split('.'),
      R.view(R.lensIndex(R.prop('index')(frequencyArtefact))),
      R.ifElse(
        R.either(R.isEmpty, R.isNil),
        R.always(getFrequencyFromSdmxPeriod(sdmxPeriod)),
        R.identity,
      ),
    )(dataquery),
);

export const getStructureRequestArgs = createSelector(getRawStructureRequestArgs, args =>
  SDMXJS.getRequestArgs({ ...args, type: 'dataflow', withPartialReferences: true }),
);

export const getDimensionsWithDataQuerySelection = createSelector(
  getDimensions,
  getDataquery,
  R.pipe(
    R.useWith(
      (filters, dataquery) =>
        R.addIndex(R.map)((filter, index) => {
          if (R.isEmpty(filter)) return filter;
          if (R.pipe(R.nth(index), R.anyPass([R.isEmpty, R.isNil]))(dataquery)) return filter;
          const valueIdsSet = new Set(R.pipe(R.nth(index), R.split('+'))(dataquery));
          return R.over(
            R.lensProp('values'),
            R.map(
              R.ifElse(({ id }) => valueIdsSet.has(id), R.assoc('isSelected', true), R.identity),
            ),
            filter,
          );
        }, filters),
      [R.identity, R.ifElse(R.isNil, R.always([]), R.split('.'))],
    ),
    R.reject(
      R.anyPass([
        R.pipe(R.prop('display'), R.not),
        SDMXJS.isFrequencyDimension,
        R.pipe(R.prop('values'), R.length, R.gte(1)),
      ]),
    ),
  ),
);

//------------------------------------------------------------------------------------------------#4
export const getFilters = createSelector(
  getHasDataAvailability,
  getDimensionsWithDataQuerySelection,
  getAvailableConstraints,
  getHierarchies,
  (hasDataAvailability, filters, availableConstraints, hierarchies) =>
    R.pipe(
      dimensions => SDMXJS.constrainDimensions(dimensions, availableConstraints, 'isEnabled'),
      dimensions => getMultiHierarchicalFilters(dimensions, hierarchies),
      R.when(
        R.always(hasDataAvailability),
        R.pipe(R.map(R.over(R.lensProp('values'), cleanDeadBranches))),
      ),
    )(filters),
);

export const getAvailableFrequencies = createSelector(
  getFrequencyArtefact,
  getHasDataAvailability,
  (frequencyArtefact = {}, hasDataAvailability) => {
    if (R.propEq('display', false)(frequencyArtefact)) return [];

    return R.pipe(
      R.ifElse(
        R.always(hasDataAvailability),
        R.pipe(R.of, getOnlyHasDataDimensions, R.head),
        R.identity,
      ),
      getFrequencies,
    )(frequencyArtefact);
  },
);

export const getDatesBoundaries = createSelector(
  getTimePeriodArtefact,
  getFrequency,
  getHasDataAvailability,
  (artefact, frequency, hasDataAvailability) => {
    if (R.isNil(artefact)) {
      return null;
    }
    const boundaries = hasDataAvailability
      ? R.propOr([], 'timePeriodBoundaries', artefact)
      : sdmxPeriodBoundaries;
    return R.map(getAjustedDate(frequency))(boundaries);
  },
);

//------------------------------------------------------------------------------------------------#5
export const getSelection = createSelector(
  getFilters,
  R.pipe(
    R.reject(R.pipe(R.propOr([], 'values'), R.length, R.gt(2))),
    getSelectedValuesWithPath,
    R.filter(R.pipe(R.prop('values'), R.length, R.flip(R.gt)(0))),
  ),
);

export const getPeriod = createSelector(
  getRouterPeriod,
  getFrequency,
  getDatesBoundaries,
  (sdmxPeriod, frequency, boundaries) => {
    if (R.isNil(sdmxPeriod)) return [undefined, undefined];
    const period = parseDateFromSdmxPeriod(frequency, sdmxPeriod);
    return R.ifElse(
      R.pipe(R.length, R.equals(2)),
      R.map(getDateInTheRange(boundaries)),
      R.always(R.map(getDateInTheRange(boundaries))(sdmxPeriod)),
    )(period);
  },
);

export const getTimeFormats = createSelector(getLocale, locale => {
  return R.mergeRight(sdmxFormat, { M: R.pathOr('YYYY MMM', [locale, 'timeFormat'], locales) });
});

//------------------------------------------------------------------------------------------------#6
export const getDataRequestParams = createSelector(
  getRouterPeriod,
  getLastNObservations,
  (period, lastNObservations) => {
    if (R.isNil(period)) return null;
    return {
      startPeriod: R.head(period),
      endPeriod: R.last(period),
      lastNObservations,
    };
  },
);

export const getDataRequestRange = createSelector(getDataRequestSize, size =>
  R.isNil(size) || size <= 0 ? sdmxRange : [0, size - 1],
);

export const getRefinedDataRange = createSelector(getData, getDataRange, (data, range) => {
  if (!R.isNil(range) && !R.isEmpty(range)) {
    return range;
  }
  const observations = R.pathOr({}, ['dataSets', 0, 'observations'], data);
  const count = R.length(R.values(observations));
  return { count, total: count };
});

export const getRawDataRequestArgs = createSelector(
  getRawStructureRequestArgs,
  getDataquery,
  getDataRequestParams,
  getExternalReference,
  getDataRequestRange,
  rawDataRequestArgsWrapper,
);

export const getDataRequestArgs = createSelector(getRawDataRequestArgs, args =>
  SDMXJS.getRequestArgs({ ...args, type: 'data' }),
);

export const getDataFileRequestArgs = isFull =>
  createSelector(getRawDataRequestArgs, getDisplay, dataFileRequestArgsWrapper(isFull));

export const getDataSourceHeaders = createSelector(getDataRequestArgs, R.prop('headers'));

export const getStructureUrl = createSelector(
  getRawStructureRequestArgs,
  getExternalReference,
  (args, externalReference) =>
    SDMXJS.getSDMXUrl({
      ...R.when(R.always(R.not(R.isNil(externalReference))), R.mergeLeft(externalReference))(args),
      type: 'dataflow',
    }),
);

export const getDataUrl = ({ agnostic }) =>
  createSelector(getRawDataRequestArgs, args =>
    SDMXJS.getSDMXUrl({ ...args, agnostic, type: 'data' }),
  );

export const getAvailableConstraintsArgs = createSelector(getRawDataRequestArgs, args =>
  R.pipe(
    R.over(R.lensProp('params'), params => {
      if (R.isNil(params)) return params;
      return R.pick(['endPeriod', 'startPeriod'], params);
    }),
    SDMXJS.getRequestArgs,
    R.over(R.lensProp('params'), R.assoc('mode', 'available')),
  )({
    ...args,
    withReferences: false,
    type: 'availableconstraint',
  }),
);
