import { createSelector } from 'reselect';
import * as R from 'ramda';
import { getRequestArgs, updateDataquery } from '@sis-cc/dotstatsuite-sdmxjs';
import {
  getMicrodataConstraints,
  getViewer,
  getDataquery as getRouterDataquery,
  getDisplay,
} from './router';
import {
  getRawStructureRequestArgs,
  getDataRequestParams,
  getExternalReference,
  getDimensions,
  getDimensionsWithDataQuerySelection,
  getTimePeriodArtefact,
  getDataRequestRange,
} from './sdmx';
import { getDescendants } from '../lib/sdmx/microdata';
import { dataFileRequestArgsWrapper, rawDataRequestArgsWrapper } from '../lib/sdmx';
import { MICRODATA } from '../utils/constants';

const getState = R.prop('microdata');

export const getDimensionId = createSelector(getState, R.prop('dimensionId'));
export const getData = createSelector(getState, R.prop('data'));
export const getHasMicrodata = createSelector(getDimensionId, R.pipe(R.isNil, R.not));
export const getHasMicrodataData = createSelector(getData, R.pipe(R.isNil, R.not));
export const getIsMicrodata = createSelector(getViewer, R.equals(MICRODATA));
export const getRange = createSelector(getState, R.propOr({}, 'range'));

const hasNotMany = R.pipe(R.length, R.gte(1));
export const getDataquery = createSelector(
  getDimensionId,
  getDimensions,
  getRouterDataquery,
  getMicrodataConstraints,
  getDimensionsWithDataQuerySelection,
  (dimensionId, dimensions, routerDataquery, constraints, dimensionsWithSelection) => {
    const _constraints = R.mergeRight(
      R.reduce(
        (memo, { id, values }) => {
          if (hasNotMany(values)) return memo;
          if (R.includes(id, R.keys(constraints))) return memo;
          const selectedValues = R.filter(R.propEq('isSelected', true), values);
          if (R.isEmpty(selectedValues)) return memo;
          const familyIds = R.reduce(
            (ids, value) => R.concat(ids, getDescendants(R.prop('id', value), values)),
            [],
            selectedValues,
          );
          if (hasNotMany(familyIds)) return memo;
          return R.assoc(id, familyIds, memo);
        },
        {},
        dimensionsWithSelection,
      ),
      R.reduce(
        (memo, [dimId, valId]) =>
          R.assoc(
            dimId,
            getDescendants(
              valId,
              R.pipe(R.find(R.propEq('id', dimId)), R.prop('values'))(dimensions),
            ),
            memo,
          ),
        {},
        R.toPairs(constraints),
      ),
    );
    const _dataquery = updateDataquery(
      dimensions,
      routerDataquery,
      R.pipe(R.map(R.always([])), R.assoc(dimensionId, ['DD', '_T']))(_constraints),
    );
    return updateDataquery(dimensions, _dataquery, _constraints);
  },
);

const getMicrodataRequestParams = createSelector(
  getDataRequestParams,
  getMicrodataConstraints,
  getTimePeriodArtefact,
  (defaultParams, constraints, timeArtefact) => {
    const timeConstraint = R.prop(timeArtefact.id, constraints);

    if (R.isNil(timeConstraint)) {
      return defaultParams;
    }

    return { startPeriod: timeConstraint, endPeriod: timeConstraint };
  },
);

export const getRawMicrodataRequestArgs = createSelector(
  getRawStructureRequestArgs,
  getDataquery,
  getMicrodataRequestParams,
  getExternalReference,
  getDataRequestRange,
  rawDataRequestArgsWrapper,
);

export const getMicrodataRequestArgs = createSelector(getRawMicrodataRequestArgs, args => {
  /*
    We wish to separate microdata request headers customization from regular data request headers customization,
    (don't want to spread hybrid format for metadata avaibility)
    but microdata being a data request, sdmxjs will look for data entry in headers customization,
    so here microdata is plug into data 
  */
  const refined = R.over(R.lensPath(['datasource', 'headers']), customHeaders => {
    const customMicrodataHeaders = R.propOr({}, 'microdata', customHeaders);
    return R.assoc('data', customMicrodataHeaders, customHeaders);
  })(args);
  return getRequestArgs({ ...refined, type: 'data' });
});

export const getMicrodataFileRequestArgs = isFull =>
  createSelector(getRawMicrodataRequestArgs, getDisplay, dataFileRequestArgsWrapper(isFull));
