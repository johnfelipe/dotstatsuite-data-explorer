import * as search from '../search';
import { always, times } from 'ramda';

jest.mock('../../lib/settings', () => ({
  search: { pinnedFacetIds: ['datasourceId'] },
  i18n: { localeId: 'en' },
}));
const dataflows = times(always({ id: 1 }), 10);
const state = search => ({ search: { ...search, dataflows } });

describe('search selectors', () => {
  it('should handle datasource id pinned if relevant', () => {
    const facets = [{ id: 'datasourceId', values: [{ id: '1', count: 5 }] }];
    expect(search.getResultsFacets(state({ facets }))).toEqual([
      { id: 'datasourceId', isPinned: true, index: 0, values: [{ id: '1', count: 5 }] },
    ]);
  });
});
