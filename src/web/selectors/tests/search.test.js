import * as R from 'ramda';
import * as search from '../search';

jest.mock('../../lib/settings', () => ({
  search: { pinnedFacetIds: ['pinned'], excludedFacetIds: ['excluded'] },
  i18n: { localeId: 'en' },
}));

const dataflows = R.times(R.always({ id: 1 }), 10);
const state = search => ({ search: { ...search, dataflows } });

describe('search selectors', () => {
  it('should handle no facet', () => {
    expect(search.getResultsFacets(state())).toEqual([]);
  });

  it('should handle pinnedFacetIds', () => {
    const facets = [
      { id: 'country', values: [{ id: '1', count: 1 }] },
      { id: 'pinned', values: [{ id: '2', count: 1 }] },
    ];
    expect(search.getResultsFacets(state({ facets }))).toEqual([
      { id: 'pinned', isPinned: true, index: 0, values: [{ id: '2', count: 1 }] },
      { id: 'country', values: [{ id: '1', count: 1 }] },
    ]);
  });

  it('should handle irrelevant facets even pinned', () => {
    const facets = [
      {
        id: 'relevant1',
        values: [
          { id: '1', count: 10 },
          { id: '2', count: 4 },
        ],
      },
      { id: 'relevant2', values: [{ id: '2', count: 7 }] },
      { id: 'pinned', values: [{ id: '2', count: 10 }] },
      {
        id: 'irrelevant1',
        values: [
          { id: '2', count: 10 },
          { id: '3', count: 10 },
        ],
      },
      { id: 'irrelevant2', values: [{ id: '2', count: 10 }] },
      {
        id: 'irrelevant3',
        values: [
          { id: 'a', count: null },
          { id: 'b', count: 0 },
          { id: 'c', count: 10 },
        ],
      },
    ];
    expect(search.getResultsFacets(state({ facets }))).toEqual([
      {
        id: 'relevant1',
        values: [
          { id: '1', count: 10 },
          { id: '2', count: 4 },
        ],
      },
      { id: 'relevant2', values: [{ id: '2', count: 7 }] },
    ]);
  });

  it('should handle excludedFacetIds', () => {
    const facets = [
      { id: 'excluded', values: [{ id: '1', count: 1 }] },
      { id: 'home', values: [{ id: '2', count: 1 }] },
    ];
    expect(search.getResultsFacets(state({ facets }))).toEqual([facets[1]]);
  });
});
