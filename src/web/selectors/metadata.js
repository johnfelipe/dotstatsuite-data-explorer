import { createSelector } from 'reselect';
import * as R from 'ramda';
import { getRequestArgs } from '@sis-cc/dotstatsuite-sdmxjs';
import { getDataflow, getDisplay, getLocale } from './router';
import { getData } from './sdmx';
import { getTablePreparedData, getDataDimensions } from './';
import { getDatasource } from '../lib/settings';

const getState = R.prop('metadata');

export const getIsOpen = createSelector(getState, R.prop('isOpen'));
export const getIsLoading = createSelector(getState, R.prop('isLoading'));

export const getCoordinates = createSelector(getState, R.prop('coordinates'));
export const getMetadataSeries = createSelector(getState, R.prop('metadataSeries'));
export const getIsDownloading = createSelector(getState, R.prop('isDownloading'));
export const getMSD = createSelector(getState, R.prop('msd'));

export const getMSDIdentifiers = createSelector(getData, data => {
  const msdDefinition = R.pipe(
    R.pathOr([], ['structure', 'annotations']),
    R.find(({ type }) => R.toLower(type) === 'metadata'),
    R.prop('title'),
  )(data);
  if (R.isNil(msdDefinition)) {
    return null;
  }
  const match = msdDefinition.match(/=([\w@_.]+):([\w@_.]+)\(([\d.]+)\)$/);
  if (R.isNil(match)) {
    return null;
  }
  const [agencyId, code, version] = R.tail(match);
  return { agencyId, code, version };
});

export const getMSDRequestArgs = createSelector(
  getDataflow,
  getLocale,
  getMSDIdentifiers,
  (dataflow, locale, identifiers) =>
    R.pipe(
      getRequestArgs,
      R.evolve({
        url: R.flip(R.concat)('/?references=conceptscheme'),
      }),
    )({
      datasource: getDatasource(R.prop('datasourceId', dataflow)),
      identifiers,
      locale,
      type: 'metadatastructure',
      withReferences: false,
    }),
);

const attribueValueDisplay = display => data => {
  if (display === 'code') {
    return R.prop('id', data);
  }
  if (display === 'both') {
    return `${R.prop('id', data)}: ${R.prop('name', data)}`;
  }
  return R.prop('name', data);
};

export const getHeaderSideProps = createSelector(
  getTablePreparedData(),
  getDataDimensions(),
  ({ advancedAttributes, metadataCoordinates }, dimensions) => {
    const headerCoordinates = R.reduce(
      (acc, dim) => {
        if (R.length(dim.values || []) > 1) {
          return acc;
        }
        return R.assoc(dim.id, R.path(['values', 0, 'id'], dim), acc);
      },
      {},
      dimensions,
    );
    const hasAdvancedAttributes = R.pipe(
      R.values,
      R.find(serie => {
        const mergedCoord = R.mergeLeft(serie.coordinates, headerCoordinates);
        return R.equals(mergedCoord, headerCoordinates);
      }),
      R.complement(R.isNil),
    )(advancedAttributes || {});
    const hasMetadata = R.pipe(
      R.find(coordinates => {
        const mergedCoord = R.mergeLeft(coordinates, headerCoordinates);
        return R.equals(mergedCoord, headerCoordinates);
      }),
      R.complement(R.isNil),
    )(metadataCoordinates || []);

    return hasMetadata || hasAdvancedAttributes
      ? {
          coordinates: headerCoordinates,
          hasMetadata,
          hasAdvancedAttributes,
        }
      : null;
  },
);

export const getAttributesSeries = createSelector(
  getCoordinates,
  getTablePreparedData(),
  getDisplay,
  (coordinates, { advancedAttributes }, display) => {
    return R.pipe(
      R.filter(serie => {
        const mergedCoord = R.mergeLeft(serie.coordinates, coordinates);
        return R.equals(mergedCoord, coordinates);
      }),
      R.map(serie => {
        return R.pipe(
          R.propOr({}, 'attributes'),
          R.values,
          R.map(attribute => ({
            id: attribute.id,
            label: attribueValueDisplay(display)(attribute),
            value: attribueValueDisplay(display)(attribute.value),
          })),
        )(serie);
      }),
    )(advancedAttributes);
  },
);
