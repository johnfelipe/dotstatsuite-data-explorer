import { createSelector } from 'reselect';
import * as R from 'ramda';
import { withIndex } from '../utils';
import {
  getLocale,
  getDataflow,
  getTableLayout,
  getDisplay,
  getTimeDimensionOrders,
} from './router';
import { getData, getDataflowName, getDataRequestSize, getHierarchies } from './sdmx';
import * as Settings from '../lib/settings';
import * as Layout from '../lib/layout';
import { rules, rules2 } from '@sis-cc/dotstatsuite-components';
import * as SDMXJS from '@sis-cc/dotstatsuite-sdmxjs';

//-----------------------------------------------------------------------------------------------vis
const getVisState = R.prop('vis');
export const getVisActionId = () => createSelector(getVisState, R.prop('actionId'));
export const getIsFull = () => createSelector(getVisState, R.prop('isFull'));
export const getIsOpeningFullscreen = createSelector(getVisState, R.prop('isOpeningFullscreen'));
export const getDataflowTitle = createSelector(getVisState, R.prop('title'));
export const getFilter = createSelector(getVisState, R.prop('filter'));
export const getShareLocale = () => createSelector(getLocale, R.flip(R.prop)(Settings.locales));

export const getVisTableData = () =>
  createSelector(
    getTimeDimensionOrders,
    getData,
    getTableLayout,
    (timeDimensionOrders, data, layout) => {
      const tableData = R.uncurryN(2, rules.getTableData)(timeDimensionOrders, data);
      const lastRowId = R.pipe(R.propOr([], 'rows'), R.last)(layout);
      return R.when(
        R.always(R.not(R.isNil(lastRowId))),
        R.over(R.lensPath(['dimensions', lastRowId]), SDMXJS.withFlatHierarchy),
      )(tableData);
    },
  );

export const getVisDimensionFormat = accessors =>
  createSelector(getDisplay, display => {
    return R.prop(display, {
      label: rules.dimensionValueDisplay('label', accessors),
      code: rules.dimensionValueDisplay('code', accessors),
      both: rules.dimensionValueDisplay('both', accessors),
    });
  });

export const getDataDimensions = () =>
  createSelector(
    [getData],
    R.pipe(
      R.pathOr([], ['structure', 'dimensions', 'observation']),
      R.addIndex(R.map)((dimension, index) =>
        R.pipe(
          R.assoc('index', index),
          R.set(R.lensProp('values'), withIndex(R.propOr([], 'values')(dimension))),
        )(dimension),
      ),
    ),
  );
export const getVisDataDimensions = () =>
  createSelector(
    getDataDimensions(),
    R.pipe(
      R.partition(R.pipe(R.propOr([], 'values'), R.length, R.flip(R.gt)(1))),
      R.converge(
        (many, one) => ({
          many: R.indexBy(R.prop('id'))(many),
          one: R.indexBy(R.prop('id'))(one),
        }),
        [R.head, R.last],
      ),
    ),
  );
export const getTableConfigDimensions = () =>
  createSelector(getVisDataDimensions(), ({ many, one }) =>
    R.mergeRight(Layout.oneFormat(one), Layout.manyFormat(many)),
  );

export const getVisTableConfigLayout = () =>
  createSelector([getTableLayout, getVisDataDimensions()], (visLayout = {}, { many, one }) => {
    const layout = R.mergeRight({ sections: [], rows: [], header: [] }, visLayout);
    const oneIds = R.keys(one);
    const dimensionsIds = R.concat(R.keys(many), oneIds);
    const getMissingIds = R.difference(dimensionsIds);
    const layoutIds = Layout.getValuesFlat(layout);
    if (Layout.isInvalid(dimensionsIds, layoutIds, layout))
      return Layout.getDefaultLayout(many, getMissingIds);

    const missingIds = getMissingIds(layoutIds);
    if (R.isEmpty(missingIds)) return Layout.adjustment(oneIds, layout);

    return Layout.getLayout(missingIds, oneIds)(layout);
  });

export const getVisDimensionLayout = createSelector(
  getTableConfigDimensions(),
  getVisTableConfigLayout(),
  (dimension, layout) => {
    if (R.isEmpty(dimension)) return {};
    return R.map(R.map(R.flip(R.prop)(dimension)), layout);
  },
);
export const getVisTableLayout = () =>
  createSelector([getVisTableConfigLayout(), getVisDataDimensions()], (layout, dimensions) =>
    Layout.compactLayout(dimensions)(layout),
  );

export const getVisChoroMap = createSelector(getVisState, R.prop('map'));
export const getVisIsLoadingMap = createSelector(getVisState, R.prop('isLoadingMap'));

export const getEndpoint = spaceId =>
  createSelector([getDataflow], ({ datasourceId } = {}) => {
    if (R.and(R.isNil(spaceId), R.isNil(datasourceId))) throw new Error('No datasource');
    return Settings.getDatasource(spaceId ? spaceId : datasourceId).url;
  });
export const getReferencePartial = () =>
  createSelector(getDataflow, ({ datasourceId } = {}) => {
    if (R.isNil(datasourceId)) throw new Error('No datasource');
    const datasource = Settings.getDatasource(datasourceId);
    return R.propEq('supportsReferencePartial', true, datasource) ? '&detail=referencepartial' : '';
  });
export const getFormatedDataflowQuery = ({ agencyId, code, version, joint, defaultVersion }) =>
  R.pipe(R.reject(R.isNil), R.join(joint))([agencyId, code, R.defaultTo(defaultVersion, version)]);
export const getDataflowQuery = (joint = '/', defaultVersion = 'latest', sdmxId) =>
  createSelector([getDataflow], ({ agencyId = '', code = '', version = '' } = {}) => {
    if (R.all(R.complement(R.isEmpty), [agencyId, code, version])) {
      return getFormatedDataflowQuery({
        agencyId,
        code,
        version,
        joint,
        defaultVersion,
      });
    }
    if (R.all(R.isNil, R.props(['agencyId', 'code', 'version'], sdmxId))) return undefined;
    return getFormatedDataflowQuery({ ...sdmxId, joint, defaultVersion });
  });

export const getCustomAttributes = createSelector(getData, data => {
  const tooltipAttributesIds = rules2.getDataflowTooltipAttributesIds(
    { data },
    Settings.customAttributes,
  );
  return R.merge(
    tooltipAttributesIds,
    R.pick(['prefscale', 'decimals'], Settings.customAttributes),
  );
});

export const getTablePreparedData = () =>
  createSelector(
    getData,
    getCustomAttributes,
    getHierarchies,
    (data, customAttributes, hierarchies) => {
      return rules.prepareData({ data }, customAttributes, Settings.units, { hierarchies });
    },
  );

export const getTableCellsLimit = createSelector(getDataRequestSize, size =>
  R.isNil(size) || size <= 0 ? Settings.cellsLimit : Math.floor(size * 1.2),
);

export const getIsIncreased = createSelector(getDataRequestSize, size => {
  const settingsSize = Settings.sdmxRange[1] - Settings.sdmxRange[0] + 1;
  return !R.isNil(size) && size > settingsSize;
});

export const getIsTimeInverted = createSelector(
  getTimeDimensionOrders,
  R.pipe(R.values, R.head, R.equals(true)),
);

export const getAvailableCharts = createSelector(getData, rules.getAvailableChartTypes);

export const getVisDataflow = createSelector(getDataflow, getDataflowName, (dataflow, name) => ({
  id: dataflow.dataflowId,
  name,
}));
