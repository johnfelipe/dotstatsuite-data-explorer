import React, { useState, Fragment, useEffect } from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import Typography from '@material-ui/core/Typography';
import { defineMessages, useIntl } from 'react-intl';
import { FormattedMessage } from '../../i18n';
import Form from './Form';
import { requestToken } from '../reducer';
import { getLog, getIsPending } from '../accessors';

export const expiredMessages = defineMessages({
  success: { id: 'de.share.expired.token.success' },
  email: { id: 'de.share.expired.label' },
});

const ExpiredToken = ({ email, state, dispatch }) => {
  const intl = useIntl();
  const isPending = getIsPending('postList')(state);
  const logList = getLog('postList', 'method')(state);

  const [isDisabled, setDisabled] = useState(isPending);
  const [notification, setNotification] = useState({});

  useEffect(() => {
    if (isPending) setDisabled(true);
  }, [isPending]);

  useEffect(() => {
    if (logList) {
      const isSuccess = logList?.log?.statusCode == 200;
      setNotification({
        open: true,
        severity: isSuccess ? 'success' : 'error',
        message: isSuccess
          ? intl.formatMessage(expiredMessages.success)
          : logList?.message || 'Network error',
      });
    }
  }, [logList]);

  return (
    <Fragment>
      <Typography component="label" variant="body2">
        <FormattedMessage id="de.share.expired.description" />
      </Typography>
      <Form
        email={email}
        action={email => dispatch(requestToken(email)(dispatch))}
        isDisabled={isDisabled}
        notification={notification}
        closeNotificationPanel={() => setNotification(R.set(R.lensProp('open'), false))}
        isPending={isPending}
        labels={{
          submit: <FormattedMessage id="de.share.expired.submit" />,
          email: intl.formatMessage(expiredMessages.email),
        }}
      />
    </Fragment>
  );
};

ExpiredToken.propTypes = {
  email: PropTypes.string,
  dispatch: PropTypes.func,
  state: PropTypes.object,
};

export default ExpiredToken;
