import React, { useState } from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import cx from 'classnames';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import DeleteIcon from '@material-ui/icons/Delete';
import GlobeIcon from '@material-ui/icons/Public';
import FacebookIcon from '@material-ui/icons/Facebook';
import LinkedInIcon from '@material-ui/icons/LinkedIn';
import TwitterIcon from '@material-ui/icons/Twitter';
import MailIcon from '@material-ui/icons/Mail';
import OpenInNewIcon from '@material-ui/icons/OpenInNew';
import { CopyContent as CopyContentIcon, Button } from '@sis-cc/dotstatsuite-visions';
import { useIntl } from 'react-intl';
import { getViewerLink, DATE_FORMAT, icons } from '../../constants';
import AlertDialog from '../AlertDialog';
import { useDialog } from '../useDialog';
import ViewerCell from './ViewerCell';
import Cell from './Cell';
import { requestDelete } from '../../reducer';
import { FormattedMessage } from '../../../i18n';
import Social from '../Social';

const useStyles = makeStyles(theme => ({
  root: {
    '& > *': {
      borderBottom: 'unset',
    },
  },
  cell: {
    whiteSpace: 'nowrap',
  },
  icon: {
    verticalAlign: 'middle',
    margin: `0 ${theme.spacing(0.5)}px`,
    cursor: 'pointer',
  },
  code: {
    border: `4px solid ${theme.palette.grey[100]}`,
    padding: theme.spacing(1),
    userSelect: 'all',
  },
  tag: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.highlight.hl2,
    color: theme.palette.getContrastText(theme.palette.highlight.hl2),
    padding: theme.spacing(0.5),
    borderRadius: '4px',
  },
  copyCode: {
    position: 'relative',
    float: 'right',
  },
  row: {
    backgroundColor: theme.palette.highlight.hl1,
  },
  cellChartIcon: {
    paddingRight: 0,
  },
}));

const Item = ({ title, value }) => (
  <Grid item>
    <Grid container direction="column">
      <Grid item>{title}</Grid>
      <Grid item>{value}</Grid>
    </Grid>
  </Grid>
);

Item.propTypes = {
  title: PropTypes.oneOfType([PropTypes.element, PropTypes.string]),
  value: PropTypes.oneOfType([PropTypes.element, PropTypes.string]),
};

const getContent = viewerUrl => `<iframe src="${viewerUrl}" style="border: none"; allowfullscreen="true">;
  <a rel="noopener noreferrer" href="${viewerUrl}" target="_blank">Dataflow</a>
</iframe>;
`;

const Row = ({ row, colLength, viewerId, token, dispatch, isUpSmall, isUpLarge }) => {
  const [open, setOpen] = useState(false);

  const classes = useStyles();
  const intl = useIntl();
  const Icon = R.propOr(GlobeIcon, R.path(['data', 'type'], row), icons);
  const id = R.prop('id', row);
  const title = R.path(['data', 'title'], row) || `[${R.path(['data', 'dataflowId'], row)}]`;
  const viewerUrl = getViewerLink(R.prop('confirmUrl', row), id);
  const iframeContent = getContent(viewerUrl);
  const { selectedId, isOpen, handleClose, handleOpen, handleDelete } = useDialog({
    id,
    action: props => dispatch(requestDelete(props)(dispatch)),
  });

  const deleteDialog = (
    <AlertDialog
      Icon={DeleteIcon}
      title={<FormattedMessage id="de.share.alert.delete.title" />}
      description={
        <FormattedMessage id="de.share.alert.delete.description" values={{ title, id }} />
      }
      handleOpen={handleOpen}
      handleClose={handleClose}
      open={isOpen}
    >
      <Button onClick={handleClose} color="primary">
        <FormattedMessage id="de.share.alert.delete.cancel" />
      </Button>
      <Button
        alternative="siscc"
        variant="contained"
        onClick={() => handleDelete({ token, id })}
        color="primary"
      >
        <FormattedMessage id="de.share.alert.delete.confirm" />
      </Button>
    </AlertDialog>
  );
  const createdAt = intl.formatDate(R.prop('createdAt', row), DATE_FORMAT);
  const lastAccessedAt = intl.formatDate(R.prop('lastAccessedAt', row), DATE_FORMAT);
  const expire = intl.formatDate(R.prop('expire', row), DATE_FORMAT);
  const links = <ViewerCell viewerUrl={viewerUrl} />;

  const socialList = [
    {
      id: 'newTab',
      Icon: OpenInNewIcon,
      link: viewerUrl,
    },
    {
      id: 'facebook',
      Icon: FacebookIcon,
      link: `https://www.facebook.com/sharer/sharer.php?u=${viewerUrl}`,
    },
    {
      id: 'linkedin',
      Icon: LinkedInIcon,
      link: `https://www.linkedin.com/sharing/share-offsite/?url=${viewerUrl}`,
    },
    {
      id: 'twitter',
      Icon: TwitterIcon,
      link: `https://twitter.com/intent/tweet?text=${title}&url=${viewerUrl}`,
    },
    {
      id: 'mail',
      Icon: MailIcon,
      link: `mailto:?subject=Shared visualisation: ${title}&body=${viewerUrl}`,
    },
  ];

  return (
    <React.Fragment>
      <TableRow className={cx(classes.root, { [classes.row]: selectedId === id })}>
        {isUpSmall && <Cell align="center">{id}</Cell>}
        <Cell align="right" className={classes.cellChartIcon}>
          <Icon className={classes.icon} />
        </Cell>
        <Cell align="left">
          {title}
          {viewerId === id && (
            <Typography className={classes.tag} component="span">
              <FormattedMessage id="de.share.tag.new.shared.object" />
            </Typography>
          )}
        </Cell>
        {isUpLarge && (
          <>
            <Cell align="left" className={classes.cell}>
              {createdAt}
            </Cell>
            <Cell align="left" className={classes.cell}>
              {lastAccessedAt}
            </Cell>
            <Cell align="left" className={classes.cell}>
              {expire}
            </Cell>
            <Cell align="left">{links}</Cell>
          </>
        )}
        <Cell align="left">
          <Social links={R.drop(isUpLarge ? 1 : 0, socialList)} viewerUrl={viewerUrl} />
        </Cell>
        {isUpLarge && <Cell align="center">{deleteDialog}</Cell>}
        <Cell align="center">
          <IconButton aria-label="expand row" size="small" onClick={() => setOpen(!open)}>
            {open ? (
              <KeyboardArrowUpIcon color="primary" />
            ) : (
              <KeyboardArrowDownIcon color="primary" />
            )}
          </IconButton>
        </Cell>
      </TableRow>
      <TableRow>
        <Cell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={colLength}>
          <Collapse in={open} timeout="auto" unmountOnExit>
            <Box margin={1}>
              <Grid container spacing={3}>
                {!isUpLarge && (
                  <>
                    <Item
                      title={<FormattedMessage id="de.share.table.col.actions" />}
                      value={deleteDialog}
                    />
                    <Item title={<FormattedMessage id="de.share.table.col.link" />} value={links} />
                    <Item
                      title={<FormattedMessage id="de.share.table.col.created" />}
                      value={createdAt}
                    />
                    <Item
                      title={<FormattedMessage id="de.share.table.col.lastViewed" />}
                      value={lastAccessedAt}
                    />
                    <Item
                      title={<FormattedMessage id="de.share.table.col.expireDate" />}
                      value={expire}
                    />
                    <Item
                      title={<FormattedMessage id="de.share.embed.description" />}
                      value={
                        <IconButton
                          size="small"
                          onClick={() => navigator.clipboard.writeText(iframeContent)}
                        >
                          <CopyContentIcon color="primary" fontSize="small" />
                        </IconButton>
                      }
                    />
                  </>
                )}
                {isUpLarge && (
                  <Grid item xs={12}>
                    <Typography variant="body1" gutterBottom component="div">
                      <FormattedMessage id="de.share.embed.description" />
                    </Typography>
                    <pre className={classes.code}>
                      <IconButton size="small" className={classes.copyCode}>
                        <CopyContentIcon
                          color="primary"
                          className={cx({ [classes.copyCode]: isUpLarge })}
                          fontSize="small"
                          onClick={() => navigator.clipboard.writeText(iframeContent)}
                        />
                      </IconButton>
                      {iframeContent}
                    </pre>
                  </Grid>
                )}
              </Grid>
            </Box>
          </Collapse>
        </Cell>
      </TableRow>
    </React.Fragment>
  );
};

Row.propTypes = {
  row: PropTypes.object,
  colLength: PropTypes.number,
  viewerId: PropTypes.string,
  token: PropTypes.string,
  dispatch: PropTypes.func,
  isUpSmall: PropTypes.bool,
  isUpLarge: PropTypes.bool,
};

export default Row;
