import React from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { NoData } from '@sis-cc/dotstatsuite-visions';
import { useTheme } from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import Row from './Row';
import Cell from './Cell';
import { FormattedMessage } from '../../../i18n';

const getDescendantList = list => R.reverse(R.sortBy(R.pipe(R.prop('id'), Number))(list));

// colLength is the number of columns + 1 hide in the collapse component (see Row)
const CollapsibleTable = ({
  viewerId,
  token,
  hasNodata,
  sharedObjects = [],
  dispatch,
  colLength = 11,
}) => {
  const theme = useTheme();
  const isUpSmall = useMediaQuery(theme.breakpoints.up('sm'));
  const isUpLarge = useMediaQuery(theme.breakpoints.up('lg'));

  return (
    <TableContainer>
      <Table>
        <TableHead>
          <TableRow>
            {isUpSmall && (
              <Cell align="center">
                <FormattedMessage id="de.share.table.col.id" />
              </Cell>
            )}
            <Cell />
            <Cell align="left">
              <FormattedMessage id="de.share.table.col.title" />
            </Cell>
            {isUpLarge && (
              <>
                <Cell align="left">
                  <FormattedMessage id="de.share.table.col.created" />
                </Cell>
                <Cell align="left">
                  <FormattedMessage id="de.share.table.col.lastViewed" />
                </Cell>
                <Cell align="left">
                  <FormattedMessage id="de.share.table.col.expireDate" />
                </Cell>
                <Cell align="left">
                  <FormattedMessage id="de.share.table.col.link" />
                </Cell>
              </>
            )}
            <Cell align="left">
              <FormattedMessage id="de.share.table.col.shareOptions" />
            </Cell>
            {isUpLarge && (
              <Cell align="center">
                <FormattedMessage id="de.share.table.col.actions" />
              </Cell>
            )}
            <Cell />
          </TableRow>
        </TableHead>
        <TableBody>
          {R.isEmpty(sharedObjects) || hasNodata ? (
            <TableRow>
              <Cell align="center" colSpan={colLength}>
                <NoData message={<FormattedMessage id="de.share.table.no.data" />} />
              </Cell>
            </TableRow>
          ) : (
            R.map(
              sharedObject => (
                <Row
                  key={sharedObject.id}
                  row={sharedObject}
                  viewerId={viewerId}
                  colLength={colLength}
                  token={token}
                  dispatch={dispatch}
                  isUpSmall={isUpSmall}
                  isUpLarge={isUpLarge}
                />
              ),
              getDescendantList(sharedObjects),
            )
          )}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

CollapsibleTable.propTypes = {
  viewerId: PropTypes.string,
  token: PropTypes.string,
  hasNodata: PropTypes.bool,
  sharedObjects: PropTypes.array,
  colLength: PropTypes.number,
  dispatch: PropTypes.func,
};

export default CollapsibleTable;
