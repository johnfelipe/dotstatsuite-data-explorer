import axios from 'axios';

export const testidSelector = testid => `[data-testid="${testid}"]`;
export const changeVibe = srv => vibe => axios.get(global._E2E_.makeUrl(`/_vibes_/${vibe}`, srv));
