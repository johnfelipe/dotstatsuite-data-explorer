import { changeVibe, testidSelector } from './utils';
import { length, prop } from 'ramda';

// if style is not found, expect fails
const expectHighlightStyleHelper = (style = {}) => {
  expect(prop('backgroundColor', style)).toEqual('rgb(255, 252, 2)');
  expect(prop('borderBottomColor', style)).toEqual('rgb(140, 200, 65)');
  expect(prop('borderBottomWidth', style)).toEqual('2px');
  expect(prop('borderBottomStyle', style)).toEqual('solid');
};

describe('search by free text', () => {
  let page;
  beforeAll(async done => {
    await changeVibe('sfs')('2');
    page = await global.__BROWSER__.newPage();
    await page.setViewport({ width: 1200, height: 800 });
    const url = global._E2E_.makeUrl(`?tenant=oecd:de`);
    await page.goto(url);
    done();
  });
  it('load the homepage', async () => {
    await page.waitForSelector(testidSelector('spotlight_input'));
    if (process.env.E2E_ENV === 'debug') await page.waitFor(1000);
  });
  it("type 'tourism' in the free text search and press 'enter' key", async () => {
    await page.click(testidSelector('spotlight_input'));
    await page.keyboard.type('tourism');
    if (process.env.E2E_ENV === 'debug') await page.waitFor(1000);
    await page.keyboard.press('Enter');
  });
  it("should find 'Inbound & Outbound tourism' dataflows in the results", async () => {
    await page.waitForSelector(testidSelector('OECD-staging:DF_INBOUND_TOURISM'));
    await page.waitForSelector(testidSelector('OECD-staging:DF_OUTBOUND_TOURISM'));
    const text = await page.evaluate(() => document.body.textContent);
    expect(text).toContain('Inbound tourism');
    expect(text).toContain('Outbound tourism');
  });
  it('should have 4 highlights in the results', async () => {
    await page.waitForSelector(testidSelector('search_results'));
    const emTags = await page.$$(`${testidSelector('search_results')} em`);
    expect(length(emTags)).toEqual(4);
  });
  it("should have 'tourism' highlighted in the title (style checked)", async () => {
    await page.waitForSelector(testidSelector('search_results'));
    const style = await page.evaluate(() => {
      const tags = document
        .querySelector('[data-testid="OECD-staging:DF_INBOUND_TOURISM_title"]')
        .getElementsByTagName('em');
      return JSON.parse(JSON.stringify(getComputedStyle(tags[0])));
    });
    expectHighlightStyleHelper(style);
  });
  it("should have 'tourism' highlighted in the measure attribute (style checked)", async () => {
    await page.waitForSelector(testidSelector('search_results'));
    const style = await page.evaluate(() => {
      const tags = document
        .querySelector('[data-testid="OECD-staging:DF_INBOUND_TOURISM_highlight_Measure"]')
        .getElementsByTagName('em');
      return JSON.parse(JSON.stringify(getComputedStyle(tags[0])));
    });
    expectHighlightStyleHelper(style);
  });
});
