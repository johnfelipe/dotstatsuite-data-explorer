import * as R from 'ramda';
import { updateDataquery, getDataquery } from '@sis-cc/dotstatsuite-sdmxjs';
import { getDataquery as getRouterDataquery, getPeriod } from '../selectors/router';
import { getDimensions, getFrequency, getFrequencyArtefact } from '../selectors/sdmx';
import {
  getSelectedIdsIndexed,
  setSelectedDimensionsValues,
  getOnlyHasDataDimensions,
} from '../lib/sdmx';
import { getSdmxPeriod, changeFrequency, getFrequencies } from '../lib/sdmx/frequency';
import { START_PERIOD, END_PERIOD, LASTN } from '../utils/used-filter';
import { getHighlightedConstraints } from '../utils';
import { CHANGE_LOCALE } from './app';

//---------------------------------------------------------------------------------------------model
export const model = () => ({
  dataRequestSize: null,
  datasetAttributes: null,
  attributes: [],
  constraints: {},
  dimensions: [],
  data: undefined,
  layout: {},
  frequencyArtefact: {},
  externalResources: [],
  hierarchySchemes: [],
  timePeriodArtefact: {},
  range: {},
  observationsCount: undefined,
  externalReference: undefined,
  name: undefined,
  hierarchies: {},
});

//-----------------------------------------------------------------------------------------constants
export const HANDLE_STRUCTURE = '@@sdmx/HANDLE_STRUCTURE';
export const REQUEST_STRUCTURE = '@@sdmx/REQUEST_STRUCTURE';
export const PARSE_STRUCTURE = '@@sdmx/PARSE_STRUCTURE';
export const HANDLE_DATA = '@@sdmx/HANDLE_DATA';
export const REQUEST_DATA = '@@sdmx/REQUEST_DATA';
export const PARSE_DATA = '@@sdmx/PARSE_DATA';
export const FLUSH_DATA = '@@sdmx/FLUSH_DATA';
export const RESET_SDMX = '@@sdmx/RESET_SDMX';
export const REQUEST_VIS_DATAFILE = '@@sdmx/REQUEST_VIS_DATAFILE';
export const REQUEST_SEARCH_DATAFILE = '@@sdmx/REQUEST_SEARCH_DATAFILE';
export const CHANGE_DATAFLOW = '@@sdmx/CHANGE_DATAFLOW';
export const RESET_DATAFLOW = '@@sdmx/RESET_DATAFLOW';
export const CHANGE_DATAQUERY = '@@sdmx/CHANGE_DATAQUERY';
export const APPLY_DATA_AVAILABILITY = '@@sdmx/APPLY_DATA_AVAILABILITY';
export const CHANGE_FREQUENCY_PERIOD = '@@sdmx/CHANGE_FREQUENCY_PERIOD';
export const CHANGE_LAST_N_OBS = '@@sdmx/CHANGE_LAST_N_OBS';
export const RESET_SPECIAL_FILTERS = '@@sdmx/RESET_SPECIAL_FILTERS';
export const EXTERNAL_REFERENCE = '@@sdmx/EXTERNAL_REFERENCE';
export const HANDLE_EXTERNAL_RESOURCES = '@@sdmx/HANDLE_EXTERNAL_RESOURCES';
export const REQUEST_EXTERNAL_RESOURCES = '@@search/REQUEST_EXTERNAL_RESOURCES';
export const REQUEST_AVAILABLE_CONSTRAINTS = '@@sdmx/REQUEST_AVAILABLE_CONSTRAINTS';
export const PARSE_AVAILABLE_CONSTRAINTS = '@@sdmx/PARSE_AVAILABLE_CONSTRAINTS';
export const HANDLE_AVAILABLE_CONSTRAINTS = '@@sdmx/HANDLE_AVAILABLE_CONSTRAINTS';
export const REQUEST_BLANK_DATA = '@@sdmx/REQUEST_BLANK_DATA';
export const HANDLE_BLANK_DATA = '@@sdmx/HANDLE_BLANK_DATA';

export const REQUEST_DATA_PROPS = ['dataquery', 'lastNObservations', 'period'];

//------------------------------------------------------------------------------------------creators
export const requestData = ({ shouldRequestStructure } = {}) => ({
  type: REQUEST_DATA,
  shouldRequestStructure,
});

export const requestVisDataFile = ({ isDownloadAllData }) => ({
  type: REQUEST_VIS_DATAFILE,
  payload: { isDownloadAllData },
});

export const requestSearchDataFile = ({ dataflow }) => ({
  type: REQUEST_SEARCH_DATAFILE,
  payload: { dataflow },
});

export const requestExternalResources = ({ dataflow }) => ({
  type: REQUEST_EXTERNAL_RESOURCES,
  dataflow,
});

export const resetSdmx = () => ({
  type: RESET_SDMX,
  request: 'getSearch',
});

// actionHistory could be replaceHistory, pushHistory.
export const changeDataflow = (dataflow, actionHistory = 'pushHistory', viewer) => dispatch => {
  if (R.isNil(dataflow)) return;
  dispatch({
    type: CHANGE_DATAFLOW,
    [actionHistory]: {
      pathname: '/vis',
      payload: {
        viewer,
        dataflow,
        highlightedConstraints: getHighlightedConstraints(R.propOr([], 'highlights', dataflow)),
      },
    },
  });
};

export const resetDataflow = () => dispatch => {
  dispatch(resetSdmx());
  dispatch({
    type: RESET_DATAFLOW,
    pushHistory: {
      pathname: '/',
      payload: {
        dataflow: null,
        filter: null,
        dataquery: null,
        hasDataAvailability: null,
        viewer: null,
        // period: [undefined, undefined] is used to force no period, null means that default can be used
        period: null,
        lastNObservations: null,
        layout: null,
        time: null,
        microdataConstraints: null,
        highlightedConstraints: null,
        display: null,
        hierarchies: {},
      },
    },
  });
};

export const changeLastNObservations = lastNObservations => ({
  type: CHANGE_LAST_N_OBS,
  pushHistory: { pathname: '/vis', payload: { lastNObservations } },
  request: 'getData',
});

export const resetFilters = () => (dispatch, getState) => {
  dispatch({
    type: CHANGE_DATAQUERY,
    pushHistory: {
      pathname: '/vis',
      payload: {
        dataquery: updateDataquery(getDimensions(getState()), getRouterDataquery(getState()), null),
        period: [undefined, undefined],
        lastNObservations: null,
      },
    },
    request: 'getData',
  });
};

export const resetSpecialFilters = (_, ids) => (dispatch, getState) => {
  const id = R.is(Array)(ids) ? R.head(ids) : ids;
  // special is lastNObservations
  if (R.equals(LASTN, id)) return dispatch(changeLastNObservations());

  // special is start OR end (period)
  if (R.either(R.equals(START_PERIOD), R.equals(END_PERIOD))(id)) {
    const payloadPeriod = period => {
      if (R.equals(START_PERIOD, id)) return [undefined, R.last(period)];
      if (R.equals(END_PERIOD, id)) return [R.head(period), undefined];
    };
    return dispatch({
      type: CHANGE_FREQUENCY_PERIOD,
      request: 'getData',
      pushHistory: { pathname: '/vis', payload: { period: payloadPeriod(getPeriod(getState())) } },
    });
  }

  // reset all special filters (period and lastNObservations)
  dispatch({
    type: RESET_SPECIAL_FILTERS,
    pushHistory: {
      pathname: '/vis',
      payload: { period: [undefined, undefined], lastNObservations: null },
    },
    request: 'getData',
  });
};

export const changeDataquery = (filterId, valueIds) => (dispatch, getState) => {
  const dataquery = updateDataquery(getDimensions(getState()), getRouterDataquery(getState()), {
    [filterId]: valueIds,
  });
  dispatch({
    type: CHANGE_DATAQUERY,
    pushHistory: { pathname: '/vis', payload: { dataquery } },
    request: 'getData',
  });
};

export const applyDataAvailability = hasDataAvailability => (dispatch, getState) => {
  dispatch({
    type: APPLY_DATA_AVAILABILITY,
    pushHistory: { pathname: '/vis', payload: { hasDataAvailability } },
  });

  if (R.not(hasDataAvailability)) return;

  const currentDataquery = getRouterDataquery(getState());
  const dimensions = getDimensions(getState());
  const dimensionsWithSelectedValues = setSelectedDimensionsValues(currentDataquery, dimensions);
  const frequencyArtefact = getFrequencyArtefact(getState());
  const frequencyId = R.prop('id')(frequencyArtefact);
  const availableFrequencies = R.pipe(
    getOnlyHasDataDimensions,
    R.head,
    getFrequencies,
  )([frequencyArtefact]);
  const frequency = changeFrequency(getFrequency(getState()))(availableFrequencies);
  const selection = R.assoc(
    frequencyId,
    frequency,
    getSelectedIdsIndexed(dimensionsWithSelectedValues),
  );
  const dataquery = getDataquery(dimensions, selection);

  if (R.equals(currentDataquery, dataquery)) return;

  dispatch({ type: CHANGE_FREQUENCY_PERIOD, payload: { frequency } });
  dispatch({
    type: CHANGE_DATAQUERY,
    pushHistory: { pathname: '/vis', payload: { dataquery } },
    request: 'getData',
  });
};

export const changeFrequencyPeriod = ({ valueId, period } = {}) => (dispatch, getState) => {
  const filterId = R.prop('id')(getFrequencyArtefact(getState()));
  const action = {
    type: CHANGE_FREQUENCY_PERIOD,
    pushHistory: {
      pathname: '/vis',
      payload: {
        period: R.ifElse(
          R.either(R.isNil, R.isEmpty),
          R.always([undefined, undefined]),
          R.identity,
        )(R.map(getSdmxPeriod(valueId))(period)),
      },
    },
  };

  if (R.not(R.isNil(filterId))) {
    dispatch(action);
    dispatch(changeDataquery(filterId, [valueId]));
    return;
  }

  dispatch(R.assoc('request', 'getData', action));
};

//-------------------------------------------------------------------------------------------reducer
export default (state = model(), action = {}) => {
  switch (action.type) {
    case FLUSH_DATA:
      return R.pipe(
        R.set(R.lensProp('data'), undefined),
        R.set(R.lensProp('range'), undefined),
      )(state);
    case EXTERNAL_REFERENCE:
      return R.set(R.lensProp('externalReference'), R.prop('externalReference', action))(state);
    case HANDLE_STRUCTURE:
      return R.pipe(
        R.set(R.lensProp('dataRequestSize'), R.path(['structure', 'dataRequestSize'], action)),
        R.set(R.lensProp('externalResources'), R.path(['structure', 'externalResources'], action)),
        R.set(R.lensProp('dimensions'), R.path(['structure', 'dimensions'], action)),
        R.set(R.lensProp('attributes'), R.path(['structure', 'attributes'], action)),
        R.set(
          R.lensProp('timePeriodArtefact'),
          R.path(['structure', 'timePeriodArtefact'], action),
        ),
        R.set(R.lensProp('name'), R.path(['structure', 'title'], action)),
        R.set(R.lensProp('description'), R.path(['structure', 'description'], action)),
        R.set(R.lensProp('hierarchySchemes'), R.path(['structure', 'hierarchySchemes'], action)),
        R.set(
          R.lensPath(['constraints', 'actual']),
          R.path(['structure', 'actualContentConstraints'], action),
        ),
        R.set(R.lensProp('observationsCount'), R.path(['structure', 'observationsCount'], action)),
        R.set(R.lensPath(['constraints', 'validFrom']), R.path(['structure', 'validFrom'], action)),
        R.dissoc('range'),
        R.set(R.lensProp('hierarchies'), R.path(['structure', 'hierarchies'], action)),
      )(state);
    case HANDLE_AVAILABLE_CONSTRAINTS:
      return R.pipe(R.set(R.lensPath(['constraints', 'available']), action.availableConstraints))(
        state,
      );
    case HANDLE_DATA:
      return R.pipe(
        R.set(R.lensProp('data'), R.prop('data', action)),
        R.set(R.lensProp('range'), R.prop('range', action)),
      )(state);
    case HANDLE_BLANK_DATA:
      return R.set(R.lensProp('datasetAttributes'), R.prop('datasetAttributes', action))(state);
    case CHANGE_LOCALE:
    case RESET_SDMX:
      return { ...state, ...model() };
    default:
      return state;
  }
};
