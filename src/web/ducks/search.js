import * as R from 'ramda';
import md5 from 'md5';
import { CHANGE_LOCALE } from './app';
import { HANDLE_EXTERNAL_RESOURCES } from './sdmx';
import {
  getTerm,
  getConstraints,
  getLocale,
  getStart,
  getFacet,
  getSortIndexSelected,
} from '../selectors/router';
import { getConfig, getHasNoSearchParams, getRows } from '../selectors/search';
import searchApi from '../api/search';
import { setPending, pushLog, LOG_ERROR, flushLog } from './app';
import { defaultSearchRows, search, valueIcons, homeFacetIds } from '../lib/settings';
import { sortedStringParams } from '../lib/search/getStringSearchParams';

const isDev = process.env.NODE_ENV === 'development';

//---------------------------------------------------------------------------------------------model
export const model = () => ({
  dataflows: [],
  facets: [],
  config: undefined,
  rows: defaultSearchRows, // number of dataflows per page (search)
  sortParams: sortedStringParams(), // default sort params
});

//-----------------------------------------------------------------------------------------constants
export const HANDLE_CONFIG = '@@search/HANDLE_CONFIG';
export const HANDLE_SEARCH = '@@search/HANDLE_SEARCH';
export const RESET_SEARCH = '@@search/RESET_SEARCH';
export const CHANGE_TERM = '@@search/CHANGE_TERM';
export const CHANGE_START = '@@search/CHANGE_START';
export const CHANGE_FACET = '@@search/CHANGE_FACET';
export const CHANGE_CONSTRAINTS = '@@search/CHANGE_CONSTRAINTS';
export const CHANGE_SEARCH_ORDER = '@@search/CHANGE_SEARCH_ORDER';

export const REQUEST_SEARCH_PROPS = ['term', 'start', 'constraints', 'sortIndexSelected'];

//------------------------------------------------------------------------------------------creators
export const resetSearch = () => ({
  type: RESET_SEARCH,
  pushHistory: { pathname: '/' },
});

export const changeTerm = ({ term } = {}) => ({
  type: CHANGE_TERM,
  pushHistory: {
    pathname: '/',
    payload: {
      term,
      start: 0,
      constraints: null,
      facet: null,
      highlightedConstraints: null,
    },
  },
  request: 'getSearch',
});

export const changeFacet = facetId => (dispatch, getState) => {
  const prevFacetId = getFacet(getState());
  const facet = R.ifElse(R.equals(prevFacetId), R.always(null), R.identity)(facetId);
  dispatch({ type: CHANGE_FACET, pushHistory: { pathname: '/', payload: { facet } } });
};

export const changeStart = start => ({
  type: CHANGE_START,
  pushHistory: { pathname: '/', payload: { start } },
  request: 'getSearch',
});

export const changeConstraints = (facetId, constraintIds) => (dispatch, getState) => {
  // constraintIds can be an array because of Chips component or a string because of CollapseButtons
  const constraintId = R.is(Array)(constraintIds) ? R.head(constraintIds) : constraintIds;
  const prevConstraints = getConstraints(getState());
  let constraints;
  if (R.isNil(facetId)) {
    // clear all constraints
    constraints = {};
  } else if (R.isEmpty(constraintIds)) {
    // clear all values of a facet
    constraints = R.reject(R.propEq('facetId', facetId), prevConstraints);
  } else {
    // add or remove a value in a facet
    const hash = md5(`${facetId}${constraintId}`);
    constraints = R.ifElse(
      R.has(hash),
      R.dissoc(hash),
      R.assoc(hash, { facetId, constraintId }),
    )(prevConstraints);
  }

  dispatch({
    type: CHANGE_CONSTRAINTS,
    pushHistory: { pathname: '/', payload: { constraints, start: 0, facet: facetId } },
    request: 'getSearch',
  });
};

//--------------------------------------------------------------------------------------thunks (api)
const request = (dispatch, ctx) => {
  const { method } = ctx;
  const pendingId = ctx.pendingId || method;

  // eslint-disable-next-line no-console
  if (isDev) console.info(`request: ${pendingId}`);

  dispatch(setPending(pendingId, true));
  dispatch(flushLog(LOG_ERROR));
  return searchApi(ctx)
    .then(res => {
      dispatch(setPending(pendingId));
      return res;
    })
    .catch(error => {
      const log = error.response
        ? { method, errorCode: error.response.data.errorCode, statusCode: error.response.status }
        : { method, error };

      dispatch(setPending(pendingId));
      dispatch(pushLog({ type: LOG_ERROR, payload: { log } }));

      // required to break the promised chain
      throw error;
    });
};

export const requestConfig = () => (dispatch, getState) => {
  const locale = getLocale(getState());
  if (R.equals(R.prop('locale', getConfig(getState())), locale)) return Promise.resolve();

  const requestArgs = { lang: locale, rows: 0, fl: homeFacetIds };
  const parserArgs = {
    facetIds: R.prop('homeFacetIds', search),
    config: { valueIcons },
  };
  return request(dispatch, { method: 'getConfig', requestArgs, parserArgs }).then(config =>
    dispatch({ type: HANDLE_CONFIG, config }),
  );
};

export const requestSearch = () => (dispatch, getState) => {
  if (getHasNoSearchParams(getState())) return Promise.resolve();
  const requestArgs = {
    lang: getLocale(getState()),
    search: getTerm(getState()),
    sort: sortedStringParams(getSortIndexSelected(getState())),
    facets: R.pipe(
      R.ifElse(
        R.isEmpty,
        R.identity,
        R.reduceBy((acc, { constraintId }) => acc.concat(constraintId), [], R.prop('facetId')),
      ),
    )(R.values(getConstraints(getState()))),
    rows: getRows(getState()),
    start: getStart(getState()),
  };
  const parserArgs = {
    config: { valueIcons },
    constraints: getConstraints(getState()),
  };

  return request(dispatch, { method: 'getSearch', requestArgs, parserArgs }).then(search =>
    dispatch({
      type: HANDLE_SEARCH,
      search,
      replaceHistory: { pathname: '/', payload: { searchResultNb: search?.searchResultNb } },
    }),
  );
};

export const changeSearchOrder = index => dispatch => {
  dispatch({
    type: CHANGE_SEARCH_ORDER,
    pushHistory: { pathname: '/', payload: { sortIndexSelected: index } },
  });
  dispatch(requestSearch());
};

//-------------------------------------------------------------------------------------------reducer
export default (state = model(), action = {}) => {
  switch (action.type) {
    case HANDLE_CONFIG:
      return R.set(R.lensProp('config'), R.prop('config', action), state);
    case HANDLE_SEARCH:
      return R.pipe(R.prop('search'), R.pick(['dataflows', 'facets']), R.mergeRight(state))(action);
    case RESET_SEARCH:
      return {
        ...R.pick(['config'], state),
        rows: defaultSearchRows,
      };
    case CHANGE_LOCALE:
      return model();
    case HANDLE_EXTERNAL_RESOURCES:
      return {
        ...state,
        externalResources: R.mergeRight(
          R.prop('externalResources')(action),
          R.prop('externalResources')(state),
        ),
      };
    default:
      return state;
  }
};
