import * as R from 'ramda';
import { getVisDataDimensions } from '../selectors';
import { CHANGE_DATAQUERY, HANDLE_STRUCTURE, RESET_DATAFLOW } from './sdmx';
import { MICRODATA } from '../utils/constants';

//---------------------------------------------------------------------------------------------model
const model = () => ({
  data: undefined,
  range: undefined,
  dimensionId: undefined,
});

//-------------------------------------------------------------------------------------------actions
export const REQUEST_MICRODATA = '@@microdata/request';
export const FLUSH_MICRODATA = '@@microdata/flush';
export const PARSE_MICRODATA = '@@microdata/parse';
export const HANDLE_MICRODATA = '@@microdata/handle';
export const UPDATE_MICRODATA_CONSTRAINTS = '@@microdata/updateConstraints';

//------------------------------------------------------------------------------------------creators
export const requestMicrodata = ({ shouldRequestStructure } = {}) => ({
  type: REQUEST_MICRODATA,
  datatype: MICRODATA,
  shouldRequestStructure,
});

export const updateMicrodataConstraints = (constraints = {}) => (dispatch, getState) => {
  const { many } = getVisDataDimensions()(getState());
  const microdataConstraints = R.pick(R.keys(many), constraints);
  dispatch({
    type: UPDATE_MICRODATA_CONSTRAINTS,
    pushHistory: {
      pathname: '/vis',
      payload: { microdataConstraints, viewer: MICRODATA },
    },
  });
  dispatch(requestMicrodata());
};

//-------------------------------------------------------------------------------------------reducer
export default (state = model(), action = {}) => {
  switch (action.type) {
    case HANDLE_STRUCTURE:
      return R.set(R.lensProp('dimensionId'), action.structure.microdataDimensionId, state);
    case FLUSH_MICRODATA:
    case RESET_DATAFLOW:
    case CHANGE_DATAQUERY:
      return { ...state, data: null, range: null };
    case HANDLE_MICRODATA:
      return { ...state, ...R.pick(['data', 'range'], action) };
    default:
      return state;
  }
};
