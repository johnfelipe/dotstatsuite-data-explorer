import FileSaver from 'file-saver';
import * as R from 'ramda';
import { getFilter } from '../selectors';
import { getData, getRawDataRequestArgs } from '../selectors/sdmx';
import { HANDLE_STRUCTURE, CHANGE_DATAFLOW, RESET_DATAFLOW } from './sdmx';
import { SET_PENDING } from './app';
import { createExcelWorkbook } from '../xlsx';
import { getFilename } from '../lib/sdmx';
import { PANEL_USED_FILTERS, CHART_IDS } from '../utils/constants';
import { getIsMicrodata } from '../selectors/microdata';

//-----------------------------------------------------------------------------------------constants
export const CSV_SELECTION = 'CSV_SELECTION';
export const CSV_FULL = 'CSV_FULL';

//---------------------------------------------------------------------------------------------model
export const model = () => ({
  actionId: undefined,
  isFull: false,
  title: undefined,
  filter: PANEL_USED_FILTERS,
  excel: { error: undefined },
  png: { error: undefined },
  map: null,
  isLoadingMap: false,
});

//-------------------------------------------------------------------------------------------actions
export const CHANGE_ACTION_ID = '@@vis/CHANGE_ACTION_ID';
export const CHANGE_FULLSCREEN = '@@vis/CHANGE_FULLSCREEN';
export const SHARE_CHART = '@@vis/SHARE_CHART';
export const CHANGE_LAYOUT = '@@vis/CHANGE_LAYOUT';
export const CHANGE_DISPLAY = '@@vis/CHANGE_DISPLAY';
export const CHANGE_TIME_DIMENSION_ORDERS = '@@vis/CHANGE_TIME_DIMENSION_ORDERS';
export const SHARE_SUCCESS = '@@vis/SHARE_SUCCESS';
export const LOADING_MAP = '@@vis/LOADING_MAP';
export const LOAD_MAP_SUCCESS = '@@vis/LOAD_MAP_SUCCESS';
export const LOAD_MAP_ERROR = '@@vis/LOAD_MAP_ERROR';
export const DOWNLOAD_EXCEL_SUCCESS = '@@vis/DOWNLOAD_EXCEL_SUCCESS ';
export const DOWNLOAD_EXCEL_ERROR = '@@vis/DOWNLOAD_EXCEL_ERROR';
export const DOWNLOADING_PNG_CHART = '@@vis/DOWNLOADING_PNG_CHART';
export const CLOSE_SHARE_POPUP = '@@vis/CLOSE_SHARE_POPUP';
export const DOWNLOADING_PNG = '@@vis/DOWNLOADING_PNG';
export const DOWNLOAD_PNG_SUCCESS = '@@vis/DOWNLOAD_PNG_SUCCESS ';
export const DOWNLOAD_PNG_ERROR = '@@vis/DOWNLOAD_PNG_ERROR';
export const CHANGE_VIEWER = '@@vis/CHANGE_VIEWER';
export const CHANGE_FILTER = '@@vis/CHANGE_FILTER';

//------------------------------------------------------------------------------------------creators
export const changeFilter = filterId => (dispatch, getState) => {
  const prevFilterId = getFilter(getState());
  const filter = R.ifElse(R.equals(prevFilterId), R.always(null), R.identity)(filterId);
  dispatch({ type: CHANGE_FILTER, payload: { filter } });
};
export const changeFullscreen = (isFull, isOpening) => ({
  type: CHANGE_FULLSCREEN,
  payload: { isFull, isOpening },
});
export const changeActionId = actionId => ({
  type: CHANGE_ACTION_ID,
  payload: { actionId },
});
export const shareChart = ({ media, sharedData, shareResponse }) => ({
  type: SHARE_CHART,
  payload: { label: media, sharedData, shareResponse },
});
export const changeLayout = layout => ({
  type: CHANGE_LAYOUT,
  pushHistory: { pathname: '/vis', payload: { layout: R.map(R.pluck('id'))(layout) } },
});
export const changeDisplay = display => ({
  type: CHANGE_DISPLAY,
  pushHistory: { pathname: '/vis', payload: { display } },
});
export const changeTimeDimensionOrders = (id, order) => ({
  type: CHANGE_TIME_DIMENSION_ORDERS,
  pushHistory: { pathname: '/vis', payload: { time: { [id]: order } } },
});
export const shareSuccess = () => ({
  type: SHARE_SUCCESS,
  payload: {},
});
export const loadingMap = () => ({
  type: LOADING_MAP,
  payload: {},
});
export const loadMapSuccess = map => ({
  type: LOAD_MAP_SUCCESS,
  payload: { map },
});
export const loadMapError = error => ({
  type: LOAD_MAP_ERROR,
  payload: { error },
});
export const closeSharePopup = () => ({ type: CLOSE_SHARE_POPUP });
export const changeViewer = (id, option = {}) => (dispatch, getState) => {
  const action = {
    type: CHANGE_VIEWER,
    pushHistory: {
      pathname: '/vis',
      payload: {
        viewer: R.has('map', option) ? CHART_IDS.CHOROPLETHCHART : id,
        map: R.prop('map', option),
      },
    },
  };
  const noData = R.isNil(getData(getState()));
  if (getIsMicrodata(getState()) && noData) return dispatch(R.assoc('request', 'getData', action));
  dispatch(action);
};

//--------------------------------------------------------------------------------------thunks (api)
export const downloadExcel = props => (dispatch, getState) => {
  dispatch({ type: SET_PENDING, payload: { id: props.id, is: true } });

  const args = getRawDataRequestArgs(getState());
  const fileName = getFilename(args);
  return createExcelWorkbook(props)
    .then(workbook => workbook.outputAsync(workbook))
    .then(blob => {
      FileSaver.saveAs(blob, `${fileName}.xlsx`);
      dispatch({ type: SET_PENDING, payload: { id: props.id, is: false } });
    })
    .then(() => dispatch({ type: DOWNLOAD_EXCEL_SUCCESS }))
    .catch(error => dispatch({ type: DOWNLOAD_EXCEL_ERROR, payload: { error } }));
};

export const downloadPng = (uiHandler, id) => (dispatch, getState) => {
  dispatch({ type: SET_PENDING, payload: { id, is: true } });
  const args = getRawDataRequestArgs(getState());
  const fileName = getFilename(args);
  return uiHandler()
    .then(function(canvas) {
      canvas.toBlob(function(blob) {
        FileSaver.saveAs(blob, `${fileName}.png`);
      });
      dispatch({ type: SET_PENDING, payload: { id, is: false } });
    })
    .then(() => dispatch({ type: DOWNLOAD_PNG_SUCCESS }))
    .catch(error => dispatch({ type: DOWNLOAD_PNG_ERROR, payload: { error } }));
};

//-------------------------------------------------------------------------------------------reducer
export default (state = model(), action = {}) => {
  switch (action.type) {
    case HANDLE_STRUCTURE:
      return R.pipe(
        R.set(R.lensProp('layout'), R.path(['structure', 'layout'], action)),
        R.set(R.lensProp('title'), R.path(['structure', 'title'], action)),
      )(state);
    case CHANGE_FULLSCREEN:
      return R.pipe(
        R.set(R.lensProp('isFull'), R.path(['payload', 'isFull'], action)),
        R.set(R.lensProp('isOpeningFullscreen'), R.path(['payload', 'isOpening'], action)),
      )(state);
    case CHANGE_ACTION_ID:
      return R.evolve({
        actionId: R.cond([
          [R.equals(action.payload.actionId), R.always(undefined)],
          [R.T, R.always(action.payload.actionId)],
        ]),
      })(state);
    case CHANGE_LAYOUT:
      return { ...state, actionId: undefined };
    case LOADING_MAP:
      return { ...state, isLoadingMap: true };
    case LOAD_MAP_SUCCESS:
      return { ...state, map: action.payload.map, isLoadingMap: false };
    case LOAD_MAP_ERROR:
      return { ...state, map: { error: action.payload.error }, isLoadingMap: false };
    case CHANGE_FILTER:
      return { ...state, filter: action.payload.filter };
    case DOWNLOAD_EXCEL_ERROR:
      return { ...state, excel: { error: action.payload.error } };
    case DOWNLOAD_EXCEL_SUCCESS:
      return { ...state, excel: { error: undefined } };
    case DOWNLOAD_PNG_ERROR:
      return { ...state, png: { error: action.payload.error } };
    case DOWNLOAD_PNG_SUCCESS:
      return { ...state, png: { error: undefined } };
    case RESET_DATAFLOW:
    case CHANGE_DATAFLOW:
      return model();
    default:
      return state;
  }
};
