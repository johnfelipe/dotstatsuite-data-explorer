import { LOCATION_CHANGE } from 'connected-react-router';
import * as R from 'ramda';
import { setLocale } from '../i18n';
import { getPathname } from '../selectors/router';

//------------------------------------------------------------------------------constants
export const LOG_ERROR = 'LOG_ERROR';
export const LOG_ERROR_SDMX_STRUCTURE = 'LOG_ERROR_SDMX_STRUCTURE';
export const LOG_ERROR_SDMX_DATA = 'LOG_ERROR_SDMX_DATA';

//----------------------------------------------------------------------------------model
export const model = () => ({
  logs: [],
  pending: {},
  token: undefined,
  user: undefined,
  isFirstRendering: true,
});

//--------------------------------------------------------------------------------actions
export const PUSH_LOG = '@@app/PUSH_LOG';
export const FLUSH_LOG = '@@app/FLUSH_LOG';
export const SET_PENDING = '@@app/SET_PENDING';
export const CHANGE_HAS_ACCESSIBILITY = '@@app/CHANGE_HAS_ACCESSIBILITY';
export const CHANGE_LOCALE = '@@app/CHANGE_LOCALE';
export const USER_SIGNED_IN = '@@app/USER_SIGNED_IN';
export const USER_SIGNED_OUT = '@@app/USER_SIGNED_OUT';
export const REFRESH_TOKEN = '@@app/REFRESH_TOKEN';

//-------------------------------------------------------------------------------creators
export const pushLog = log => ({ type: PUSH_LOG, payload: { log } });
export const flushLog = type => ({ type: FLUSH_LOG, payload: { type } });

export const setPending = (id, is) => ({ type: SET_PENDING, payload: { id, is } });

export const changeHasAccessibility = hasAccessibility => ({
  type: CHANGE_HAS_ACCESSIBILITY,
  pushHistory: { payload: { hasAccessibility } },
});

export const changeLocale = locale => dispatch => {
  setLocale(locale);
  dispatch({
    type: CHANGE_LOCALE,
    pushHistory: {
      payload: {
        locale,
        term: '',
        start: 0,
        constraints: null,
        facet: null,
        highlightedConstraints: null,
      },
    },
  });
};

// on sign(in/out), requests that are impacted by auth should be re-fetched
// search is public, no need to re-fetch anything
// (reminder, the popup auth flow doesn't rely on redirect!)
// so far, only SDMX requests are concerned and they are triggered only under /vis route
// in order to avoid having a saga or a middleware for only 2 actions
// the behavior is implemented here
const getRequest = getState => (getPathname(getState()) === '/vis' ? 'getStructure' : 'getNothing');
export const userSignedIn = (token, user) => (dispatch, getState) => {
  dispatch({ type: USER_SIGNED_IN, token, user, request: getRequest(getState) });
};
export const userSignedOut = () => (dispatch, getState) => {
  dispatch({ type: USER_SIGNED_OUT, request: getRequest(getState) });
};
export const refreshToken = token => ({ type: REFRESH_TOKEN, token });

//--------------------------------------------------------------------------------reducer
export default (state = model(), action = {}) => {
  switch (action.type) {
    case PUSH_LOG:
      return R.over(R.lensProp('logs'), R.prepend(action.payload.log), state);
    case FLUSH_LOG:
      return R.over(R.lensProp('logs'), R.reject(R.propEq('type', action.payload.type)), state);
    case SET_PENDING:
      return R.assocPath(['pending', action.payload.id], action.payload.is, state);
    case USER_SIGNED_IN:
      return { ...state, token: action.token, user: action.user };
    case USER_SIGNED_OUT:
      return { ...state, user: null, token: null };
    case REFRESH_TOKEN:
      return { ...state, token: action.token };
    case LOCATION_CHANGE:
      return { ...state, isFirstRendering: action.isFirstRendering };
    default:
      return state;
  }
};
