import React from 'react';
import { connect, useSelector } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose, onlyUpdateForKeys, withProps } from 'recompose';
import * as R from 'ramda';
import ToolBar from './ToolBar';
import { TableLayout, ChartsConfig } from '@sis-cc/dotstatsuite-visions';
import Paper from '@material-ui/core/Paper';
import Collapse from '@material-ui/core/Collapse';
import { makeStyles } from '@material-ui/core';
import { injectIntl } from 'react-intl';
import { FormattedMessage, formatMessage } from '../i18n';
import { changeActionId, changeLayout, changeTimeDimensionOrders } from '../ducks/vis';
import { getVisActionId, getVisDimensionLayout, getVisDimensionFormat } from '../selectors';
import { getTimePeriodArtefact } from '../selectors/sdmx';
import { getViewer, getHasAccessibility, getTimeDimensionOrders } from '../selectors/router';
import APIQueries from './vis/api-queries';
import ShareView from './vis/share';
import messages from './messages';
import { getUser } from '../selectors/app';
import { TABLE, MARGE_SIZE, MARGE_RATIO, SIDE_WIDTH, SMALL_SIDE_WIDTH } from '../utils/constants';

const useStyles = makeStyles(theme => ({
  container: {
    margin: theme.spacing(1, 0, 1, 0),
    padding: theme.spacing(2),
  },
  toolbar: {
    position: 'sticky',
    borderTop: `solid 2px ${theme.palette.grey[700]}`,
    [theme.breakpoints.down('sm')]: {
      left: `${MARGE_SIZE}%`,
      maxWidth: ({ maxWidth }) => maxWidth,
    },
    [theme.breakpoints.only('md')]: {
      left: `calc(${MARGE_SIZE}% + ${SMALL_SIDE_WIDTH + theme.spacing(2)}px)`,
      maxWidth: ({ maxWidth }) => maxWidth - SMALL_SIDE_WIDTH - theme.spacing(2),
    },
    [theme.breakpoints.up('lg')]: {
      left: `calc(${MARGE_SIZE}% + ${SIDE_WIDTH + theme.spacing(2)}px)`,
      maxWidth: ({ maxWidth }) => maxWidth - SIDE_WIDTH - theme.spacing(2),
    },
  },
}));

// maxWidth - maxWidth * (1 - MARGE_RATIO)- SMALL_SIDE_WIDTH - 16
//-----------------------------------------------------------------------------------------Constants
const API = 'api';
const FILTERS = 'filters';
const CONFIG = 'config';
const SHARE = 'share';

//-----------------------------------------------------------------------------------------ToolTable
export const ToolTable = compose(
  connect(
    createStructuredSelector({
      layout: getVisDimensionLayout,
      itemRenderer: getVisDimensionFormat(),
      itemButton: getTimeDimensionOrders,
      timePeriod: getTimePeriodArtefact,
      accessibility: getHasAccessibility,
    }),
    { changeLayout, changeTimeDimensionOrders },
  ),
  injectIntl,
  onlyUpdateForKeys(['layout', 'timePeriod', 'itemRenderer', 'itemButton', 'accessibility']),
)(
  ({
    layout,
    changeLayout,
    itemButton,
    itemRenderer,
    timePeriod,
    intl,
    changeTimeDimensionOrders,
    accessibility,
  }) => {
    const asc = formatMessage(intl)(messages.asc);
    const desc = formatMessage(intl)(messages.desc);
    const timePeriodId = R.propOr('', 'id')(timePeriod);
    const isTimeDimensionInverted = R.propOr(false, timePeriodId)(itemButton);
    const timePeriodButton = R.pipe(
      R.set(R.lensProp('value'), isTimeDimensionInverted ? desc : asc),
      R.set(R.lensProp('options'), [asc, desc]),
      R.set(R.lensProp('onChange'), v =>
        changeTimeDimensionOrders(timePeriodId, R.equals(desc)(v)),
      ),
    )(timePeriod);
    return (
      <TableLayout
        accessibility={accessibility}
        layout={layout}
        commit={changeLayout}
        itemButton={itemButton}
        itemRenderer={itemRenderer}
        itemButtonProps={{
          [timePeriodId]: timePeriodButton,
        }}
        labels={{
          commit: formatMessage(intl)(messages.apply),
          cancel: formatMessage(intl)(messages.cancelChanges),
          row: <FormattedMessage id="de.table.layout.x" />,
          column: <FormattedMessage id="de.table.layout.y" />,
          section: <FormattedMessage id="de.table.layout.z" />,
          asc: <FormattedMessage id="de.table.layout.time.asc" />,
          desc: <FormattedMessage id="de.table.layout.time.desc" />,
          help: <FormattedMessage id="de.table.layout.help" />,
          table: <FormattedMessage id="de.table.layout.table" />,
          one: <FormattedMessage id="de.table.layout.value.one" />,
          wcagDragStart: props => formatMessage(intl)(messages.start, props),
          wcagDragUpdate: props => formatMessage(intl)(messages.update, props),
          wcagDragEnd: props => formatMessage(intl)(messages.end, props),
          wcagDragCancel: formatMessage(intl)(messages.cancel),
          wcagDragExplanation: formatMessage(intl)(messages.explanation),
        }}
      />
    );
  },
);

//---------------------------------------------------------------------------------------ChartConfig
const renameKey = R.curry((oldKey, newKey, obj) =>
  R.assoc(newKey, R.prop(oldKey, obj), R.dissoc(oldKey, obj)),
);

const renameProperties = (keys = []) =>
  R.map(
    R.ifElse(
      R.pipe(R.prop('id'), R.flip(R.includes)(keys)),
      renameKey('onChange', 'onSubmit'),
      R.identity,
    ),
  );

const propertiesKeys = [
  'width',
  'height',
  'freqStep',
  'maxX',
  'minX',
  'pivotX',
  'stepX',
  'maxY',
  'minY',
  'pivotY',
  'stepY',
  'title',
  'subtitle',
  'source',
];

export const Config = compose(
  injectIntl,
  withProps(({ intl, isAuthenticated, properties = {} }) => {
    const labels = {
      focus: formatMessage(intl)(messages.focus),
      highlight: formatMessage(intl)(messages.highlight),
      select: formatMessage(intl)(messages.select),
      baseline: formatMessage(intl)(messages.baseline),
      size: formatMessage(intl)(messages.size),
      width: formatMessage(intl)(messages.width),
      height: formatMessage(intl)(messages.height),
      display: formatMessage(intl)(messages.display),
      displayOptions: {
        label: formatMessage(intl)(messages.label),
        code: formatMessage(intl)(messages.code),
        both: formatMessage(intl)(messages.both),
      },
      series: formatMessage(intl)(messages.series),
      scatterDimension: formatMessage(intl)(messages.scatterDimension),
      scatterX: formatMessage(intl)(messages.scatterX),
      scatterY: formatMessage(intl)(messages.scatterY),
      symbolDimension: formatMessage(intl)(messages.symbolDimension),
      stackedDimension: formatMessage(intl)(messages.stackedDimension),
      stackedMode: formatMessage(intl)(messages.stackedMode),
      stackedModeOptions: {
        values: formatMessage(intl)(messages.values),
        percent: formatMessage(intl)(messages.percent),
      },
      axisX: formatMessage(intl)(messages.axisX),
      axisY: formatMessage(intl)(messages.axisY),
      max: formatMessage(intl)(messages.max),
      min: formatMessage(intl)(messages.min),
      pivot: formatMessage(intl)(messages.pivot),
      step: formatMessage(intl)(messages.step),
      frequency: formatMessage(intl)(messages.frequency),
      freqStep: formatMessage(intl)(messages.freqStep),
      title: formatMessage(intl)(messages.configTitle),
      subtitle: formatMessage(intl)(messages.subtitle),
      source: formatMessage(intl)(messages.source),
      logo: formatMessage(intl)(messages.logo),
      copyright: formatMessage(intl)(messages.copyright),
      reset: formatMessage(intl)(messages.reset),
      uniqFocusOption: formatMessage(intl)(messages.uniqFocusOption),
    };
    return {
      labels,
      properties: R.pipe(
        renameProperties(propertiesKeys),
        R.when(
          R.always(!isAuthenticated),
          R.omit(['title', 'subtitle', 'source', 'logo', 'copyright']),
        ),
      )(properties),
    };
  }),
)(ChartsConfig);

//---------------------------------------------------------------------------------------------Tools
export default compose(
  connect(
    createStructuredSelector({
      actionId: getVisActionId(),
      viewerId: getViewer,
    }),
    { changeActionId },
  ),
  withProps(({ actionId, viewerId }) => ({
    isApi: R.equals(API)(actionId),
    isTableConfig: R.equals(CONFIG)(actionId) && R.equals(TABLE)(viewerId),
    isChartConfig: R.equals(CONFIG)(actionId) && !R.equals(TABLE)(viewerId),
    isShare: R.equals(SHARE)(actionId),
    isFilters: R.equals(FILTERS)(actionId),
  })),
)(({ isApi, isTableConfig, isChartConfig, isShare, properties, share, viewerProps, maxWidth }) => {
  const classes = useStyles({ maxWidth: maxWidth - maxWidth * (1 - MARGE_RATIO) });
  const isAuthenticated = !!useSelector(getUser);

  return (
    <div className={classes.toolbar}>
      <ToolBar viewerProps={viewerProps} />
      <Collapse in={isApi}>
        <Paper elevation={2} className={classes.container}>
          <APIQueries />
        </Paper>
      </Collapse>
      <Collapse in={isTableConfig}>
        <Paper elevation={2} className={classes.container}>
          <ToolTable />
        </Paper>
      </Collapse>
      <Collapse in={isChartConfig}>
        <Paper elevation={2} className={classes.container}>
          <Config isAuthenticated={isAuthenticated} properties={properties} />
        </Paper>
      </Collapse>
      <Collapse in={isShare}>
        {isShare && (
          <Paper elevation={2} className={classes.container}>
            <ShareView share={share} viewerProps={viewerProps} />
          </Paper>
        )}
      </Collapse>
    </div>
  );
});
