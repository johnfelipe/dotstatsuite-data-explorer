import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose, withProps, pure, branch, renderNothing } from 'recompose';
import * as R from 'ramda';
import { injectIntl } from 'react-intl';
import dateFns from 'date-fns';
import { formatMessage, FormattedMessage } from '../i18n';
import {
  ScopeList,
  ExpansionPanel,
  PeriodPicker,
  InputNumber,
  Tag,
  Spotlight,
  spotlightScopeListEngine,
} from '@sis-cc/dotstatsuite-visions';
import { PanelDataAvailability, ToggleDataAvailability } from './vis-side/toggle-data-availability';
import { FiltersCurrent, UsedFilters, PeriodFilters, ClearFilters } from './vis-side/used-filter';
import { changeDataquery, changeFrequencyPeriod, changeLastNObservations } from '../ducks/sdmx';
import { changeFilter } from '../ducks/vis';
import { PANEL_PERIOD } from '../utils/constants';
import {
  getLastNObservations,
  getHasLastNObservations,
  getHasAccessibility,
  getHasDataAvailability,
} from '../selectors/router';
import {
  getFrequency,
  getFilters,
  getPeriod,
  getDatesBoundaries,
  getAvailableFrequencies,
  getFrequencyArtefact,
  getTimePeriodArtefact,
} from '../selectors/sdmx';
import { getFilter, getVisDimensionFormat } from '../selectors';
import { getIntervalPeriod } from '../lib/sdmx/frequency.js';
import messages, { periodMessages } from './messages';
import { getFilterLabel } from '../utils';
import SanitizedInnerHTML from './SanitizedInnerHTML';

const tagAccessor = (count, total) => (
  <FormattedMessage id="de.filter.tag" values={{ count, total }} />
);

const Filter = pure(ScopeList);
export const Filters = compose(
  injectIntl,
  connect(
    createStructuredSelector({
      filters: getFilters,
      activePanelId: getFilter,
      accessibility: getHasAccessibility,
      labelAccessor: getVisDimensionFormat(),
      hasDataAvailability: getHasDataAvailability,
    }),
    { changeSelection: changeDataquery, onChangeActivePanel: changeFilter },
  ),
  withProps(({ intl, labelAccessor }) => ({
    labels: {
      navigateNext: formatMessage(intl)(messages.next),
      backHelper: formatMessage(intl)(messages.backHelper),
      childrenNavigateDesc: formatMessage(intl)(messages.childrenNavigateDesc),
      selection: {
        title: formatMessage(intl)(messages.selectionTitle),
        currentLevel: formatMessage(intl)(messages.selectionCurrent),
        level: formatMessage(intl)(messages.selectionLevel),
      },
    },
    HTMLRenderer: SanitizedInnerHTML,
    topElementProps: {
      fullWidth: true,
      hasClearAll: true,
      mainPlaceholder: formatMessage(intl)(messages.primary),
      secondaryPlaceholder: formatMessage(intl)(messages.secondary),
      defaultSpotlight: {
        engine: spotlightScopeListEngine,
        placeholder: formatMessage(intl)(messages.placeholder),
        fields: {
          label: {
            id: 'label',
            accessor: getFilterLabel(labelAccessor),
            isSelected: true,
          },
        },
      },
    },
    topElementComponent: values => (R.gte(R.length(values), 8) ? Spotlight : null),
  })),
  pure,
)(({ topElementComponent, filters, labelAccessor, hasDataAvailability, ...parentProps }) =>
  R.map(
    ({ id, label, values = [] }) => (
      <Filter
        {...parentProps}
        limitDisplay={1}
        disableAccessor={R.pipe(R.prop('isEnabled'), R.not)}
        displayAccessor={
          R.and(hasDataAvailability, R.not(R.isNil(R.find(R.prop('hasData'), values))))
            ? R.prop('hasData')
            : undefined
        }
        tagAccessor={tagAccessor}
        labelRenderer={getFilterLabel(labelAccessor)}
        id={id}
        key={id}
        label={label}
        items={values}
        TopElementComponent={topElementComponent(values)}
        bulkSelection={true}
      />
    ),
    filters,
  ),
);

//--------------------------------------------------------------------------------------------------
export const Period = compose(
  injectIntl,
  connect(
    createStructuredSelector({
      period: getPeriod,
      frequency: getFrequency,
      availableFrequencies: getAvailableFrequencies,
      boundaries: getDatesBoundaries,
      frequencyArtefact: getFrequencyArtefact,
    }),
    { changeFrequencyPeriod },
  ),
  withProps(
    ({
      availableFrequencies,
      frequency,
      period,
      changeFrequencyPeriod,
      intl,
      boundaries,
      frequencyArtefact,
    }) => ({
      isBlank: R.isNil(period),
      frequencyDisabled: R.pipe(R.prop('display'), R.equals(false))(frequencyArtefact),
      defaultFrequency: frequency,
      availableFrequencies: R.keys(availableFrequencies),
      boundaries,
      period,
      labels: R.mergeRight(
        R.reduce(
          (memo, [id, message]) => R.assoc(id, formatMessage(intl)(message), memo),
          {},
          R.toPairs(periodMessages),
        ),
      )(availableFrequencies),
      changeFrequency: frequency =>
        changeFrequencyPeriod({
          valueId: frequency,
          period: [dateFns.startOfYear(R.head(period)), dateFns.endOfYear(R.last(period))],
        }),
      changePeriod: period => changeFrequencyPeriod({ valueId: frequency, period }),
    }),
  ),
  pure,
)(PeriodPicker);

export const LastNPeriod = compose(
  injectIntl,
  connect(
    createStructuredSelector({
      value: getLastNObservations,
      hasLastNObservations: getHasLastNObservations,
    }),
    { onChange: changeLastNObservations },
  ),
  withProps(({ intl }) => ({
    beforeLabel: formatMessage(intl)(messages.beforeLabel),
    afterLabel: formatMessage(intl)(messages.afterLabel),
    popperLabel: formatMessage(intl)(messages.popperLabel),
  })),
  branch(({ hasLastNObservations }) => R.not(hasLastNObservations), renderNothing),
)(InputNumber);

export const FilterPeriod = compose(
  injectIntl,
  connect(
    createStructuredSelector({
      activePanelId: getFilter,
      period: getPeriod,
      frequency: getFrequency,
      availableFrequencies: getAvailableFrequencies,
      datesBoundaries: getDatesBoundaries,
      timePeriodArtefact: getTimePeriodArtefact,
      frequencyArtefact: getFrequencyArtefact,
    }),
    { changeFilter, onChangeActivePanel: changeFilter },
  ),
  branch(R.pathEq(['timePeriodArtefact', 'display'], false), renderNothing),
  branch(R.pipe(R.prop('datesBoundaries'), R.isNil), renderNothing),
  withProps(
    ({
      intl,
      activePanelId,
      period,
      frequency,
      availableFrequencies,
      datesBoundaries,
      timePeriodArtefact,
      frequencyArtefact,
    }) => {
      const periodLabel = timePeriodArtefact?.label || formatMessage(intl)(messages.last);
      const frequencyLabel = frequencyArtefact?.label || formatMessage(intl)(messages.head);
      return {
        label: R.or(
          R.isEmpty(availableFrequencies),
          R.pipe(R.keys, R.length, R.equals(1))(availableFrequencies),
        )
          ? periodLabel
          : R.join(' & ')([frequencyLabel, periodLabel]),
        overflow: true,
        id: PANEL_PERIOD,
        isOpen: R.equals(PANEL_PERIOD, activePanelId),
        tag: R.not(R.isNil(datesBoundaries)) && (
          <Tag>
            {R.pipe(getIntervalPeriod(datesBoundaries), ([count, total]) =>
              tagAccessor(count, total),
            )(frequency, period)}
          </Tag>
        ),
      };
    },
  ),
)(ExpansionPanel);

//----------------------------------------------------------------------------------------------Side
const Side = ({ isNarrow, isRtl }) => (
  <Fragment>
    <FiltersCurrent isNarrow={isNarrow} isRtl={isRtl}>
      <UsedFilters />
      <PeriodFilters />
      <ClearFilters />
    </FiltersCurrent>
    <FilterPeriod isNarrow={isNarrow} isRtl={isRtl}>
      <Period />
      <LastNPeriod />
    </FilterPeriod>
    <Filters isNarrow={isNarrow} isRtl={isRtl} />
    <PanelDataAvailability>
      <ToggleDataAvailability />
    </PanelDataAvailability>
  </Fragment>
);

Side.propTypes = {
  isRtl: PropTypes.bool,
  isNarrow: PropTypes.bool,
};

export default Side;
