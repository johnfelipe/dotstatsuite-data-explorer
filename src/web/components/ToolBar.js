import React from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import html2canvas from 'html2canvas';
import { useDispatch, useSelector } from 'react-redux';
import { useIntl } from 'react-intl';
import { getRefAreaDimension } from '@sis-cc/dotstatsuite-sdmxjs';
import { useTheme } from '@material-ui/core';
import DEToolBar from './visions/DeToolBar';
import {
  changeActionId,
  changeDisplay,
  downloadExcel,
  downloadPng,
  changeFullscreen,
  changeViewer,
} from '../ducks/vis';
import { requestVisDataFile } from '../ducks/sdmx';
import { getViewer, getDisplay, getIsOverview } from '../selectors/router';
import { getIsPending } from '../selectors/app';
import { getDimensions, getExternalResources } from '../selectors/sdmx';
import {
  getIsFull,
  getVisActionId,
  getIsOpeningFullscreen,
  getAvailableCharts,
} from '../selectors';
import { getIsMicrodata, getHasMicrodata, getHasMicrodataData } from '../selectors/microdata';
import { ID_VIEWER_COMPONENT } from '../css-api';
import { MICRODATA, TABLE, CHART_IDS } from '../utils/constants';
import * as Settings from '../lib/settings';
import { formatMessage } from '../i18n';
import messages from './messages';

export const DOWNLOAD = 'download';
const EXCEL = 'excel';
const PNG = 'png';

const rangeLimit = R.last(Settings.sdmxRange) - R.head(Settings.sdmxRange) + 1;
const incompleteMessValues = { range: rangeLimit, cellsLimit: Settings.cellsLimit };

const enhanceTableProps = intl =>
  R.pipe(
    R.set(
      R.lensPath(['footerProps', 'copyright', 'label']),
      formatMessage(intl)(messages.footerCopyright),
    ),
    R.set(
      R.lensPath(['footerProps', 'copyright', 'content']),
      formatMessage(intl)(messages.viewerLinkLabel),
    ),
    R.set(
      R.lensPath(['footerProps', 'copyright', 'link']),
      formatMessage(intl)(messages.viewerLink),
    ),
    R.over(R.lensPath(['headerProps', 'disclaimer']), disclaimer =>
      R.isNil(disclaimer)
        ? null
        : formatMessage(intl)(messages.incompleteExcelData, incompleteMessValues),
    ),
  );

const ToolBar = ({ viewerProps }) => {
  const dispatch = useDispatch();
  const intl = useIntl();
  const theme = useTheme();

  const isDownloading = useSelector(getIsPending('requestingDataFile'));
  const isPendingExcel = useSelector(getIsPending(EXCEL));
  const isPendingPng = useSelector(getIsPending(PNG));
  const dimensions = useSelector(getDimensions);
  const viewerId = useSelector(getViewer);
  const availableCharts = useSelector(getAvailableCharts);
  const externalResources = useSelector(getExternalResources);
  const dimensionGetter = useSelector(getDisplay);
  const actionId = useSelector(getVisActionId());
  const isFull = useSelector(getIsFull());
  const isOpening = useSelector(getIsOpeningFullscreen);
  const hasMicrodata = useSelector(getHasMicrodata);
  const isMicrodata = useSelector(getIsMicrodata);
  const isOverview = useSelector(getIsOverview);
  const hasMicrodataData = useSelector(getHasMicrodataData);

  const csvSelection = {
    key: 'csv.selection',
    handler: () => dispatch(requestVisDataFile({ isDownloadAllData: false })),
  };
  const csvAll = {
    key: 'csv.all',
    handler: () => dispatch(requestVisDataFile({ isDownloadAllData: true })),
  };

  const build = () => {
    if (R.equals(TABLE, viewerId)) {
      const excelSelection = {
        key: 'excel.selection',
        handler: () =>
          dispatch(
            downloadExcel({
              ...enhanceTableProps(intl)(viewerProps),
              id: EXCEL,
              theme,
            }),
          ),
      };
      return [excelSelection, csvSelection, csvAll];
    }

    if (R.equals(MICRODATA, viewerId)) {
      return [
        { key: 'microdata.download.excel.selection' },
        { ...csvSelection, key: 'microdata.download.csv.selection' },
        { key: 'microdata.download.csv.all' },
      ];
    }

    if (R.includes(viewerId, CHART_IDS)) {
      // charts
      const pngSelection = {
        key: 'chart.selection',
        handler: () => {
          const { width, height } = viewerProps?.chartOptions?.base;
          let options = { scale: 2, scrollY: -window.scrollY };
          if (!R.isNil(width)) options.width = width;
          if (!R.isNil(height)) options.height = height;
          dispatch(
            downloadPng(
              () => html2canvas(document.getElementById(ID_VIEWER_COMPONENT), options),
              PNG,
            ),
          );
        },
      };
      return [pngSelection, csvSelection, csvAll];
    }
    return [csvSelection, csvAll];
  };

  return (
    <DEToolBar
      availableCharts={availableCharts}
      changeActionId={changeActionId}
      viewerId={viewerId}
      actionId={actionId}
      changeViewer={changeViewer}
      changeDisplay={changeDisplay}
      changeFullscreen={changeFullscreen}
      dimensionGetter={dimensionGetter}
      isFull={isFull}
      isOpening={isOpening}
      isDownloading={R.any(R.identity)([isPendingExcel, isPendingPng, isDownloading])}
      download={build()}
      hasRefAreaDimension={R.not(R.isNil(getRefAreaDimension({ dimensions })))}
      externalResources={externalResources}
      hasMicrodata={hasMicrodata}
      isMicrodata={isMicrodata}
      hasMicrodataData={hasMicrodataData}
      isOverview={isOverview}
    />
  );
};

ToolBar.propTypes = {
  viewerProps: PropTypes.object,
};

export default ToolBar;
