import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import * as R from 'ramda';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from './visions/SisccAppBar';
import Footer from './footer';
import { getAsset, getApp, locales, isAuthRequired } from '../lib/settings';
import { getLocale, getIsRtl, getPathname } from '../selectors/router';
import { getIsFull } from '../selectors';
import { changeLocale } from '../ducks/app';
import { useOidc, userSignIn } from '../lib/oidc';
import Vis from './vis';
import Search from './search';
import Share from '../share';
import { Route, Switch } from 'react-router';
import Page from './Page';
import { getUser } from '../selectors/app';
import { Button, Typography } from '@material-ui/core';
import { FormattedMessage } from '../i18n/index';

const useStyles = makeStyles(theme => ({
  root: {
    height: '100%',
  },
  content: {
    flexGrow: 1,
  },
  footerContainer: {
    zIndex: 0,
  },
  wrapper: {
    padding: theme.spacing(8),
  },
}));

const Component = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const localeId = useSelector(getLocale);
  const isRtl = useSelector(getIsRtl);
  const isFull = useSelector(getIsFull());

  const auth = useOidc();
  const user = useSelector(getUser);

  const isFixed = useSelector(getPathname) === '/vis';

  const userSignInProxy = () => {
    if (!auth)
      throw new Error('Authentication is required but oidc configuration is missing or erroneous');
    userSignIn(auth);
  };

  return (
    <Grid container wrap="nowrap" direction="column" className={classes.root}>
      {R.not(isFull) && (
        <Grid item>
          <AppBar
            isFixed={isFixed}
            logo={getAsset('header', localeId)}
            locales={R.keys(locales)}
            logoLink={getApp('headerLink', localeId)}
            localeId={localeId}
            isRtl={isRtl}
            changeLocale={localeId => dispatch(changeLocale(localeId))}
          />
        </Grid>
      )}
      {!isAuthRequired || (isAuthRequired && user) ? (
        <Switch>
          <Route exact path="/" component={Search} />
          <Route path="/vis" component={Vis} />
          <Route path="/share" component={Share} />
          <Route
            component={() => (
              <Page id="notFound">
                <Grid container direction="column" className={classes.wrapper}>
                  <Grid item container justifyContent="center">
                    <Typography variant="h6" align="center">
                      Page not found
                    </Typography>
                  </Grid>
                </Grid>
              </Page>
            )}
          />
        </Switch>
      ) : (
        <Page id="authRequired">
          <Grid container direction="column" className={classes.wrapper}>
            <Grid item container justifyContent="center">
              <Typography variant="h6" align="center">
                <FormattedMessage id="auth.required" />
              </Typography>
            </Grid>
            <Grid item container justifyContent="center">
              <Button onClick={userSignInProxy} variant="outlined" color="primary">
                <FormattedMessage id="user.login" />
              </Button>
            </Grid>
          </Grid>
        </Page>
      )}
      {R.not(isFull) && (
        <Grid item className={classes.footerContainer}>
          <Footer />
        </Grid>
      )}
    </Grid>
  );
};

export default Component;
