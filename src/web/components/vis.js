import React from 'react';
import * as R from 'ramda';
import { useSelector, useDispatch } from 'react-redux';
import { NoData } from '@sis-cc/dotstatsuite-visions';
import Typography from '@material-ui/core/Typography';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import FiltersHelp from './filters-help';
import Grid from '@material-ui/core/Grid';
import { FormattedMessage } from '../i18n';
import { getIsFull } from '../selectors';
import { resetSearch } from '../ducks/search';
import { requestData, resetDataflow } from '../ducks/sdmx';
import { requestMicrodata } from '../ducks/microdata';
import { LOG_ERROR_SDMX_STRUCTURE } from '../ducks/app';
import { getIsPending, getLog } from '../selectors/app';
import { getIsMicrodata } from '../selectors/microdata';
import { getLocale, getTerm, getSearchResultNb } from '../selectors/router';
import Loader from './loader';
import Side from './vis-side';
import * as Settings from '../lib/settings';
import { ParsedDataVisualisation } from './vis/vis-data';
import MicrodataTitle from './vis/microdata/title';
import NarrowFilters from './vis-side/side-container';
import DeAppBar from './visions/DeAppBar';
import Page from './Page';
import { ID_VIS_PAGE } from '../css-api';
import useEventListener from '../utils/useEventListener';
import ScrollToButtons from './vis/ScrollToButtons';
import { MARGE_RATIO, MARGE_SIZE, SIDE_WIDTH, SMALL_SIDE_WIDTH } from '../utils/constants';

const useStyles = makeStyles(theme => ({
  visPage: {
    overflow: 'visible',
    backgroundColor: theme.palette.background.default,
    marginTop: ({ isFull }) => (isFull ? 0 : 176),
    padding: `0 ${MARGE_SIZE}%`,
    zIndex: 1,
  },
  gutters: {
    display: 'flex',
    margin: 0,
    flexWrap: 'nowrap',
    [theme.breakpoints.down('sm')]: {
      flexWrap: 'wrap',
    },
  },
  visContainer: {
    marginTop: theme.spacing(-2.5),
    paddingTop: theme.spacing(2.5),
    backgroundColor: theme.palette.background.default,
  },
  space: {
    position: 'sticky',
    left: 0,
    backgroundColor: 'red',
    maxWidth: ({ width }) => width * MARGE_RATIO,
  },
  side: {
    [theme.breakpoints.down('sm')]: {
      width: '100%',
      maxWidth: ({ width }) => width * MARGE_RATIO,
      position: 'sticky',
      left: `${MARGE_SIZE}%`,
      '&::after': {
        content: '""',
        position: 'absolute',
        backgroundColor: theme.palette.background.default,
        top: '0',
        left: `-${MARGE_SIZE + 3}%`,
        width: `${MARGE_SIZE + 3}%`,
        height: '100%',
      },
      '&::before': {
        content: '""',
        position: 'absolute',
        backgroundColor: theme.palette.background.default,
        top: '0',
        right: `-${MARGE_SIZE + 3}%`,
        width: `${MARGE_SIZE + 3}%`,
        height: '100%',
      },
    },
    [theme.breakpoints.only('md')]: {
      marginRight: theme.spacing(2),
      minWidth: SMALL_SIDE_WIDTH,
      maxWidth: SMALL_SIDE_WIDTH,
    },
    [theme.breakpoints.up('lg')]: {
      marginRight: theme.spacing(2),
      minWidth: SIDE_WIDTH,
      maxWidth: SIDE_WIDTH,
    },
  },
}));

export default () => {
  const dispatch = useDispatch();
  const theme = useTheme();
  const localeId = useSelector(getLocale);
  const searchResultNb = useSelector(getSearchResultNb);
  const term = useSelector(getTerm);
  const isNarrow = useMediaQuery(theme.breakpoints.down('sm'));
  const isFull = useSelector(getIsFull());
  const isMicrodata = useSelector(getIsMicrodata);
  const visPageRef = React.useRef(null);
  const [scrollsPos, setScrollsPos] = React.useState({ scrollY: 0, scrollX: 0 });
  const [visPageWidth, setVisPageWidth] = React.useState(0);
  const classes = useStyles({ width: visPageWidth, isFull });
  const goHome = () => dispatch(resetSearch());
  const goSearch = () => dispatch(resetDataflow());
  const goBack = searchResultNb <= 1 && !term ? goHome : goSearch;

  useEventListener('resize', () => {
    if (visPageRef.current) {
      setVisPageWidth(visPageRef.current.offsetWidth);
    }
  });
  useEventListener('scroll', () =>
    setScrollsPos({
      scrollY: window.scrollY,
      scrollX: window.scrollX,
    }),
  );

  React.useLayoutEffect(() => {
    if (visPageRef.current) {
      setVisPageWidth(visPageRef.current.offsetWidth);
    }
  }, []);

  React.useEffect(() => {
    if (isMicrodata) dispatch(requestMicrodata({ shouldRequestStructure: true }));
    else dispatch(requestData({ shouldRequestStructure: true }));
  }, []);

  const isLoadingStructure = useSelector(getIsPending('getStructure'));
  const isLoadingData = useSelector(getIsPending('getData'));
  const log = useSelector(getLog(LOG_ERROR_SDMX_STRUCTURE));

  let errorMessage = null;
  if (log) {
    const status = R.path(['log', 'status'], log);
    if (status === 404) errorMessage = <FormattedMessage id="log.error.sdmx.404" />;
    else if (R.includes(status, [401, 402, 403]))
      errorMessage = <FormattedMessage id="log.error.sdmx.40x" />;
    else errorMessage = <FormattedMessage id="log.error.sdmx.xxx" />;
  }
  return (
    <React.Fragment>
      {isLoadingStructure && (
        <Typography tabIndex={0} aria-live="assertive" variant="srOnly">
          <FormattedMessage id="de.visualisation.loading" />
        </Typography>
      )}

      {!isFull && (
        <DeAppBar
          isFixed
          goHome={goHome}
          goBack={goBack}
          goBackLabel={<FormattedMessage id="de.search.back" />}
          logo={Settings.getAsset('subheader', localeId)}
        />
      )}
      <Page id={ID_VIS_PAGE} classNames={[classes.visPage]} ref={visPageRef}>
        {errorMessage && (
          <div>
            <div className={classes.gutters}>
              <NoData message={errorMessage} />
            </div>
          </div>
        )}

        {isLoadingStructure && (
          <div>
            <div className={classes.gutters}>
              <NoData
                icon={<Loader style={{ marginTop: 24 }} />}
                message={<FormattedMessage id="de.visualisation.loading" />}
              />
            </div>
          </div>
        )}

        {!errorMessage && !isLoadingStructure && (
          <Grid container>
            {R.not(isFull) && R.not(isMicrodata) && (
              <div className={classes.gutters}>
                {!isNarrow && <FiltersHelp />}
                <div className={classes.space}></div>
              </div>
            )}
            <div className={classes.gutters} wrap={isNarrow ? 'wrap' : 'nowrap'}>
              {R.not(isFull) && R.not(isMicrodata) && (
                <div className={classes.side}>
                  <NarrowFilters isNarrow={isNarrow}>
                    <Side />
                  </NarrowFilters>
                </div>
              )}
              <Grid item xs={12} className={classes.visContainer}>
                {isMicrodata && !isLoadingData && <MicrodataTitle />}
                <ParsedDataVisualisation visPageWidth={visPageWidth} />
              </Grid>
            </div>
            <ScrollToButtons scrollsPos={scrollsPos} />
          </Grid>
        )}
      </Page>
    </React.Fragment>
  );
};
