import React from 'react';
import PropTypes from 'prop-types';
import { defineMessages, useIntl } from 'react-intl';
import { Tooltip } from '@sis-cc/dotstatsuite-visions';
import HelpIcon from '@material-ui/icons/Help';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import LensIcon from '@material-ui/icons/Lens';
import { FormattedMessage, formatMessage } from '../i18n';

const messages = defineMessages({
  help: { id: 'wcag.filters.search.help' },
});

const FILTERS_ID = 'filters';

const useStyles = makeStyles(() => ({
  root: {
    display: 'flex',
  },
}));

const FiltersHelp = ({ isSearch }) => {
  const classes = useStyles();
  const intl = useIntl();

  return (
    <Grid container>
      <Typography variant="body2" className={classes.root} id={FILTERS_ID} tabIndex={0}>
        <FormattedMessage id="de.side.filters.action" />
        &nbsp;
      </Typography>
      {isSearch && (
        <Tooltip
          variant="light"
          tabIndex={0}
          aria-label={formatMessage(intl)(messages.help)}
          aria-hidden={false}
          placement="bottom-start"
          title={
            <Typography id="filtersHelpers" variant="body2">
              <FormattedMessage
                id="de.filters.search.help"
                values={{ icon: <LensIcon style={{ fontSize: 5 }} /> }}
              />
            </Typography>
          }
        >
          <HelpIcon fontSize="small" />
        </Tooltip>
      )}
    </Grid>
  );
};

FiltersHelp.propTypes = {
  isSearch: PropTypes.bool,
};

export default FiltersHelp;
