import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import { VerticalButton } from '@sis-cc/dotstatsuite-visions';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import MuiMenu from '@material-ui/core/Menu';

const useStyles = makeStyles(theme => ({
  button: {
    padding: theme.spacing(0.5, 0.5),
    [theme.breakpoints.down('md')]: {
      minWidth: 48,
    },
    [theme.breakpoints.down('xs')]: {
      minWidth: 36,
    },
  },
}));

export const Button = props => {
  const classes = useStyles();
  const isXS = useMediaQuery(theme => theme.breakpoints.down('xs'));
  const isMD = useMediaQuery(theme => theme.breakpoints.only('md'));

  return (
    <VerticalButton className={classes.button} color="primary" {...props}>
      {isXS || isMD ? null : props.children}
    </VerticalButton>
  );
};

Button.propTypes = {
  children: PropTypes.node,
};

export const Menu = props => (
  <MuiMenu
    keepMounted
    getContentAnchorEl={null}
    anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
    transformOrigin={{ vertical: 'top', horizontal: 'center' }}
    {...props}
  >
    {props.children}
  </MuiMenu>
);

Menu.propTypes = {
  children: PropTypes.oneOfType([PropTypes.node, PropTypes.arrayOf(PropTypes.node)]),
};
