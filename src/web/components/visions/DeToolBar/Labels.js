import React from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import LocalOfferOutlinedIcon from '@material-ui/icons/LocalOfferOutlined';
import ListItemText from '@material-ui/core/ListItemText';
import MenuItem from '@material-ui/core/MenuItem';
import { defineMessages, useIntl } from 'react-intl';
import { FormattedMessage, formatMessage } from '../../../i18n';
import { Button, Menu } from './helpers';

const messages = defineMessages({
  label: { id: 'vx.config.display.label' },
  code: { id: 'vx.config.display.code' },
  both: { id: 'vx.config.display.both' },
});

const Component = ({ label, changeLabel }) => {
  const intl = useIntl();

  const [anchorEl, setAnchorEl] = React.useState(null);

  const openMenu = event => {
    setAnchorEl(event.currentTarget);
  };

  const closeMenu = () => {
    setAnchorEl(null);
  };

  const itemClick = (onClickHandler, ...args) => () => {
    if (R.is(Function)(onClickHandler)) onClickHandler(...args);
    closeMenu();
  };

  return (
    <React.Fragment>
      <Button
        startIcon={<LocalOfferOutlinedIcon />}
        selected={Boolean(anchorEl)}
        onClick={openMenu}
        aria-haspopup="true"
      >
        <FormattedMessage id="de.visualisation.toolbar.action.labels" />
      </Button>
      <Menu anchorEl={anchorEl} open={Boolean(anchorEl)} onClose={closeMenu}>
        {R.map(id => (
          <MenuItem
            key={id}
            onClick={itemClick(changeLabel, id)}
            selected={R.equals(label, id)}
            dense
            aria-labelledby={id}
          >
            <ListItemText id={id} primaryTypographyProps={{ color: 'primary' }}>
              <span>{formatMessage(intl)(messages[id])}</span>
            </ListItemText>
          </MenuItem>
        ))(['label', 'code', 'both'])}
      </Menu>
    </React.Fragment>
  );
};

Component.propTypes = {
  changeLabel: PropTypes.func.isRequired,
  label: PropTypes.string,
};

export default Component;
