import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';
import * as R from 'ramda';
import { shareEndpoint } from '../../../lib/settings';
import { makeStyles } from '@material-ui/core/styles';
import Toolbar from '@material-ui/core/Toolbar';
import TuneIcon from '@material-ui/icons/Tune';
import ShareIcon from '@material-ui/icons/Share';
import TableChartOutlinedIcon from '@material-ui/icons/TableChartOutlined';
import DeveloperModeOutlinedIcon from '@material-ui/icons/DeveloperModeOutlined';
import ListAltIcon from '@material-ui/icons/ListAlt';
import FullscreenIcon from '@material-ui/icons/Fullscreen';
import FullscreenExitIcon from '@material-ui/icons/FullscreenExit';
import GridOnOutlinedIcon from '@material-ui/icons/GridOnOutlined';
import { Button } from './helpers';
import { FormattedMessage } from '../../../i18n';
import Labels from './Labels';
import Charts from './Charts';
import Downloads from './Downloads';
import { chartIsVisible } from '../../../lib/settings';
// avoid MICRODATA repetition but requires to pass as a prop to avoid import if pushed to visions
import { OVERVIEW, TABLE, MICRODATA } from '../../../utils/constants';

const FULLSCREEN = 'fullscreen';
const SHARE = 'share';
const CONFIG = 'config';
const API = 'api';

const useStyles = makeStyles(() => ({
  root: {
    display: 'flex',
    justifyContent: 'space-between',
  },
}));

const Component = props => {
  const classes = useStyles();
  const dispatch = useDispatch();
  // icon dry
  // usemenu dry
  // xs display
  useEffect(() => {
    if (R.and(R.isNil(props.isOpening), props.isFull)) {
      document.getElementById(FULLSCREEN).focus();
      props.changeFullscreen(true, true);
    }
  }, [props.isFull]);

  return (
    <Toolbar data-testid="detoolbar" className={classes.root} disableGutters>
      <div>
        <Button
          startIcon={<ListAltIcon />}
          onClick={() => dispatch(props.changeViewer(OVERVIEW))}
          selected={R.equals(props.viewerId, OVERVIEW)}
        >
          <FormattedMessage id="de.visualisation.toolbar.overview" />
        </Button>
        <Button
          startIcon={<TableChartOutlinedIcon />}
          onClick={() => dispatch(props.changeViewer(TABLE))}
          selected={R.equals(props.viewerId, TABLE)}
        >
          <FormattedMessage id="de.visualisation.toolbar.table" />
        </Button>
        {props.hasMicrodata && (
          <Button
            id={MICRODATA}
            startIcon={<GridOnOutlinedIcon />}
            onClick={() => dispatch(props.changeViewer(MICRODATA))}
            selected={props.isMicrodata}
            aria-pressed={props.isMicrodata}
            disabled={R.not(props.hasMicrodataData)}
          >
            <FormattedMessage id="de.visualisation.toolbar.microdata" />
          </Button>
        )}
        {chartIsVisible && (
          <Charts
            availableCharts={props.availableCharts}
            chart={props.viewerId}
            changeChart={(type, options) => dispatch(props.changeViewer(type, options))}
            selected={!R.any(R.equals(props.viewerId), [TABLE, MICRODATA, OVERVIEW])}
            hasRefAreaDimension={props.hasRefAreaDimension}
          />
        )}
      </div>
      <div>
        <Labels
          label={props.dimensionGetter}
          changeLabel={id => dispatch(props.changeDisplay(id))}
        />
        {!props.isMicrodata && !props.isOverview && (
          <Button
            startIcon={<TuneIcon />}
            onClick={() => dispatch(props.changeActionId(CONFIG))}
            aria-expanded={R.equals(props.actionId, CONFIG)}
            selected={R.equals(props.actionId, CONFIG)}
          >
            <FormattedMessage id="de.visualisation.toolbar.action.customize" />
          </Button>
        )}
        {shareEndpoint && !props.isMicrodata && !props.isOverview && (
          <Button
            startIcon={<ShareIcon />}
            onClick={() => dispatch(props.changeActionId(SHARE))}
            selected={R.equals(props.actionId, SHARE)}
            aria-expanded={R.equals(props.actionId, SHARE)}
          >
            <FormattedMessage id="de.visualisation.toolbar.action.share" />
          </Button>
        )}
        {R.or(!R.isEmpty(props.download), !R.isEmpty(props.externalResources)) && (
          <Downloads
            loading={props.isDownloading}
            download={props.download}
            externalResources={props.externalResources}
          />
        )}
        {!props.isMicrodata && (
          <Button
            id={API}
            startIcon={<DeveloperModeOutlinedIcon />}
            onClick={() => dispatch(props.changeActionId(API))}
            selected={R.equals(props.actionId, API)}
            aria-expanded={R.equals(props.actionId, API)}
          >
            <FormattedMessage id="de.visualisation.toolbar.action.apiqueries" />
          </Button>
        )}
        <Button
          id={FULLSCREEN}
          startIcon={props.isFull ? <FullscreenExitIcon /> : <FullscreenIcon />}
          onClick={() => dispatch(props.changeFullscreen(R.not(props.isFull)))}
          selected={props.isFull}
          aria-pressed={props.isFull}
        >
          <FormattedMessage id="de.visualisation.toolbar.action.fullscreen" />
        </Button>
        {/*<More actionId={props.actionId} changeActionId={props.changeActionId} />*/}
      </div>
    </Toolbar>
  );
};

Component.propTypes = {
  availableCharts: PropTypes.object.isRequired,
  changeActionId: PropTypes.func.isRequired,
  viewerId: PropTypes.string,
  actionId: PropTypes.string,
  changeViewer: PropTypes.func.isRequired,
  changeDisplay: PropTypes.func.isRequired,
  changeFullscreen: PropTypes.func.isRequired,
  dimensionGetter: PropTypes.string,
  isFull: PropTypes.bool,
  isOpening: PropTypes.bool,
  isDownloading: PropTypes.bool,
  download: PropTypes.array.isRequired,
  hasRefAreaDimension: PropTypes.bool,
  externalResources: PropTypes.array,
  hasMicrodata: PropTypes.bool,
  isMicrodata: PropTypes.bool,
  hasMicrodataData: PropTypes.bool,
  isOverview: PropTypes.bool,
};

export default Component;
