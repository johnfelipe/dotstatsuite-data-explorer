import React from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import { makeStyles } from '@material-ui/core/styles';
import GetAppIcon from '@material-ui/icons/GetApp';
import MenuItem from '@material-ui/core/MenuItem';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import MuiButton from '@material-ui/core/Button';
import { defineMessages, useIntl } from 'react-intl';
import { FormattedMessage, formatMessage } from '../../../i18n';
import { Button, Menu } from './helpers';

const messages = defineMessages({
  'csv.selection': { id: 'de.visualisation.toolbar.action.download.csv.selection' },
  'csv.all': { id: 'de.visualisation.toolbar.action.download.csv.all' },
  'excel.selection': { id: 'de.visualisation.toolbar.action.download.excel.selection' },
  'chart.selection': { id: 'de.visualisation.toolbar.action.download.chart.selection' },
  'microdata.download.excel.selection': { id: 'microdata.download.excel.selection' },
  'microdata.download.csv.selection': { id: 'microdata.download.csv.selection' },
  'microdata.download.csv.all': { id: 'microdata.download.csv.all' },
});

const useStyles = makeStyles(theme => ({
  link: {
    margin: 0,
    padding: theme.spacing(0.75, 2),
    width: '100%',
    height: '100%',
    display: 'flex',
    justifyContent: 'end',
  },
  linkIcon: {
    height: 20,
    paddingRight: theme.spacing(1),
    alignItems: 'center',
  },
}));

const Component = ({ download, loading, externalResources }) => {
  const classes = useStyles();
  const intl = useIntl();
  const [anchorEl, setAnchorEl] = React.useState(null);

  const openMenu = event => {
    setAnchorEl(event.currentTarget);
  };

  const closeMenu = () => {
    setAnchorEl(null);
  };

  const itemClick = (onClickHandler, ...args) => () => {
    if (R.is(Function)(onClickHandler)) onClickHandler(...args);
    closeMenu();
  };

  return (
    <React.Fragment>
      <Button
        startIcon={<GetAppIcon />}
        selected={Boolean(anchorEl)}
        loading={loading}
        onClick={openMenu}
        aria-haspopup="true"
      >
        <FormattedMessage id="de.visualisation.toolbar.action.download" />
      </Button>
      <Menu anchorEl={anchorEl} open={Boolean(anchorEl)} onClose={closeMenu}>
        {R.map(
          ({ key, handler }) => (
            <MenuItem
              key={key}
              onClick={itemClick(handler)}
              dense
              disabled={!R.is(Function, handler)}
            >
              <ListItemText primaryTypographyProps={{ color: 'primary' }}>
                <span>{formatMessage(intl)(messages[key])}</span>
              </ListItemText>
            </MenuItem>
          ),
          download,
        )}
        {R.not(R.isEmpty(externalResources)) && <Divider />}
        {R.not(R.isEmpty(externalResources)) &&
          R.map(
            ({ id, label, link, img }) => (
              <MuiButton
                key={id}
                component="a"
                color="primary"
                target="_blank"
                href={link}
                className={classes.link}
                id={id}
                tabIndex={0}
                download
              >
                {R.not(R.isNil(img)) && (
                  <img src={img} className={classes.linkIcon} alt={`${id} icon`} />
                )}
                {label}
              </MuiButton>
            ),
            externalResources,
          )}
      </Menu>
    </React.Fragment>
  );
};

Component.propTypes = {
  download: PropTypes.array.isRequired,
  loading: PropTypes.bool,
  externalResources: PropTypes.array,
};

export default Component;
