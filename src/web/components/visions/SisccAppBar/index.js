import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import * as R from 'ramda';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import Divider from '@material-ui/core/Divider';
import LanguageIcon from '@material-ui/icons/Language';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core';
import { useIntl } from 'react-intl';
import { formatMessage } from '../../../i18n';
import User from '../../user';
import AccessibilityButton from '../../AccessibilityButton';
import useStyles from './useStyles';
import Link from '@material-ui/core/Link';
import { messages } from '../../../i18n/messages';
import { useOidc } from '../../../lib/oidc/index';

const Input = id => {
  const intl = useIntl();
  const classes = useStyles();
  const theme = useTheme();
  const isXs = useMediaQuery(theme.breakpoints.down('xs'));

  if (isXs) return <LanguageIcon className={classes.value} color="primary" />;
  return (
    <span className={classes.value}>
      <LanguageIcon color="primary" />
      {R.has(id, messages) ? formatMessage(intl)(messages[id]) : id}
    </span>
  );
};

Input.propTypes = {
  id: PropTypes.string,
};

const Component = ({ logo, locales, localeId, changeLocale, logoLink, isFixed }) => {
  const classes = useStyles();
  const intl = useIntl();
  const auth = useOidc();

  return (
    <AppBar
      data-testid="sisccappbar"
      position="static"
      className={cx(classes.appBar, { [classes.fixed]: isFixed })}
      elevation={0}
    >
      <Toolbar className={classes.toolBar}>
        <div className={classes.logoWrapper}>
          {logoLink ? (
            <Link href={logoLink} target="_blank" rel="noopener noreferrer">
              {' '}
              <img
                className={cx(classes.logo, classes.alternativeBrowserLogo)}
                src={logo}
                alt="siscc logo"
              />{' '}
            </Link>
          ) : (
            <img
              className={cx(classes.logo, classes.alternativeBrowserLogo)}
              src={logo}
              alt="siscc logo"
            />
          )}
        </div>
        <AccessibilityButton />
        {auth && (
          <React.Fragment>
            <Divider orientation="vertical" flexItem className={classes.divider} />
            <User />
          </React.Fragment>
        )}
        <Divider orientation="vertical" flexItem className={classes.divider} />
        <TextField
          select
          id="languages" // TextField should always have id (accessibility)
          className={classes.textField}
          value={localeId}
          variant="outlined"
          onChange={event => changeLocale(event.target.value)}
          inputProps={{
            renderValue: Input,
          }}
          SelectProps={{
            classes: { root: classes.select, iconOutlined: classes.arrowDown },
            MenuProps: {
              getContentAnchorEl: null,
              anchorOrigin: { vertical: 'bottom', horizontal: 'center' },
              transformOrigin: { vertical: 'top', horizontal: 'center' },
              classes: { paper: classes.paper },
            },
          }}
        >
          {R.map(id => (
            <MenuItem key={id} id={id} value={id} dense className={classes.menuItem}>
              <span>{R.has(id, messages) ? formatMessage(intl)(messages[id]) : id}</span>
            </MenuItem>
          ))(locales)}
        </TextField>
      </Toolbar>
    </AppBar>
  );
};

Component.propTypes = {
  logo: PropTypes.string,
  locales: PropTypes.array.isRequired,
  localeId: PropTypes.string,
  changeLocale: PropTypes.func.isRequired,
  logoLink: PropTypes.string,
  isFixed: PropTypes.bool,
};

export default Component;
