import React from 'react';
import { useSelector } from 'react-redux';
import cx from 'classnames';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import ButtonBase from '@material-ui/core/ButtonBase';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import Link from '@material-ui/core/Link';
import useStyles from './useStyles';
import { getIsRtl } from '../../../selectors/router';
import { ID_APPBAR } from '../../../css-api';

const Component = ({ logo, goHome, goBack, goBackLabel, children, isFixed }) => {
  const classes = useStyles();
  const isRtl = useSelector(getIsRtl);

  return (
    <div className={cx({ [classes.fixed]: isFixed })} id={ID_APPBAR}>
      <AppBar data-testid="deappbar" position="static" className={classes.appBar} elevation={0}>
        <Toolbar className={classes.toolBar}>
          <div className={classes.logoWrapper}>
            <ButtonBase disableRipple onClick={goHome}>
              <img
                className={cx(classes.logo, classes.alternativeBrowserLogo)}
                src={logo}
                alt="delogo"
              />
            </ButtonBase>
          </div>
          {children}
        </Toolbar>
      </AppBar>
      {R.is(Function, goBack) && (
        <Toolbar variant="dense" className={classes.toolBar}>
          <Link
            underline="none"
            classes={{ underlineNone: classes.backLink }}
            component="button"
            onClick={goBack}
            variant="body2"
            color="textSecondary"
          >
            {isRtl ? (
              <ArrowForwardIosIcon className={classes.backIcon} />
            ) : (
              <ArrowBackIosIcon className={classes.backIcon} />
            )}
            {goBackLabel}
          </Link>
        </Toolbar>
      )}
    </div>
  );
};

Component.propTypes = {
  isFixed: PropTypes.bool,
  logo: PropTypes.string,
  goHome: PropTypes.func.isRequired,
  goBack: PropTypes.func,
  goBackLabel: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
  children: PropTypes.oneOfType([PropTypes.array, PropTypes.element]),
};

export default Component;
