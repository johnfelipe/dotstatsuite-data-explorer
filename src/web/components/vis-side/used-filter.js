import React from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { branch, compose, withProps, pure, renderNothing } from 'recompose';
import * as R from 'ramda';
import { injectIntl } from 'react-intl';
import { Chips, ExpansionPanel, Tag } from '@sis-cc/dotstatsuite-visions';
import { formatMessage } from '../../i18n';
import { changeDataquery, resetFilters, resetSpecialFilters } from '../../ducks/sdmx';
import { changeFilter } from '../../ducks/vis';
import { PANEL_USED_FILTERS } from '../../utils/constants';
import {
  getLastNObservations,
  getLocale,
  getPeriod as getRouterPeriod,
} from '../../selectors/router';
import {
  getFrequency,
  getSelection,
  getFrequencyArtefact,
  getTimePeriodArtefact,
  getAvailableFrequencies,
} from '../../selectors/sdmx';
import { getFilter, getVisDimensionFormat } from '../../selectors';
import { countNumberOf, getFilterLabel } from '../../utils';
import {
  START_PERIOD,
  END_PERIOD,
  LASTN,
  addI18nLabels,
  getUsedFilterPeriod,
  getUsedFilterFrequency,
} from '../../utils/used-filter';
import { locales } from '../../lib/settings';
import { applyFormat, sdmxFormat } from '../../lib/sdmx/frequency';
import { getDateLocale } from '../../i18n/dates';
import messages from '../messages';

const withFilterFrequency = withProps(({ frequency, frequencyArtefact, availableFrequencies }) => ({
  frequencyFilter: getUsedFilterFrequency(frequency, availableFrequencies)(frequencyArtefact),
}));

const withFilterPeriod = withProps(({ frequency, intl, period, lastN, locale, timePeriod }) => ({
  periodFilter: getUsedFilterPeriod(
    frequency,
    period,
    lastN,
    applyFormat(
      frequency,
      {
        ...sdmxFormat,
        M: R.pathOr(sdmxFormat.M, [locale, 'timeFormat'], locales),
      },
      { locale: getDateLocale(locale) },
    ),
    timePeriod,
  ),
  periodLabels: {
    [START_PERIOD]: formatMessage(intl)(messages.periodStart),
    [END_PERIOD]: formatMessage(intl)(messages.periodEnd),
    [LASTN]: [formatMessage(intl)(messages.periodLast), formatMessage(intl)(messages.periods)],
  },
}));

//--------------------------------------------------------------------------------------------------
export const UsedFilters = compose(
  injectIntl,
  connect(
    createStructuredSelector({
      items: getSelection,
      labelAccessor: getVisDimensionFormat(),
    }),
    { onDelete: changeDataquery },
  ),
  withProps(({ intl, labelAccessor }) => ({
    labelRenderer: getFilterLabel(labelAccessor),
    labels: {
      reducingChip: formatMessage(intl)(messages.reducingChip),
    },
  })),
  pure,
)(Chips);

export const PeriodFilters = compose(
  injectIntl,
  connect(
    createStructuredSelector({
      frequency: getFrequency,
      frequencyArtefact: getFrequencyArtefact,
      period: getRouterPeriod,
      lastN: getLastNObservations,
      locale: getLocale,
      timePeriod: getTimePeriodArtefact,
      availableFrequencies: getAvailableFrequencies,
    }),
    { onDelete: resetSpecialFilters },
  ),
  branch(R.pipe(R.prop('timePeriod'), R.isNil), renderNothing),
  branch(R.pathEq(['timePeriodArtefact', 'display'], false), renderNothing),
  withFilterFrequency,
  withFilterPeriod,
  withProps(({ intl, frequencyFilter = [], periodFilter = [], periodLabels }) => ({
    items: R.concat(frequencyFilter, addI18nLabels(periodLabels)(periodFilter)),
    clearAllLabel: formatMessage(intl)(messages.clear),
  })),
  pure,
)(Chips);

export const ClearFilters = compose(
  injectIntl,
  connect(null, { onDeleteAll: resetFilters }),
  withProps(({ intl }) => ({ clearAllLabel: formatMessage(intl)(messages.clear) })),
  pure,
)(Chips);

export const FiltersCurrent = compose(
  injectIntl,
  connect(
    createStructuredSelector({
      activePanelId: getFilter,
      selection: getSelection,
      frequency: getFrequency,
      frequencyArtefact: getFrequencyArtefact,
      period: getRouterPeriod,
      lastN: getLastNObservations,
      locale: getLocale,
      timePeriod: getTimePeriodArtefact,
      availableFrequencies: getAvailableFrequencies,
    }),
    { onChangeActivePanel: changeFilter },
  ),
  withFilterFrequency,
  withFilterPeriod,
  withProps(({ selection = [], frequencyFilter = [], periodFilter = [] }) => ({
    counter: countNumberOf(R.concat(selection, R.concat(frequencyFilter, periodFilter))),
  })),
  branch(({ counter }) => counter === 0, renderNothing),
  withProps(({ intl, counter, activePanelId }) => ({
    isOpen: R.equals(PANEL_USED_FILTERS, activePanelId),
    id: PANEL_USED_FILTERS,
    label: formatMessage(intl)(messages.title),
    tag: <Tag>{counter}</Tag>,
  })),
)(ExpansionPanel);
