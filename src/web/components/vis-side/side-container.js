import React from 'react';
import PropTypes from 'prop-types';
import { ExpansionPanel } from '@sis-cc/dotstatsuite-visions';
import { useIntl } from 'react-intl';
import { PANEL_NARROW_FILTER } from '../../utils/constants';
import { formatMessage } from '../../i18n';

const messages = {
  label: { id: 'de.side.filters.action' },
};

const NarrowFilters = ({ isNarrow, children }) => {
  const intl = useIntl();
  if (isNarrow) {
    return (
      <ExpansionPanel
        key={PANEL_NARROW_FILTER}
        id={PANEL_NARROW_FILTER}
        label={formatMessage(intl)(messages.label)}
        overflow
      >
        {children}
      </ExpansionPanel>
    );
  }
  return children;
};

NarrowFilters.propTypes = {
  isNarrow: PropTypes.bool,
  children: PropTypes.node,
};

export default NarrowFilters;
