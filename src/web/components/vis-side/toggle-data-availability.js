import React from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose, withProps } from 'recompose';
import { injectIntl } from 'react-intl';
import Checkbox from '@material-ui/core/Checkbox';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import { withStyles } from '@material-ui/core/styles';
import { ExpansionPanel } from '@sis-cc/dotstatsuite-visions';
import { applyDataAvailability } from '../../ducks/sdmx';
import { changeFilter } from '../../ducks/vis';
import { getHasDataAvailability, getIsDataAvaibilityInState } from '../../selectors/router';
import { getFilter } from '../../selectors';
import { PANEL_CONTENT_CONSTRAINTS } from '../../utils/constants';
import { formatMessage } from '../../i18n';
import { defineMessages } from 'react-intl';

const messages = defineMessages({
  label: { id: 'de.contentConstraints.checkbox.label' },
  title: { id: 'de.panel.contentConstraints.title' },
});

const styles = {
  root: {
    marginLeft: 0,
  },
};

export const ComponentDataAvailability = props => (
  <FormControl component="fieldset">
    <FormControlLabel
      classes={{ root: props.classes.root }}
      checked={props.isChecked}
      control={
        <Checkbox
          onChange={() => props.onChange(!props.isChecked)}
          value="hasDataAvailability"
        />
      }
      label={props.label}
    />
  </FormControl>
);

ComponentDataAvailability.propTypes = {
  label: PropTypes.string,
  onChange: PropTypes.func,
  isChecked: PropTypes.bool,
  classes: PropTypes.object,
};

export const ToggleDataAvailability = compose(
  injectIntl,
  connect(
    createStructuredSelector({
      dataAvailability: getHasDataAvailability,
    }),
    { onChange: applyDataAvailability },
  ),
  withProps(({ intl, dataAvailability }) => ({
    isChecked: dataAvailability,
    label: formatMessage(intl)(messages.label),
  })),
  withStyles(styles),
)(ComponentDataAvailability);

export const PanelDataAvailability = compose(
  injectIntl,
  connect(
    createStructuredSelector({
      activePanelId: getFilter,
      isActive: getIsDataAvaibilityInState,
    }),
    { onChangeActivePanel: changeFilter },
  ),
  withProps(({ intl, activePanelId, isActive }) => ({
    isOpen: R.equals(PANEL_CONTENT_CONSTRAINTS, activePanelId),
    id: PANEL_CONTENT_CONSTRAINTS,
    label: formatMessage(intl)(messages.title),
    isBlank: R.not(isActive),
  })),
)(ExpansionPanel);
