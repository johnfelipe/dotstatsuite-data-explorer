import React, { Fragment, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import PropTypes from 'prop-types';
import { FormattedMessage, formatMessage } from '../../i18n';
import { compose, withProps, pure } from 'recompose';
import * as R from 'ramda';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import {
  Spotlight,
  Chips,
  ScopeList,
  ExpansionPanel,
  NoData,
  spotlightScopeListEngine,
  Tag,
} from '@sis-cc/dotstatsuite-visions';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import { getDataflows } from '../../selectors/search';
import { PANEL_SEARCH_CURRENT } from '../../utils/constants';
import NarrowFilters from '../vis-side/side-container';
import DeAppBar from '../visions/DeAppBar';
import FiltersHelp from '../filters-help';
import Loader from '../loader';
import Dataflows from './dataflows';
import Pagination from './pagination';
import { countNumberOf } from '../../utils';
import { getIsRtl, getSearchResultNb } from '../../selectors/router';
import Page from '../Page';
import { ID_SEARCH_PAGE } from '../../css-api';
import { setLabel, getAccessor } from './utils';
import messages from '../messages';
import { setOfHideHomeAndResultFacetItemIDs } from '../../lib/settings/index';
import Sort from './Sort';
import { changeDataflow } from '../../ducks/sdmx';

const tagAccessor = (count, total) => (
  <FormattedMessage id="de.filter.tag" values={{ count, total }} />
);

const Facet = pure(ScopeList);
const Facets = compose(
  withProps(({ intl }) => ({
    labels: {
      navigateNext: formatMessage(intl)(messages.next),
      backHelper: formatMessage(intl)(messages.backHelper),
      childrenNavigateDesc: formatMessage(intl)(messages.childrenNavigateDesc),
    },
    topElementProps: {
      fullWidth: true,
      hasClearAll: true,
      mainPlaceholder: formatMessage(intl)(messages.primary),
      secondaryPlaceholder: formatMessage(intl)(messages.secondary),
      defaultSpotlight: {
        engine: spotlightScopeListEngine,
        fields: {
          label: {
            id: 'label',
            accessor: R.pipe(R.propOr(null, 'label'), R.ifElse(R.isNil, R.always(''), R.identity)),
            isSelected: true,
          },
        },
      },
    },
    topElementComponent: values => (R.gte(R.length(values), 8) ? Spotlight : null),
  })),
)(({ topElementComponent, facets = [], intl, ...parentProps }) =>
  R.map(
    ({ id, label, values, isPinned }) => (
      <Facet
        {...parentProps}
        id={id}
        key={id}
        label={R.prop('label', setLabel({ intl })({ label, id }))}
        expansionPanelProps={{ isPinned, pinnedLabel: formatMessage(intl)(messages.pinned) }}
        items={values}
        hasPath={false}
        TopElementComponent={topElementComponent(values)}
        displayAccessor={R.pipe(R.prop('isDisabled'), R.not)}
        tagAccessor={tagAccessor}
        labelRenderer={props => getAccessor(setOfHideHomeAndResultFacetItemIDs.has(id))(props)}
      />
    ),
    facets,
  ),
);

const useStyles = makeStyles(theme => ({
  margin: {
    margin: 0,
  },
  side: {
    paddingTop: 37,
    [theme.breakpoints.between('xs', 'sm')]: {
      paddingTop: 0,
      minWidth: '100%',
    },
    [theme.breakpoints.only('md')]: {
      paddingRight: theme.spacing(2),
      minWidth: '40%',
      maxWidth: '40%',
    },
    [theme.breakpoints.up('lg')]: {
      paddingRight: theme.spacing(2),
      minWidth: '33.33%',
      maxWidth: '33.33%',
    },
  },
  main: {
    width: '100%',
    display: 'flex',
    alignSelf: 'center',
    padding: '12px 24px 0',
    // breakpoint length - padding left and right
    [theme.breakpoints.down('xs')]: {
      padding: '48px 16px 0',
    },
    [theme.breakpoints.only('sm')]: {
      maxWidth: 600,
      minWidth: 600,
    },
    [theme.breakpoints.only('md')]: {
      maxWidth: 960,
      minWidth: 960,
    },
    [theme.breakpoints.up('lg')]: {
      maxWidth: 1280,
      minWidth: 1280,
    },
  },
  results: {
    width: 'inherit',
  },
  resultsHeader: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'end',
  },
}));

const Results = ({
  isLoading,
  isBlank,
  intl,
  facets,
  constraints,
  changeConstraints,
  term,
  changeTerm,
  changeFacet,
  facet,
  logo,
  resetSearch,
  size,
  accessibility,
}) => {
  const theme = useTheme();
  const isRtl = useSelector(getIsRtl);
  const searchResultNb = useSelector(getSearchResultNb);
  const dataflows = useSelector(getDataflows);
  const isNarrow = useMediaQuery(theme.breakpoints.down('sm'));
  const classes = useStyles();
  const dispatch = useDispatch();

  const NO_RESULT_MESSAGE_ID = 'noResultMessage';
  const SEARCH_RESULTS_COUNT_ID = 'searchResultsCount';
  useEffect(() => {
    if (searchResultNb === 1 && !term) {
      dispatch(changeDataflow(R.head(dataflows), 'replaceHistory', 'overview'));
    }
  }, [searchResultNb, term, dataflows]);

  useEffect(() => {
    if (R.not(accessibility)) return;
    if (R.not(isLoading)) {
      R.not(isBlank)
        ? document.getElementById(SEARCH_RESULTS_COUNT_ID).focus()
        : document.getElementById(NO_RESULT_MESSAGE_ID).focus();
    }
    if (R.and(R.not(isLoading), R.not(isBlank)))
      document.getElementById(SEARCH_RESULTS_COUNT_ID).focus();
  }, [isLoading, isBlank]); //eslint-disable-line react-hooks/exhaustive-deps

  return (
    <Fragment>
      <DeAppBar goHome={resetSearch} logo={logo}>
        <Spotlight
          hasClearAll
          hasCommit
          action={changeTerm}
          placeholder={formatMessage(intl)(messages.placeholder)}
          isRtl={isRtl}
          term={term}
        />
      </DeAppBar>
      <Page id={ID_SEARCH_PAGE} alignSelf="center">
        {R.or(R.and(isBlank, R.not(isLoading)), isLoading) && (
          <Grid item className={classes.main}>
            <Grid container className={classes.margin}>
              <Grid item xs={12} style={{ marginTop: 200 - 64 * 2 }}>
                {isLoading && (
                  <NoData
                    message={<FormattedMessage id="de.search.list.loading" />}
                    icon={<Loader />}
                  />
                )}
                {R.and(isBlank, R.not(isLoading)) && (
                  <NoData message={<FormattedMessage id="de.search.list.blank" />} />
                )}
              </Grid>
              {isLoading && (
                <Typography tabIndex={0} aria-live="assertive" variant="srOnly">
                  <FormattedMessage id="de.search.list.loading" />
                </Typography>
              )}
            </Grid>
          </Grid>
        )}
        {R.and(R.not(isLoading), R.not(isBlank)) && (
          <Grid item className={classes.main}>
            <Grid container className={classes.margin} wrap={isNarrow ? 'wrap' : 'nowrap'}>
              <div className={classes.side}>
                <FiltersHelp isSearch />
                <NarrowFilters isNarrow={isNarrow}>
                  {R.not(R.isEmpty(constraints)) && (
                    <ExpansionPanel
                      key={PANEL_SEARCH_CURRENT}
                      id={PANEL_SEARCH_CURRENT}
                      label={<FormattedMessage id="vx.filters.current.title" />}
                      onChangeActivePanel={changeFacet}
                      tag={<Tag>{countNumberOf(constraints)}</Tag>}
                      isOpen={R.isEmpty(facets) || R.equals(facet, PANEL_SEARCH_CURRENT)}
                    >
                      <Chips
                        items={constraints}
                        onDelete={changeConstraints}
                        onDeleteAll={changeConstraints}
                        clearAllLabel={<FormattedMessage id="vx.filters.current.clear" />}
                        labels={{
                          reducingChip: formatMessage(intl)(messages.reducingChip),
                        }}
                      />
                    </ExpansionPanel>
                  )}
                  <Facets
                    accessibility={accessibility}
                    intl={intl}
                    facets={facets}
                    activePanelId={facet}
                    onChangeActivePanel={changeFacet}
                    changeSelection={changeConstraints}
                  />
                </NarrowFilters>
              </div>
              <div className={classes.results}>
                <div className={classes.resultsHeader}>
                  <Typography
                    variant="body2"
                    color="textSecondary"
                    tabIndex={0}
                    id={SEARCH_RESULTS_COUNT_ID}
                  >
                    <FormattedMessage id="de.search.list.status" values={{ size }} />
                  </Typography>
                  <Sort />
                </div>
                <Dataflows />
                <Grid container justifyContent="flex-end">
                  <Pagination />
                </Grid>
              </div>
            </Grid>
          </Grid>
        )}
      </Page>
    </Fragment>
  );
};

Results.propTypes = {
  isRtl: PropTypes.bool,
  isLoading: PropTypes.bool,
  isBlank: PropTypes.bool,
  intl: PropTypes.object,
  constraints: PropTypes.array,
  facets: PropTypes.array,
  changeConstraints: PropTypes.func,
  term: PropTypes.string,
  changeTerm: PropTypes.func,
  changeFacet: PropTypes.func,
  facet: PropTypes.string,
  logo: PropTypes.string,
  resetSearch: PropTypes.func.isRequired,
  size: PropTypes.number,
  accessibility: PropTypes.bool,
};

export default Results;
