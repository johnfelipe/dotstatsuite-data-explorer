import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose, branch, renderComponent, withProps, withStateHandlers } from 'recompose';
import { injectIntl } from 'react-intl';
import * as R from 'ramda';
import Home from './home';
import Results from './results';
import { getIsPending } from '../../selectors/app';
import {
  getResultsFacets,
  getDataflows,
  getConstraints,
  getConfigFacets,
  getHasNoSearchParams,
} from '../../selectors/search';
import {
  requestConfig,
  resetSearch,
  changeTerm,
  changeFacet,
  changeConstraints,
} from '../../ducks/search';
import {
  getTerm,
  getFacet,
  getSearchResultNb,
  getHasAccessibility,
  getLocale,
} from '../../selectors/router';
import { getAsset, hasNoSearch } from '../../lib/settings';
import { setLabel } from './utils';

const EnhancedHome = compose(
  injectIntl,
  connect(
    createStructuredSelector({
      facets: getConfigFacets,
      isLoading: getIsPending('getConfig'),
      localeId: getLocale,
    }),
    { changeTerm, changeConstraints, requestConfig },
  ),
  withProps(({ facets, intl, localeId }) => ({
    hasNoSearch,
    logo: getAsset('splash', localeId),
    facets: R.ifElse(
      R.isEmpty,
      R.identity,
      R.map(
        R.pipe(
          setLabel({ intl }),
          R.over(
            R.lensProp('values'),
            R.ifElse(
              R.isEmpty,
              R.identity,
              R.pipe(
                R.filter(R.pipe(R.prop('level'), R.anyPass([R.isNil, isNaN, R.gte(1)]))),
                R.groupBy(
                  R.pipe(R.prop('parentId'), R.ifElse(R.isNil, R.always('orphans'), R.identity)),
                ),
                R.converge(
                  (orphans, children) =>
                    R.map(orphan =>
                      R.assoc(
                        'subtopics',
                        R.pipe(R.propOr([], R.prop('id', orphan)))(children),
                        orphan,
                      ),
                    )(orphans),
                  [R.prop('orphans'), R.dissoc('orphans')],
                ),
              ),
            ),
          ),
        ),
      ),
    )(facets),
  })),
)(Home);

export default compose(
  injectIntl,
  connect(
    createStructuredSelector({
      dataflows: getDataflows,
      facets: getResultsFacets,
      constraints: getConstraints,
      isSearchLoading: getIsPending('getSearch'),
      isConfigLoading: getIsPending('getConfig'),
      term: getTerm,
      facet: getFacet,
      size: getSearchResultNb,
      accessibility: getHasAccessibility,
      hasNoSearchParams: getHasNoSearchParams,
      localeId: getLocale,
    }),
    {
      changeConstraints,
      changeFacet,
      changeTerm,
      resetSearch,
    },
  ),
  withStateHandlers(
    { actionId: undefined },
    {
      changeActionId: ({ actionId }) => nextActionId => ({
        actionId: R.ifElse(R.equals(actionId), R.always(null), R.identity)(nextActionId),
      }),
    },
  ),
  withProps(({ dataflows, isConfigLoading, isSearchLoading, constraints, localeId, intl }) => ({
    logo: getAsset('subheader', localeId),
    isLoading: R.or(isConfigLoading, isSearchLoading),
    isBlank: R.isEmpty(dataflows),
    constraints: R.map(setLabel({ intl }), constraints),
  })),
  branch(({ hasNoSearchParams }) => hasNoSearchParams, renderComponent(EnhancedHome)),
)(Results);
