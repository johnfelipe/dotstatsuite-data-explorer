import React from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import { useIntl } from 'react-intl';
import { Dataflow } from '@sis-cc/dotstatsuite-visions';
import { makeStyles } from '@material-ui/core/styles';
import { FormattedMessage } from '../../../i18n';
import { setHighlights } from '../utils';
import { downloadableDataflowResults } from '../../../lib/settings';
import Downloads from './downloads';
import SanitizedInnerHTML from '../../SanitizedInnerHTML';

const useStyles = makeStyles(theme => ({
  root: {
    borderTop: `2px solid ${theme.palette.grey[700]}`,
  },
}));

const Dataflows = ({
  dataflows,
  pending,
  changeDataflow,
  requestSearchDataFile,
  requestExternalResources,
}) => {
  const classes = useStyles();
  const intl = useIntl();

  const onDownloadHandler = dataflow =>
    downloadableDataflowResults
      ? () => requestSearchDataFile({ dataflow, isDownloadAllData: true })
      : null;

  return (
    <div className={classes.root} data-testid="search_results">
      {R.map(
        dataflow => (
          <Dataflow
            {...dataflow}
            HTMLRenderer={SanitizedInnerHTML}
            testId={R.prop('id', dataflow)}
            key={R.prop('id', dataflow)}
            id={R.prop('id', dataflow)}
            title={R.prop('name', dataflow)}
            body={{ description: R.prop('description', dataflow) }}
            url={R.prop('url', dataflow)}
            handleUrl={event => {
              event.stopPropagation();
              if (event.ctrlKey) return;
              event.preventDefault();
              return changeDataflow(dataflow);
            }}
            highlights={R.pipe(R.prop('highlights'), R.map(setHighlights({ intl })))(dataflow)}
            label={R.prop('datasourceId', dataflow)}
            labels={{
              dimensions: <FormattedMessage id="de.search.dataflow.dimensions" />,
              source: <FormattedMessage id="de.search.dataflow.source" />,
              lastUpdated: <FormattedMessage id="de.search.dataflow.last.updated" />,
              note: `${R.prop('agencyId', dataflow)}:${R.prop('dataflowId', dataflow)}(${R.prop(
                'version',
                dataflow,
              )})`,
              date: intl.formatDate(R.prop('indexationDate', dataflow), {
                year: 'numeric',
                month: 'long',
                day: '2-digit',
              }),
            }}
          >
            {downloadableDataflowResults && (
              <Downloads
                downloads={[{ key: 'csv.all', handler: onDownloadHandler(dataflow) }]}
                isDownloading={R.prop(`getDataFile/${R.prop('id', dataflow)}`)(pending)}
                isExternalLoading={R.prop(`getExternalResources/${R.prop('dataflowId', dataflow)}`)(
                  pending,
                )}
                action={() => requestExternalResources({ dataflow })}
                dataflowId={R.prop('dataflowId', dataflow)}
                externalMessage={<FormattedMessage id="de.no.external.resources" />}
                pending={pending}
              />
            )}
          </Dataflow>
        ),
        dataflows,
      )}
    </div>
  );
};

Dataflows.propTypes = {
  pending: PropTypes.object,
  dataflows: PropTypes.array,
  requestSearchDataFile: PropTypes.func.isRequired,
  requestExternalResources: PropTypes.func.isRequired,
  changeDataflow: PropTypes.func.isRequired,
};

export default Dataflows;
