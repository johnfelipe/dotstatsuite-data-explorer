import React from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import { makeStyles } from '@material-ui/core/styles';
import GetAppIcon from '@material-ui/icons/GetApp';
import MenuItem from '@material-ui/core/MenuItem';
import ListItemText from '@material-ui/core/ListItemText';
import { useSelector } from 'react-redux';
import Divider from '@material-ui/core/Divider';
import MuiButton from '@material-ui/core/Button';
import { defineMessages, useIntl } from 'react-intl';
import { getExternalResources } from '../../../selectors/search';
import { FormattedMessage, formatMessage } from '../../../i18n';
import { Button, Menu } from '../../visions/DeToolBar/helpers';
import CircularProgress from '@material-ui/core/CircularProgress';

const messages = defineMessages({
  'csv.all': { id: 'de.visualisation.toolbar.action.download.csv.all' },
});

const useStyles = makeStyles(theme => ({
  link: {
    margin: 0,
    padding: theme.spacing(0.75, 2),
    width: '100%',
    height: '100%',
    display: 'flex',
    justifyContent: 'end',
  },
  linkIcon: {
    height: 20,
    paddingRight: theme.spacing(1),
    alignItems: 'center',
  },
}));

const Component = ({
  dataflowId,
  downloads,
  isDownloading,
  isExternalLoading,
  action,
  externalMessage,
}) => {
  const classes = useStyles();
  const intl = useIntl();
  const [anchorEl, setAnchorEl] = React.useState(null);
  const externalResources = useSelector(getExternalResources);

  const openMenu = event => {
    setAnchorEl(event.currentTarget);
    if (R.not(R.has(dataflowId, externalResources))) {
      action();
    }
  };

  const closeMenu = () => {
    setAnchorEl(null);
  };

  const itemClick = (onClickHandler, ...args) => () => {
    if (R.is(Function)(onClickHandler)) onClickHandler(...args);
    closeMenu();
  };

  const resources = R.propOr([], dataflowId)(externalResources);

  return (
    <React.Fragment>
      <Button
        startIcon={<GetAppIcon />}
        selected={Boolean(anchorEl)}
        loading={isDownloading}
        onClick={openMenu}
        aria-haspopup="true"
      >
        <FormattedMessage id="de.visualisation.toolbar.action.download" />
      </Button>
      <Menu anchorEl={anchorEl} open={Boolean(anchorEl)} onClose={closeMenu}>
        {R.map(
          ({ key, handler }) => (
            <MenuItem
              key={key}
              onClick={itemClick(handler)}
              dense
              disabled={!R.is(Function, handler)}
            >
              <ListItemText primaryTypographyProps={{ color: 'primary' }}>
                <span>{formatMessage(intl)(messages[key])}</span>
              </ListItemText>
            </MenuItem>
          ),
          downloads,
        )}
        <Divider />
        {isExternalLoading && (
          <ListItemText primaryTypographyProps={{ color: 'primary', align: 'center' }}>
            <CircularProgress size={24} />
          </ListItemText>
        )}
        {R.and(R.not(isExternalLoading), R.isEmpty(resources)) && (
          <MenuItem dense disabled>
            <ListItemText primaryTypographyProps={{ color: 'primary' }}>
              <span>{externalMessage}</span>
            </ListItemText>
          </MenuItem>
        )}
        {R.not(R.isEmpty(resources)) &&
          R.map(
            ({ id, label, link, img }) => (
              <MuiButton
                key={id}
                component="a"
                color="primary"
                target="_blank"
                href={link}
                className={classes.link}
                id={id}
                tabIndex={0}
                download
              >
                {R.not(R.isNil(img)) && (
                  <img src={img} className={classes.linkIcon} alt={`${id} icon`} />
                )}
                {label}
              </MuiButton>
            ),
            resources,
          )}
      </Menu>
    </React.Fragment>
  );
};

Component.propTypes = {
  downloads: PropTypes.array.isRequired,
  isDownloading: PropTypes.bool,
  isExternalLoading: PropTypes.bool,
  action: PropTypes.func,
  dataflowId: PropTypes.string,
  externalMessage: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
};

export default Component;
