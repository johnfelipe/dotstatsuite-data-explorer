import React from 'react';
import * as R from 'ramda';
import { FormattedMessage } from '../../../i18n';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose, branch, renderComponent } from 'recompose';
import CircularProgress from '@material-ui/core/CircularProgress';
import { NoData } from '@sis-cc/dotstatsuite-visions';
import Dataflows from './dataflows';
import { getIsPending, getPending } from '../../../selectors/app';
import {
  requestSearchDataFile,
  changeDataflow,
  requestExternalResources,
} from '../../../ducks/sdmx';
import { getDataflows } from '../../../selectors/search';

export default compose(
  connect(
    createStructuredSelector({
      dataflows: getDataflows,
      isSearchLoading: getIsPending('getSearch'),
      isConfigLoading: getIsPending('getConfig'),
      pending: getPending, // downloads
    }),
    { changeDataflow, requestSearchDataFile, requestExternalResources },
  ),
  branch(
    R.pipe(R.pick(['isSearchLoading', 'isConfigLoading']), R.any(R.equals(true))),
    renderComponent(() => (
      <NoData
        icon={<CircularProgress fontSize="large" />}
        message={<FormattedMessage id="de.search.list.loading" />}
      />
    )),
  ),
  branch(
    R.pipe(R.prop('dataflows'), R.isEmpty),
    renderComponent(() => <NoData message={<FormattedMessage id="de.search.list.blank" />} />),
  ),
)(Dataflows);
