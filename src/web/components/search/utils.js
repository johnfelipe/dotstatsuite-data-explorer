import * as R from 'ramda';
import { searchConstants } from '../../lib/search';
import { formatMessage } from '../../i18n';
import { defineMessages } from 'react-intl';

const messages = defineMessages({
  datasourceId: { id: 'de.search.datasourceId' },
  agencyId: { id: 'de.search.agencyId' },
  version: { id: 'de.search.version' },
  dataflowId: { id: 'vx.config.display.code' },
});

const getStaticMessage = intl =>
  R.cond([
    [R.equals(searchConstants.DATASOURCE_ID), R.always(formatMessage(intl)(messages.datasourceId))],
    [R.equals(searchConstants.AGENCY_ID), R.always(formatMessage(intl)(messages.agencyId))],
    [R.equals(searchConstants.VERSION_ID), R.always(formatMessage(intl)(messages.version))],
    [R.equals(searchConstants.DATAFLOW_ID), R.always(formatMessage(intl)(messages.dataflowId))],
    [R.T, R.identity],
  ]);

export const setLabel = ({ intl }) => ({ id, label, ...rest }) => ({
  id,
  label: R.isNil(label) ? getStaticMessage(intl)(id) : label,
  ...rest,
});

export const setHighlights = ({ intl }) => highlight => [
  R.pipe(R.head, getStaticMessage(intl))(highlight),
  R.last(highlight),
];

export const getAccessor = isIdHidden => ({ label, code }) => {
  if (isIdHidden) {
    return label;
  }
  return R.isNil(code) || R.isEmpty(code) ? label : `${label} (${code})`;
};
