import React from 'react';
import PropTypes from 'prop-types';
import sanitizeHtml from 'sanitize-html';
import { sanitizeOptions } from '../lib/settings';

const SanitizedInnerHTML = ({ html, ...rest }) => {
  return (
    <span dangerouslySetInnerHTML={{ __html: sanitizeHtml(html, sanitizeOptions) }} {...rest} />
  );
};

SanitizedInnerHTML.propTypes = {
  html: PropTypes.string,
};

export default SanitizedInnerHTML;
