import React, { useCallback, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';
import axios from 'axios';
import * as R from 'ramda';
import { useIntl } from 'react-intl';
import Link from '@material-ui/core/Link';
import { Share as VisionsShare } from '@sis-cc/dotstatsuite-visions';
import { changeUserEmail } from '../../ducks/user';
import { shareSuccess } from '../../ducks/vis';
import { getIsTimeInverted, getVisTableLayout, getVisChoroMap } from '../../selectors';
import { getDataflow, getDisplay, getLocale } from '../../selectors/router';
import { getRawDataRequestArgs, getDataUrl, getDataFileRequestArgs } from '../../selectors/sdmx';
import { getUserEmail } from '../../selectors/user';
import { FormattedMessage, formatMessage } from '../../i18n';
import messages from '../messages';
import * as Settings from '../../lib/settings';
import { getShareData, getShareConfig } from '../../utils/viewer';
import { getUser } from '../../selectors/app';
import { useFetch } from '../useFetch';

const usePublicShare = () => {
  const { url, params, headers } = useSelector(getDataFileRequestArgs(true));
  const { response, isLoading } = useFetch(url, {
    params,
    headers: { ...headers, Range: 'values=0-0' },
  });
  return {
    isLoading,
    isPublic: response?.status === 200 || response?.status === 206,
  };
};

const Share = ({ viewerProps }) => {
  const dispatch = useDispatch();
  const user = useSelector(getUser);
  const userEmail = user?.email;
  const customEmail = useSelector(getUserEmail);

  const intl = useIntl();
  const [state, setState] = useState({ isSharing: false, isMessageOpen: false, hasError: false });
  const [mode, setMode] = useState('snapshot');
  const [email, setEmail] = useState(userEmail);
  const sdmxUrl = useSelector(getDataUrl({ agnostic: false }));
  const requestArgs = useSelector(getRawDataRequestArgs);
  const localeId = useSelector(getLocale);
  const dataflow = useSelector(getDataflow);
  const isTimeInverted = useSelector(getIsTimeInverted);
  const layoutIds = useSelector(getVisTableLayout());
  const map = useSelector(getVisChoroMap);
  const display = useSelector(getDisplay);
  const { isPublic, isLoading } = usePublicShare();

  useEffect(() => {
    if (customEmail) setEmail(customEmail);
  }, [customEmail]);

  const sendShare = useCallback(async () => {
    if (state.isSharing) return;
    const endpoint = `${Settings.shareEndpoint}/charts`;
    const confirmUrl = Settings.shareConfirmUrl;
    const shareConfig = getShareConfig({
      mode,
      sdmxUrl,
      requestArgs,
      localeId,
      dataflow,
      isTimeInverted,
      layoutIds,
      map,
      display,
    });

    const body = {
      confirmUrl,
      data: getShareData(viewerProps, mode, shareConfig),
      email,
    };

    setState(R.set(R.lensProp('isSharing'), true));
    await axios
      .post(endpoint, body)
      .then(() => {
        setState(
          R.pipe(R.set(R.lensProp('hasError'), false), R.set(R.lensProp('isMessageOpen'), true)),
        );
        dispatch(shareSuccess());
      })
      .catch(() =>
        setState(
          R.pipe(R.set(R.lensProp('hasError'), true), R.set(R.lensProp('isMessageOpen'), true)),
        ),
      );

    if (email !== userEmail) dispatch(changeUserEmail(email));

    setState(R.set(R.lensProp('isSharing'), false));
  }, [state.isSharing, mode, email, viewerProps]);

  return (
    <VisionsShare
      share={sendShare}
      changeMode={setMode}
      changeIsMessageOpen={() => setState(R.set(R.lensProp('isMessageOpen'), false))}
      changeMail={setEmail}
      isMessageOpen={state.isMessageOpen}
      isSharing={state.isSharing}
      mail={email}
      mode={mode}
      hasError={state.hasError}
      modes={[
        {
          label: <FormattedMessage id="share.snapshot" />,
          value: 'snapshot',
          warningMessage:
            user && !isPublic && !isLoading ? formatMessage(intl)(messages.sharePrivateData) : null,
        },
        {
          label: <FormattedMessage id="share.latest" />,
          value: 'latest',
        },
      ]}
      labels={{
        disclaimer: (
          <FormattedMessage
            id="share.disclaimer"
            values={{
              br: <br />,
              link: (
                <Link
                  target="_blank"
                  rel="noopener noreferrer"
                  href={formatMessage(intl)(messages.sharePolicyLink)}
                >
                  <FormattedMessage id="share.policy.label" />
                </Link>
              ),
            }}
          />
        ),
        email: <FormattedMessage id="share.mail" />,
        errorTitle: <FormattedMessage id="share.error.title" />,
        errorMessage: <FormattedMessage id="share.error.message" />,
        title: <FormattedMessage id="share.title" />,
        submit: formatMessage(intl)(messages.submit),
        successTitle: <FormattedMessage id="share.success.title" />,
        successMessage: <FormattedMessage id="share.success.message" />,
      }}
    />
  );
};

Share.propTypes = {
  viewerProps: PropTypes.object,
};

export default Share;
