import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import Typography from '@material-ui/core/Typography';
import cx from 'classnames';
import { makeStyles } from '@material-ui/core/styles';
import { Tooltip } from '@sis-cc/dotstatsuite-visions';
import { isLast } from './utils';

const useStyles = makeStyles(theme => ({
  tooltipIndicator: {
    borderBottom: `1px dotted ${theme.palette.primary.main}`,
  },
  label: {
    fontWeight: '700',
    fontStyle: 'italic',
  },
}));

const BulletIcon = () => (
  <svg width="16" height="8">
    <circle cx="8" cy="4" r="3" stroke={'#000000'} fill={'#000000'} />
  </svg>
);
const HierarchyLabels = ({ values = [], accessor }) => {
  return R.addIndex(R.map)(
    (value, index) => (
      <Fragment key={value.id}>
        {`${accessor(value)} ${!isLast(index, values) ? ' > ' : ''}`}
      </Fragment>
    ),
    values,
  );
};

HierarchyLabels.propTypes = {
  values: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
    }),
  ),
};

const GroupLabels = ({
  list = [],
  accessor = R.prop('name'),
  isValueVisible = true,
  isHierarchy = false,
  hasGroupSeparator = false,
}) => {
  const classes = useStyles();
  const displayedList = R.filter(R.propOr(true, 'display'))(list);
  return (
    <Typography component="span" variant="body2">
      {R.addIndex(R.map)(
        (item, index) => (
          <Fragment key={item.id}>
            <Tooltip placement="top" title={item?.description || ''}>
              <span>
                <span
                  className={cx(classes.label, {
                    [classes.tooltipIndicator]: item?.description,
                  })}
                >
                  {R.is(Function, accessor) ? `${accessor(item)}` : item.name}
                </span>
                {isValueVisible && ': '}
              </span>
            </Tooltip>
            {isValueVisible &&
              R.is(Array, item.values) &&
              (isHierarchy ? (
                <HierarchyLabels values={item.values} accessor={accessor} />
              ) : (
                accessor(R.head(item.values))
              ))}
            {(!isLast(index, displayedList) || hasGroupSeparator) && <BulletIcon />}
          </Fragment>
        ),
        displayedList,
      )}
    </Typography>
  );
};

GroupLabels.propTypes = {
  list: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string,
      name: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
      description: PropTypes.string,
      values: PropTypes.arrayOf(
        PropTypes.shape({
          name: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
        }),
      ),
    }),
  ),
  accessor: PropTypes.func,
  isValueVisible: PropTypes.bool,
  isHierarchy: PropTypes.bool,
  hasGroupSeparator: PropTypes.bool,
  classes: PropTypes.object,
};

export default GroupLabels;
