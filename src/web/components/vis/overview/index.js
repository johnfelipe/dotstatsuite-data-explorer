import React from 'react';
import * as R from 'ramda';
import PropTypes from 'prop-types';
import dateFns from 'date-fns';
import { useSelector } from 'react-redux';
import { Link, makeStyles } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import { defineMessages, useIntl } from 'react-intl';
import { DataFooter, Loading } from '@sis-cc/dotstatsuite-visions';
import {
  getDataflowDescription,
  getDimensions,
  getDatasetAttributes,
  getExternalResources,
  getObservationsCount,
  getActualConstraints,
  getHierarchySchemes,
  getValidFrom,
} from '../../../selectors/sdmx';
import { getVisDataflow, getVisDimensionFormat } from '../../../selectors';
import { getDataflow, getLocale } from '../../../selectors/router';
import { search } from '../../../lib/settings';
import { formatMessage, FormattedMessage } from '../../../i18n';
import { getDateLocale } from '../../../i18n/dates';
import GroupLabels from './GroupLabels';
import { groupDimensions, groupAttributes, isLast } from './utils';
import SanitizedInnerHTML from '../../SanitizedInnerHTML';

const messages = defineMessages({
  relatedFiles: { id: 'de.related.files' },
  observationsCount: { id: 'de.observations.count' },
  dataSource: { id: 'de.data.source' },
  validFrom: { id: 'de.last.updated' },
});

const useStyles = makeStyles(theme => ({
  divider: {
    backgroundColor: theme.palette.primary.light,
  },
  container: {
    margin: theme.spacing(1.25),
  },
  title: {
    fontSize: 18,
    ...R.pathOr({}, ['mixins', 'dataHeader', 'title'], theme),
  },
  textContainer: {
    margin: theme.spacing(1.25, 0, 0, 0),
  },
  listItem: {
    display: 'list-item',
    listStyle: 'disc',
  },
}));

const Overview = ({ isLoading, footerProps, loadingMessage }) => {
  const classes = useStyles();
  const intl = useIntl();
  const locale = useSelector(getLocale);
  const labelAccessor = useSelector(getVisDimensionFormat({ id: 'id', name: 'label' }));
  const nameAccessor = useSelector(getVisDimensionFormat());
  const dataflow = useSelector(getVisDataflow);
  const { datasourceId } = useSelector(getDataflow);
  const dataflowDescription = useSelector(getDataflowDescription);
  const dimensions = useSelector(getDimensions);
  const attributes = useSelector(getDatasetAttributes);
  const hierarchySchemes = useSelector(getHierarchySchemes);
  const actualConstraints = useSelector(getActualConstraints);
  const externalResources = useSelector(getExternalResources);
  const observationsCount = useSelector(getObservationsCount);
  const validFrom = useSelector(getValidFrom);

  if (isLoading)
    return (
      <Loading
        message={loadingMessage || <FormattedMessage id="de.visualisation.data.loading" />}
      />
    );
  const homeFacetIds = new Set(R.propOr([], 'homeFacetIds', search));
  const { oneDimensions, manyDimensions } = groupDimensions(dimensions, actualConstraints);
  const { oneAttributes } = groupAttributes(attributes || [], actualConstraints, oneDimensions);
  const selectedHierarchySchemes = R.reduce((acc, schemes) => {
    if (homeFacetIds.has(R.head(schemes)?.name)) {
      return R.append(schemes, acc);
    }
    return acc;
  }, [])(hierarchySchemes);
  const dataSpaceLabel =
    window.CONFIG?.member?.scope?.spaces?.[datasourceId]?.label ||
    window.CONFIG?.member?.scope?.datasources?.[datasourceId]?.label;
  return (
    <>
      <Divider className={classes.divider} />
      <div className={classes.container}>
        <Typography component="h1" variant="h1" className={classes.title}>
          {nameAccessor(dataflow)}
        </Typography>
        <div className={classes.textContainer}>
          <Typography variant="body2">
            <SanitizedInnerHTML html={dataflowDescription} />
          </Typography>
        </div>
        <div className={classes.textContainer}>
          <div>
            <GroupLabels list={oneDimensions} accessor={labelAccessor} />
          </div>
          <div>
            <GroupLabels list={manyDimensions} accessor={labelAccessor} isValueVisible={false} />
          </div>
          <div>
            <GroupLabels list={oneAttributes} accessor={labelAccessor} />
          </div>
        </div>
        <div className={classes.textContainer}>
          {R.addIndex(R.map)((hierarchies, index) => {
            const head = R.head(hierarchies);
            return (
              <GroupLabels
                key={`hierarchy-${index}`}
                list={[{ ...head, values: R.tail(hierarchies) }]}
                accessor={nameAccessor}
                isHierarchy
                hasGroupSeparator={!isLast(index, selectedHierarchySchemes)}
              />
            );
          }, selectedHierarchySchemes)}
          {homeFacetIds.has('datasourceId') && dataSpaceLabel && (
            <div>
              <GroupLabels
                list={[
                  {
                    id: 'dataSource',
                    name: formatMessage(intl)(messages.dataSource),
                    values: [{ name: dataSpaceLabel }],
                  },
                ]}
              />
            </div>
          )}
        </div>
        <div className={classes.textContainer}>
          {!R.isNil(observationsCount) && (
            <div>
              <GroupLabels
                list={[
                  {
                    id: 'observationsCount',
                    name: formatMessage(intl)(messages.observationsCount),
                    values: [{ name: observationsCount }],
                  },
                ]}
              />
            </div>
          )}
          {validFrom && (
            <div>
              <GroupLabels
                list={[
                  {
                    id: 'validFrom',
                    name: formatMessage(intl)(messages.validFrom),
                    values: [
                      {
                        name: dateFns.format(new Date(validFrom), 'MMMM DD, YYYY', {
                          locale: getDateLocale(locale),
                        }),
                      },
                    ],
                  },
                ]}
              />
            </div>
          )}
        </div>
        {!R.isEmpty(externalResources) && (
          <div className={classes.textContainer}>
            <div>
              <GroupLabels
                list={[
                  {
                    id: 'relatedFiles',
                    name: formatMessage(intl)(messages.relatedFiles),
                  },
                ]}
                isValueVisible={false}
              />
            </div>
            <List dense>
              {R.map(
                ({ id, label, link }) => (
                  <ListItem key={id} dense>
                    <Link color="primary" href={link} classes={{ root: classes.listItem }}>
                      {label}
                    </Link>
                  </ListItem>
                ),
                externalResources,
              )}
            </List>
          </div>
        )}
        <Divider className={classes.divider} />
        <DataFooter {...footerProps} />
      </div>
    </>
  );
};

Overview.propTypes = {
  isLoading: PropTypes.bool,
  loadingMessage: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  defaultFooterProps: PropTypes.object,
};

export default Overview;
