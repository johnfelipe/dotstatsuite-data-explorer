import React from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import { useIntl } from 'react-intl';
import * as R from 'ramda';
import { Loading, NoData, DataFooter, DataHeader } from '@sis-cc/dotstatsuite-visions';
import { rules } from '@sis-cc/dotstatsuite-components';
import Divider from '@material-ui/core/Divider';
import { makeStyles } from '@material-ui/core';
import { getVisDataflow, getCustomAttributes } from '../../../selectors';
import { getData } from '../../../selectors/microdata';
import { getDisplay, getLocale } from '../../../selectors/router';
import { getDataUrl } from '../../../selectors/sdmx';
import { FormattedMessage, formatMessage } from '../../../i18n';
import { units, getAsset } from '../../../lib/settings';
import messages from '../../messages';
import Table from './table';
import { getLog } from '../../../selectors/app';
import { LOG_ERROR_SDMX_DATA } from '../../../ducks/app';
import { displayAccessor } from '../../../lib/sdmx/microdata';

const useStyles = makeStyles(theme => ({
  divider: {
    backgroundColor: theme.palette.primary.light,
  },
}));

const MicrodataViewer = ({ loadingMessage, defaultFooterProps }) => {
  const intl = useIntl();
  const classes = useStyles();
  const display = useSelector(getDisplay);
  const dataflow = useSelector(getVisDataflow);
  const data = useSelector(getData);
  const sdmxUrl = useSelector(getDataUrl({ agnostic: false }));
  const localeId = useSelector(getLocale);
  const log = useSelector(getLog(LOG_ERROR_SDMX_DATA));
  const customAttributes = useSelector(getCustomAttributes);

  let noDataMessage = null;
  let errorMessage = null;
  if (log) {
    const status = R.path(['log', 'status'], log);
    if (status === 404) noDataMessage = <FormattedMessage id="log.error.sdmx.404" />;
    else if (R.includes(status, [401, 402, 403]))
      errorMessage = <FormattedMessage id="log.error.sdmx.40x" />;
    else errorMessage = <FormattedMessage id="log.error.sdmx.xxx" />;
  }

  if (loadingMessage) return <Loading message={loadingMessage} />;
  if (errorMessage) return <NoData message={errorMessage} />;
  if (noDataMessage) return <NoData message={noDataMessage} />;

  const informations = rules.getInformationsStateFromNewProps(
    { data, display, customAttributes, dataflowId: R.prop('id', dataflow), units },
    {},
  );

  const headerProps = rules.getHeaderProps(
    {
      dataflow,
      data,
      sdmxUrl,
      display,
      customAttributes,
      units: {
        unitDimension: {
          id: R.prop('id', units),
          name: formatMessage(intl)(messages.unitsOfMeasure),
        },
      },
    },
    R.pick(['dataflowAttributes', 'units', 'dimensions'], informations),
  );

  const _headerProps = R.over(
    R.lensPath(['title', 'label']),
    label => formatMessage(intl)(messages.microdataTitle, { label }),
    headerProps,
  );

  const footerProps = {
    ...defaultFooterProps,
    source: {
      label: R.prop('sourceLabel', rules.getFooterProps({ data, sdmxUrl, display }, {})),
      link: window.location.href,
    },
    logo: getAsset('viewerFooter', localeId),
  };

  return (
    <div>
      <Divider className={classes.divider} />
      <DataHeader {..._headerProps} />
      {!R.isNil(data) && (
        <Table data={data} displayAccessor={displayAccessor({ localeId, display })} />
      )}
      <DataFooter {...footerProps} />
    </div>
  );
};

MicrodataViewer.propTypes = {
  dataflow: PropTypes.object,
  loadingMessage: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
  defaultFooterProps: PropTypes.object,
};

export default MicrodataViewer;
