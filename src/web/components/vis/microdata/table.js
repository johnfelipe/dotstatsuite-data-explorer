import React from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import * as R from 'ramda';
import cx from 'classnames';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TablePagination from '@material-ui/core/TablePagination';
import { rules } from '@sis-cc/dotstatsuite-components';
import { prepareTableColumns, prepareTableRows } from '../../../lib/sdmx/microdata';
import { FormattedMessage } from '../../../i18n';
import { getCustomAttributes } from '../../../selectors';

const cellStyle = {
  border: `1px solid #A4A1A1`,
  fontSize: 12,
  fontWeight: 400,
  lineHeight: 1.43,
  padding: '4px 8px',
  zIndex: 1,
};

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
  },
  container: {
    maxHeight: 550,
  },
  table: {
    borderCollapse: 'collapse',
  },
  column: {
    ...cellStyle,
    whiteSpace: 'nowrap',
    fontWeight: 'bold',
    backgroundColor: theme.palette.primary.main,
    color: theme.palette.getContrastText(theme.palette.primary.main),
  },
  row: {
    color: theme.palette.grey['A700'],
    backgroundColor:
      R.path(['palette', 'tertiary', 'light'])(theme) || theme.palette.secondary.light,
  },
  cell: {
    ...cellStyle,
  },
  hover: {
    '&:hover': {
      backgroundColor: '#FEF4E6 !important', // theme override
    },
  },
}));

const MicrodataTable = ({ data, displayAccessor }) => {
  const classes = useStyles();
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(100);

  const customAttributes = useSelector(getCustomAttributes);
  const columns = React.useMemo(() => prepareTableColumns(data, customAttributes), [data]);
  const rows = React.useMemo(() => prepareTableRows(data), [data]);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = event => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  return (
    <div className={classes.root}>
      <TableContainer className={classes.container}>
        <Table stickyHeader aria-label="sticky table" className={classes.table}>
          <TableHead>
            <TableRow>
              {columns.map(column => (
                <TableCell
                  key={column.id}
                  align={R.defaultTo('left', column.align)}
                  style={{ minWidth: column.minWidth }}
                  className={classes.column}
                >
                  {R.equals(column.id, 'value') ? (
                    <FormattedMessage id="microdata.value" />
                  ) : (
                    displayAccessor(column)
                  )}
                </TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map(row => {
              return (
                <TableRow
                  hover
                  role="checkbox"
                  tabIndex={-1}
                  key={row.id}
                  classes={{ hover: classes.hover }}
                >
                  {columns.map(column => {
                    const cell = R.propOr({}, column.id, row);
                    const value = R.ifElse(
                      R.is(Object),
                      R.ifElse(
                        R.isEmpty,
                        R.always('-'),
                        R.pipe(R.propOr({}, 'value'), displayAccessor),
                      ),
                      cell => rules.getCellValue({ value: cell }),
                    )(cell);
                    /*let value;
                    if (typeof cell === 'number') value = cell;
                    else if (!R.isEmpty(cell)) value = displayAccessor(cell.value);
                    else value = '-';*/
                    return (
                      <TableCell
                        key={column.id}
                        align={R.defaultTo('left', column.align)}
                        className={cx(classes.cell, {
                          [classes.row]: R.propEq('type', 'dimension', cell),
                        })}
                      >
                        {value}
                      </TableCell>
                    );
                  })}
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[100, 1000]}
        component="div"
        count={rows.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onChangePage={handleChangePage}
        onChangeRowsPerPage={handleChangeRowsPerPage}
      />
    </div>
  );
};

MicrodataTable.propTypes = {
  data: PropTypes.object,
  displayAccessor: PropTypes.func,
};

MicrodataTable.defaultProps = {
  displayAccessor: R.identity,
};

export default MicrodataTable;
