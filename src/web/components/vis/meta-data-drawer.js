import React from 'react';
import * as R from 'ramda';
import { useDispatch, useSelector } from 'react-redux';
import numeral from 'numeral';
import dateFns from 'date-fns';
import { getDateLocale } from '../../i18n/dates';
import { makeStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import { CollapsibleTree } from '@sis-cc/dotstatsuite-visions';
import DownloadIcon from '@material-ui/icons/GetApp';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import CircularProgress from '@material-ui/core/CircularProgress';
import { FormattedMessage, formatMessage } from '../../i18n/index.js';
import { Container } from '@material-ui/core';
import { rules2 } from '@sis-cc/dotstatsuite-components';
import { useIntl } from 'react-intl';
import {
  getIsOpen,
  getIsLoading,
  getAttributesSeries,
  getMetadataSeries,
  getIsDownloading,
} from '../../selectors/metadata';
import { getVisDataflow, getDataDimensions } from '../../selectors';
import { getDisplay, getLocale } from '../../selectors/router';
import { flushMetadata, downloadMetadata } from '../../ducks/metadata';
import { locales } from '../../lib/settings';
import messages from '../messages';
import SanitizedInnerHTML from '../SanitizedInnerHTML';

const useStyles = makeStyles(theme => ({
  drawerPaper: {
    width: 350,
    padding: theme.spacing(1),
  },
  backdrop: {
    backgroundColor: 'unset',
  },
  title: {
    marginBottom: theme.spacing(1),
  },
  closeDrawer: {
    float: 'right',
  },
  downloading: {
    width: '15px !important',
    height: '15px !important',
  },
}));

const MetaDataDrawer = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const isOpen = useSelector(getIsOpen);
  const isLoading = useSelector(getIsLoading);
  const isDownloading = useSelector(getIsDownloading);
  const attributesSeries = useSelector(getAttributesSeries);
  const metadataSeries = useSelector(getMetadataSeries);
  const dataflow = useSelector(getVisDataflow);
  const dimensions = useSelector(getDataDimensions());
  const display = useSelector(getDisplay);
  const locale = useSelector(getLocale);
  const intl = useIntl();

  const options = { display, attributesLabel: formatMessage(intl)(messages.advancedAttrs) };
  const sidebarData = rules2.getSidebarData(
    attributesSeries,
    metadataSeries,
    dataflow,
    dimensions,
    options,
  );

  const valueHandler = ({ value, format }) => {
    if (R.isNil(value)) {
      return value;
    }
    if (format === 'Numeric' || format === 'Integer' || format === 'Decimal') {
      return numeral(Number(value)).format(`0,0.[0000000]`);
    }
    if (format === 'Boolean') {
      if (R.is(String, value)) {
        if (R.toLower(value) === 'true') {
          return formatMessage(intl)(messages.dataTrue);
        }
        if (R.toLower(value) === 'false') {
          return formatMessage(intl)(messages.dataFalse);
        }
      }
      return value
        ? formatMessage(intl)(messages.dataTrue)
        : formatMessage(intl)(messages.dataFalse);
    }
    if (format === 'GregorianYearMonth') {
      return dateFns.format(
        new Date(value),
        R.pathOr('YYYY-MMM', [locale, 'timeFormat'], locales),
        { locale: getDateLocale(locale) },
      );
    }
    return value;
  };

  const closeDrawer = () => event => {
    if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
      return;
    }
    dispatch(flushMetadata());
  };

  const onDownload = () => {
    dispatch(downloadMetadata());
  };

  const sideLabels = {
    collapseAll: formatMessage(intl)(messages.metadataCollapse),
    expandAll: formatMessage(intl)(messages.metadataExpand),
    noValue: formatMessage(intl)(messages.metadataNoValue),
  };

  return (
    <>
      <Drawer
        data-testid="ref-md-panel"
        className={classes.drawer}
        anchor="right"
        open={isOpen}
        classes={{ paper: classes.drawerPaper }}
        BackdropProps={{ invisible: true }}
        onClose={closeDrawer()}
      >
        <div style={{ marginTop: '70px' }} />
        <Container disableGutters>
          <IconButton
            size="small"
            className={classes.closeDrawer}
            onClick={closeDrawer()}
            data-testid="ref-md-panel-close"
          >
            <CloseIcon fontSize="small" />
          </IconButton>
          <Typography variant="h5" className={classes.title}>
            <FormattedMessage id="metadata.information.title" />
          </Typography>
        </Container>
        <CollapsibleTree
          data={sidebarData}
          defaultParentExtended={true}
          defaultValueExtented={true}
          isLoading={isLoading}
          labels={sideLabels}
          valueHandler={({ value, format }) => (
            <SanitizedInnerHTML html={valueHandler({ value, format })} />
          )}
        >
          {!isLoading && !isDownloading && !R.isNil(metadataSeries) && !R.isEmpty(metadataSeries) && (
            <Button color="primary" endIcon={<DownloadIcon />} onClick={onDownload}>
              <FormattedMessage id="metadata.download" />
            </Button>
          )}
          {!isLoading && isDownloading && <CircularProgress className={classes.downloading} />}
        </CollapsibleTree>
      </Drawer>
    </>
  );
};

export default MetaDataDrawer;
