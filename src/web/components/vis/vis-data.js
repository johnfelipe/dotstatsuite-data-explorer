import React, { useState } from 'react';
import { connect, useDispatch, useSelector } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose, withProps, branch, renderComponent } from 'recompose';
import * as R from 'ramda';
import { rules, RulesDriver, Viewer as ViewerComp } from '@sis-cc/dotstatsuite-components';
import Typography from '@material-ui/core/Typography';
import Link from '@material-ui/core/Link';
import { injectIntl, useIntl } from 'react-intl';
import { FormattedMessage, formatMessage } from '../../i18n';
import Tools from '../vis-tools';
import { LOG_ERROR_SDMX_DATA } from '../../ducks/app';
import { getHasMicrodata, getIsMicrodata } from '../../selectors/microdata';
import {
  getCustomAttributes,
  getTablePreparedData,
  getVisTableLayout,
  getIsTimeInverted,
  getVisChoroMap,
  getVisDataflow,
  getVisIsLoadingMap,
  getTableCellsLimit,
  getIsIncreased,
} from '../../selectors';
import { getIsPending, getLog } from '../../selectors/app';
import {
  getData,
  getDataUrl,
  getSelection,
  getTimeFormats,
  getDataRequestSize,
  getRefinedDataRange,
} from '../../selectors/sdmx';
import * as Settings from '../../lib/settings';
import { getLocale, getIsRtl, getDisplay, getViewer, getIsOverview } from '../../selectors/router';
import { withVisDataProps } from '../../utils/viewer';
import { ID_VIEWER_COMPONENT } from '../../css-api';
import messages from '../messages';
import MicrodataViewer from './microdata';
import { updateMicrodataConstraints } from '../../ducks/microdata';
import Guidances from './guidances';
import MetaDataDrawer from './meta-data-drawer';
import { getHeaderSideProps } from '../../selectors/metadata';
import MetadataIcon from './MetadataIcon';
import Overview from './overview';
import SanitizedInnerHTML from '../SanitizedInnerHTML';
import { OVERVIEW, TABLE } from '../../utils/constants';

const cellValueAccessor = cell =>
  R.over(
    R.lensProp('value'),
    R.when(
      R.is(Boolean),
      R.ifElse(
        R.identity,
        R.always(<FormattedMessage id="sdmx.data.true" />),
        R.always(<FormattedMessage id="sdmx.data.false" />),
      ),
    ),
  )(cell);

const getHeaderDisclaimer = (intl, range, type, isTruncated, totalCells, dataLimit, cellsLimit) => {
  const { count, total } = range;
  if (count < total || isTruncated) {
    const limit = R.isNil(dataLimit) ? count : dataLimit;
    const values = {
      range: limit,
      cellsLimit,
      total,
      totalCells,
    };
    if (type === 'table') return intl.formatMessage({ id: 'incomplete.table.data' }, values);
    return intl.formatMessage({ id: 'incomplete.chart.data' }, values);
  }
  return null;
};

const getFooterCopyrightLabel = () => <FormattedMessage id="de.viewer.copyright.label" />;

const getFooterCopyrightContent = intl => (
  <FormattedMessage
    id="de.viewer.copyright.content.label"
    values={{
      link: (
        <Link
          href={formatMessage(intl)(messages.viewerLink)}
          rel="noopener noreferrer"
          target="_blank"
          variant="body2"
        >
          <FormattedMessage id="de.viewer.copyright.content.link.label" />
        </Link>
      ),
    }}
  />
);

const DataVisView = ({
  dataflow,
  errors,
  isLoadingData,
  isLoadingMap,
  isFull,
  toolsProps,
  viewerProps,
  hasSelection,
  visPageWidth,
}) => {
  const dispatch = useDispatch();
  const intl = useIntl();
  const [activeCellCoordinates, setCellCoordinates] = useState({});
  const log = useSelector(getLog(LOG_ERROR_SDMX_DATA));
  const isMicrodata = useSelector(getIsMicrodata);
  const isOverview = useSelector(getIsOverview);
  const hasMicrodata = useSelector(getHasMicrodata);
  const headerSideProps = useSelector(getHeaderSideProps);
  const isIncreased = useSelector(getIsIncreased);
  let loading = null;
  let loadingProps = {};
  if (isLoadingData && !isOverview) {
    loading = isIncreased ? (
      <FormattedMessage id="de.visualisation.data.larger.loading" />
    ) : (
      <FormattedMessage id="de.visualisation.data.loading" />
    );

    loadingProps = isIncreased ? { isWarning: true } : {};
  } else if (isLoadingMap && !isOverview) loading = <FormattedMessage id="de.vis.map.loading" />;
  const _viewerProps = R.pipe(
    R.over(
      R.lensProp('tableProps'),
      R.pipe(
        R.assoc('SideIcon', MetadataIcon),
        R.set(R.lensProp('cellValueAccessor'), cellValueAccessor),
        R.set(R.lensProp('activeCellIds'), activeCellCoordinates),
        R.set(R.lensProp('activeCellHandler'), setCellCoordinates),
        R.set(R.lensProp('HTMLRenderer'), SanitizedInnerHTML),
        R.when(
          R.always(hasMicrodata),
          R.assoc('cellHandler', ({ indexedDimValIds }) =>
            dispatch(updateMicrodataConstraints(indexedDimValIds)),
          ),
        ),
      ),
    ),
    R.over(
      R.lensProp('headerProps'),
      R.pipe(R.assoc('sideProps', headerSideProps), R.assoc('SideIcon', MetadataIcon)),
    ),
    R.set(R.lensProp('loadingProps'), loadingProps),
  )(viewerProps);

  let noData = null;
  let errorMessage = null;
  if (log) {
    const status = R.path(['log', 'status'], log);
    if (status === 404) noData = <FormattedMessage id="log.error.sdmx.404" />;
    else if (R.includes(status, [401, 402, 403]))
      errorMessage = <FormattedMessage id="log.error.sdmx.40x" />;
    else errorMessage = <FormattedMessage id="log.error.sdmx.xxx" />;
  } else {
    if (errors.noTime) errorMessage = <FormattedMessage id="vx.no.time.data" />;
    if (hasSelection) noData = <FormattedMessage id="vx.no.data.selection" />;
  }
  return (
    <>
      <MetaDataDrawer />
      <Tools maxWidth={visPageWidth} isFull={isFull} viewerProps={_viewerProps} {...toolsProps} />
      <div id={ID_VIEWER_COMPONENT}>
        {!isMicrodata && !isOverview && (
          <ViewerComp
            {..._viewerProps}
            errorMessage={errorMessage}
            loading={loading}
            noData={noData}
          />
        )}
        {isMicrodata && (
          <MicrodataViewer
            dataflow={dataflow}
            loadingMessage={loading}
            defaultFooterProps={{
              copyright: {
                getFooterCopyrightLabel,
                getFooterCopyrightContent: () => getFooterCopyrightContent(intl),
              },
            }}
          />
        )}
      </div>
      <Guidances type={viewerProps.type} maxWidth={visPageWidth} />
      {loading && (
        <Typography tabIndex={0} aria-live="assertive" variant="srOnly">
          <FormattedMessage id="de.visualisation.data.loading" />
        </Typography>
      )}
    </>
  );
};
const DataVis = compose(
  injectIntl,
  connect(
    createStructuredSelector({
      isLoadingStructure: getIsPending('getStructure'),
    }),
  ),
  withProps(({ footerProps, locale, intl }) => ({
    footerProps: R.pipe(
      R.assoc('isSticky', true),
      R.assoc('source', {
        label: R.prop('sourceLabel', footerProps),
        link: window.location.href,
      }),
      R.when(
        R.always(R.prop('withLogo', footerProps)),
        R.assoc('logo', Settings.getAsset('viewerFooter', locale)),
      ),
      R.set(R.lensPath(['copyright', 'label']), getFooterCopyrightLabel()),
      R.set(R.lensPath(['copyright', 'content']), getFooterCopyrightContent(intl)),
      R.when(R.always(!R.prop('withCopyright', footerProps)), R.dissoc('copyright')),
    )({}),
  })),
  branch(
    R.propEq('type', OVERVIEW),
    renderComponent(({ footerProps, isLoadingStructure, visPageWidth, isFull }) => (
      <>
        <Tools maxWidth={visPageWidth} isFull={isFull} />
        <Overview isLoading={isLoadingStructure} footerProps={footerProps} />
      </>
    )),
  ),
  connect(
    createStructuredSelector({
      isLoadingData: getIsPending('getData'),
      isLoadingMap: getVisIsLoadingMap,
      isTimeInverted: getIsTimeInverted,
      isRtl: getIsRtl,
      preparedData: getTablePreparedData(),
      layoutIds: getVisTableLayout(),
      display: getDisplay,
      range: getRefinedDataRange,
      selection: getSelection,
      timeFormats: getTimeFormats,
      customAttributes: getCustomAttributes,
      cellsLimit: getTableCellsLimit,
      dataLimit: getDataRequestSize,
    }),
  ),
  withProps(
    ({
      preparedData,
      layoutIds,
      display,
      isTimeInverted,
      type,
      units,
      customAttributes,
      cellsLimit,
    }) => {
      if (type !== TABLE) {
        return {};
      }
      return {
        tableProps: rules.getTableProps({
          data: R.assocPath(['units', 'unitDimension'], units.unitDimension, preparedData),
          layoutIds,
          display,
          customAttributes,
          limit: cellsLimit,
          isTimeInverted,
        }),
      };
    },
  ),
  withProps(({ headerProps, range, selection, tableProps, type, intl, dataLimit, cellsLimit }) => ({
    headerProps: R.pipe(
      R.assoc('isSticky', true),
      R.assoc(
        'disclaimer',
        getHeaderDisclaimer(
          intl,
          range,
          type,
          !!tableProps?.truncated,
          R.defaultTo(0, tableProps?.totalCells),
          dataLimit,
          cellsLimit,
        ),
      ),
    )(headerProps),
    hasSelection: R.not(R.isEmpty(selection)),
  })),
  withVisDataProps,
)(DataVisView);

export const ParsedDataVisualisation = compose(
  connect(
    createStructuredSelector({
      display: getDisplay,
      type: getViewer,
      data: getData,
      dataflow: getVisDataflow,
      sdmxUrl: getDataUrl({ agnostic: false }),
      map: getVisChoroMap,
      locale: getLocale,
      customAttributes: getCustomAttributes,
    }),
  ),
  injectIntl,
  withProps(({ intl, map, type }) => {
    const isNonIdealMapState = R.allPass([
      R.always(type === 'ChoroplethChart'),
      R.anyPass([R.isNil, R.has('error')]),
    ])(map);
    return {
      formaterIds: {
        decimals: Settings.getSdmxAttribute('decimals'),
        prefscale: Settings.getSdmxAttribute('prefscale'),
      },
      options: Settings.chartOptions,
      source: { link: window.location.href },
      map: R.when(R.always(isNonIdealMapState), R.always(null))(map),
      type,
      isNonIdealMapState,
      units: {
        ...Settings.units,
        unitDimension: {
          id: R.prop('id', Settings.units),
          name: formatMessage(intl)(messages.unitsOfMeasure),
        },
      },
    };
  }),
)(props => (
  <RulesDriver
    {...props}
    render={(
      parsedProps, // chartData, chartOptions, properties (aka chart configs props)
    ) => {
      return (
        <DataVis
          {...parsedProps}
          {...R.pick(
            ['data', 'locale', 'type', 'isNonIdealMapState', 'units', 'dataflow', 'visPageWidth'],
            props,
          )}
        />
      );
    }}
  />
));
