import * as R from 'ramda';
import axios from 'axios';
import { shareEndpoint } from '../lib/settings';

const endpoint = path => `${shareEndpoint}${path}`;

const _delete = path => ({ params }) => {
  return axios.delete(endpoint(path), { params }).then(R.identity);
};

const _get = path => ({ params }) => {
  return axios.get(endpoint(path), { params }).then(R.identity);
};

const _post = path => ({ params }) => {
  return axios.post(endpoint(path), params).then(R.identity);
};

const methods = {
  getStatus: _get('/charts'),
  getList: _get('/list'),
  getConfirm: _get('/confirm'),
  getDelete: _delete('/delete'),
  getDeleteAll: _delete('/deleteAll'),
  postList: _post('/list'),
};

const error = method => () => {
  throw new Error(`Unkown method: ${method}`);
};

const main = ({ method, ...rest }) => (methods[method] || error(method))(rest);
R.compose(
  R.forEach(([name, fn]) => (main[name] = fn)),
  R.toPairs,
)(methods);

export default main;
