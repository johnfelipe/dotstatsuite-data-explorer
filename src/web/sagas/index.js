import { all } from 'redux-saga/effects';
import { watchRequestStructure, watchParseStructure } from './sdmx/structure';
import { watchRequestData, watchParseData, watchRequestDataFile } from './sdmx/data';
import {
  watchRequestAvailableConstraints,
  watchParseAvailableConstraints,
} from './sdmx/availableConstraints';
import { watchRequestMetadata } from './sdmx/metadata';
import { watchRequestBlankData } from './sdmx/blank-data';
import { mapSaga } from './map';

export default function* rootSaga() {
  yield all([
    watchRequestStructure(),
    watchParseStructure(),
    watchRequestData(),
    watchRequestAvailableConstraints(),
    watchParseAvailableConstraints(),
    watchRequestDataFile(),
    watchParseData(),
    mapSaga(),
    watchRequestMetadata(),
    watchRequestBlankData(),
  ]);
}
