import axios from 'axios';
import * as R from 'ramda';
import { select, put, call, takeLatest } from 'redux-saga/effects';
import { rules2 } from '@sis-cc/dotstatsuite-components';
import { getRequestArgs } from '@sis-cc/dotstatsuite-sdmxjs';
import { pushLog, LOG_ERROR } from '../../ducks/app';
import { HANDLE_BLANK_DATA, REQUEST_BLANK_DATA, REQUEST_STRUCTURE } from '../../ducks/sdmx';
import { CHANGE_VIEWER } from '../../ducks/vis';
import { getToken } from '../../selectors/app';
import { getDataflow, getViewer } from '../../selectors/router';
import { getDatasetAttributes, getDatasource, getExternalReference } from '../../selectors/sdmx';
import { OVERVIEW } from '../../utils/constants';

function* requestBlankDataWorker() {
  const viewerId = yield select(getViewer);
  const stateDatasetAttributes = yield select(getDatasetAttributes);
  if (viewerId !== OVERVIEW || !R.isNil(stateDatasetAttributes)) {
    return;
  }
  const datasource = yield select(getDatasource);
  if (!R.prop('hasRangeHeader', datasource)) {
    return;
  }
  try {
    yield put({ type: REQUEST_BLANK_DATA });
    const _dataflow = yield select(getDataflow);
    const externalRef = yield select(getExternalReference);
    const dataflow = R.isNil(externalRef) ? _dataflow : externalRef;
    const { url, headers } = getRequestArgs({
      datasource,
      identifiers: {
        code: R.prop('dataflowId', dataflow),
        ...R.pick(['agencyId', 'version'], dataflow),
      },
      type: 'data',
      range: [0, 0],
    });
    const isExternal = R.has('id', datasource) ? R.propOr(false, 'isExternal', datasource) : true;
    const token = yield select(getToken);
    const authHeaders = R.when(
      R.always(!R.isNil(token) && !isExternal),
      R.assoc('Authorization', `Bearer ${token}`),
    )(headers);
    const _response = yield call(axios.get, url, { headers: authHeaders });
    let response = _response.data;
    if (R.startsWith(rules2.SDMX_3_0_JSON_DATA_FORMAT, headers.Accept)) {
      response = rules2.sdmx_3_0_DataFormatPatch(response);
    }
    const annotations = R.pathOr([], ['data', 'structure', 'annotations'], response);
    const datasetAnnotations = R.props(
      R.pathOr([], ['data', 'dataSets', 0, 'annotations'], response),
      annotations,
    );
    const hiddenCodes = R.pipe(
      R.find(R.propEq('type', 'NOT_DISPLAYED')),
      R.when(R.isNil, R.always({})),
      R.propOr('', 'title'),
      R.split(','),
      R.indexBy(R.identity),
    )(datasetAnnotations);
    const datasetAttributes = R.pipe(
      R.pathOr({}, ['data', 'structure', 'attributes']),
      ({ observation = [], dataSet = [] }) => R.concat(observation, dataSet),
      R.reduce((acc, attr) => {
        if (!R.hasPath(['relationship', 'dataflow'], attr)) {
          return acc;
        }
        if (R.length(attr.values || []) !== 1) {
          return acc;
        }
        if (
          R.has(attr.id, hiddenCodes) ||
          R.has(`${attr.id}=${R.path(['values', 0, 'id'], attr)}`, hiddenCodes)
        ) {
          return acc;
        }
        const attrAnnots = R.props(attr.annotations || [], annotations);
        const valAnnots = R.props(R.pathOr([], ['values', 0, 'annotations'], attr), annotations);
        const hidden = !!R.find(R.propEq('type', 'NOT_DISPLAYED'), R.concat(attrAnnots, valAnnots));
        if (hidden) {
          return acc;
        }
        return R.append(
          {
            ...attr,
            label: attr.name,
            values: [{ ...R.head(attr.values), label: R.path(['values', 0, 'name'], attr) }],
          },
          acc,
        );
      }, []),
    )(response);
    yield put({ type: HANDLE_BLANK_DATA, datasetAttributes });
  } catch (error) {
    yield put(pushLog({ type: LOG_ERROR, log: error }));
  }
}

export function* watchRequestBlankData() {
  yield takeLatest([REQUEST_STRUCTURE, CHANGE_VIEWER], requestBlankDataWorker);
}
