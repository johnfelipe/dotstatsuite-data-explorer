import axios from 'axios';
import FileSaver from 'file-saver';
import * as R from 'ramda';
import { select, put, call, takeLatest, take, cancel } from 'redux-saga/effects';
import {
  getRequestArgs,
  parseDataRange,
  parseExternalReference,
} from '@sis-cc/dotstatsuite-sdmxjs';
import { rules, rules2 } from '@sis-cc/dotstatsuite-components';
import {
  getDataRequestArgs,
  getDataFileRequestArgs,
  getDimensions,
  getStructureRequestArgs,
  getAvailableConstraintsArgs,
  getFrequency,
} from '../../selectors/sdmx';
import {
  HANDLE_DATA,
  REQUEST_DATA,
  REQUEST_VIS_DATAFILE,
  REQUEST_SEARCH_DATAFILE,
  PARSE_DATA,
  FLUSH_DATA,
  REQUEST_STRUCTURE,
  HANDLE_STRUCTURE,
  REQUEST_AVAILABLE_CONSTRAINTS,
} from '../../ducks/sdmx';
import { setPending, pushLog, LOG_ERROR, LOG_ERROR_SDMX_DATA, flushLog } from '../../ducks/app';
import { getSpace, getSpaceFromUrl, isSpaceExternal, locales } from '../../lib/settings';
import { CHANGE_LAYOUT } from '../../ducks/vis';
import {
  FLUSH_MICRODATA,
  PARSE_MICRODATA,
  HANDLE_MICRODATA,
  REQUEST_MICRODATA,
} from '../../ducks/microdata';
import {
  getIsMicrodata,
  getMicrodataFileRequestArgs,
  getMicrodataRequestArgs,
} from '../../selectors/microdata';
import { getDataflow, getDisplay, getLocale } from '../../selectors/router';
import { getFilename } from '../../lib/sdmx';
import { getToken } from '../../selectors/app';
import { MICRODATA } from '../../utils/constants';

const isDev = process.env.NODE_ENV === 'development';

export const downloadFile = ({ response, filename }) => {
  const BOM = '\uFEFF';
  const blob = new Blob([R.concat(BOM, R.prop('data')(response))], {
    type: 'text/csv;charset=utf-8',
  });
  FileSaver.saveAs(blob, `${filename}.csv`);
};

function* requestDataFile(args) {
  const { pendingId, url, headers, params, filename, space } = args;
  try {
    yield put(setPending(pendingId, true));
    yield put(flushLog(LOG_ERROR));
    const token = yield select(getToken);
    const authHeaders = R.when(
      R.always(!R.isNil(token) && space && !R.propOr(false, 'isExternal', space)),
      R.assoc('Authorization', `Bearer ${token}`),
    )(headers);
    if (isDev) console.info(`request: ${pendingId}`); // eslint-disable-line no-console
    const response = yield call(axios.get, url, { headers: authHeaders, params });
    yield call(downloadFile, { response, filename });
    yield put(setPending(pendingId, false));
  } catch (error) {
    const log = error.response
      ? { errorCode: error.response.data.errorCode, statusCode: error.response.status }
      : { error };

    yield put(setPending(pendingId, false));
    yield put(pushLog({ type: LOG_ERROR, log }));
  }
}

function* requestVisDataFile(action) {
  const { isDownloadAllData } = action.payload;
  const isMicrodata = yield select(getIsMicrodata);
  let getRequestFileArgs = getDataFileRequestArgs;
  if (isMicrodata) getRequestFileArgs = getMicrodataFileRequestArgs;
  const dataflow = yield select(getDataflow);
  const space = getSpace(dataflow.datasourceId);
  const rawArgs = yield select(getRequestFileArgs(isDownloadAllData));
  yield requestDataFile({ ...rawArgs, space, pendingId: 'requestingDataFile' });
}

function* requestSearchDataFile(action) {
  const { dataflow } = action.payload;
  const locale = yield select(getLocale);
  const display = yield select(getDisplay);
  const externalUrl = R.prop('externalUrl', dataflow);
  const externalReference = parseExternalReference(
    {
      isExternalReference: !R.isNil(externalUrl),
      links: [{ rel: 'external', href: externalUrl }],
    },
    'dataflow',
  );

  const identifiers = { code: R.prop('dataflowId', dataflow), ...dataflow };
  const space = R.isNil(externalReference)
    ? getSpace(R.prop('datasourceId', dataflow))
    : getSpaceFromUrl(R.path(['datasource', 'url'], externalReference)) ||
      R.prop('datasource', externalReference);

  const rawArgs = getRequestArgs({
    datasource: space,
    identifiers: R.defaultTo(identifiers, R.prop('identifiers', externalReference)),
    locale,
    asFile: true,
    format: 'csv',
    labels: R.when(R.always(display === 'code'), R.always('code'))('both'),
    type: 'data',
  });

  const filename = getFilename({ identifiers });
  yield requestDataFile({ ...rawArgs, space, filename, pendingId: `getDataFile/${dataflow.id}` });
}

function* requestData({ shouldRequestStructure, datatype }) {
  // avoid using dynamic types to detect missing things during dev
  // ie `FLUSH_${datatype}` instead of using FLUSH_DATA or FLUSH_MICRODATA or else
  let FLUSH = FLUSH_DATA,
    PARSE = PARSE_DATA,
    LOG_ERROR = LOG_ERROR_SDMX_DATA,
    getRequestArgs = getDataRequestArgs;

  const isMicroData = R.equals(datatype, MICRODATA);
  if (isMicroData) {
    (FLUSH = FLUSH_MICRODATA), (PARSE = PARSE_MICRODATA);
    getRequestArgs = getMicrodataRequestArgs;
  }

  const pendingId = 'getData';
  let url, headers, params;
  try {
    yield put({ type: FLUSH });
    const dataflow = yield select(getDataflow);
    const datasourceId = dataflow.datasourceId;
    if (shouldRequestStructure) {
      const structureRequestArgs = yield select(getStructureRequestArgs);
      yield put({ ...structureRequestArgs, type: REQUEST_STRUCTURE, datasourceId });
      yield take(HANDLE_STRUCTURE);
    }
    const dimensions = yield select(getDimensions);
    if (R.all(R.isEmpty)(dimensions)) {
      // eslint-disable-next-line no-console
      console.log('Saga Data: Dimensions are completely empty, cancel requestData');
      yield cancel();
    }

    if (!isMicroData) {
      const availableConstraintsArgs = yield select(getAvailableConstraintsArgs);
      yield put({ type: REQUEST_AVAILABLE_CONSTRAINTS, ...availableConstraintsArgs, datasourceId });
    }

    ({ url, headers, params } = yield select(getRequestArgs));
    yield put(setPending(pendingId, true));
    yield put(flushLog(LOG_ERROR_SDMX_DATA));
    const token = yield select(getToken);
    const authHeaders = R.when(
      R.always(!R.isNil(token) && !isSpaceExternal(datasourceId)),
      R.assoc('Authorization', `Bearer ${token}`),
    )(headers);
    if (isDev) console.info(`request: ${pendingId}`); // eslint-disable-line no-console
    const _response = yield call(axios.get, url, {
      headers: authHeaders,
      params,
    });
    let response = _response.data;
    if (R.startsWith(rules2.SDMX_3_0_JSON_DATA_FORMAT, headers.Accept)) {
      response = rules2.sdmx_3_0_DataFormatPatch(response);
    }
    yield put(setPending(pendingId, false));
    yield put({ type: PARSE, data: { data: response, headers: _response.headers }, datatype });
  } catch (error) {
    yield put(setPending(pendingId, false));

    if (error.response) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx
      const log = {
        status: error.response.status,
        statusText: error.response.statusText,
        url,
        params,
        headers,
      };
      yield put(pushLog({ type: LOG_ERROR, log }));
    } else if (error.request) {
      // The request was made but no response was received
      // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
      // http.ClientRequest in node.js
      if (isDev) console.log(error.request); // eslint-disable-line no-console
      yield put(pushLog({ type: LOG_ERROR_SDMX_DATA, log: error }));
    } else {
      // Something happened in setting up the request that triggered an Error
      if (isDev) console.log('Error', error.message); // eslint-disable-line no-console
      yield put(pushLog({ type: LOG_ERROR_SDMX_DATA, log: error }));
    }
  }
}

function* parseData({ data, datatype }) {
  // avoid using dynamic types to detect missing things during dev
  // ie `FLUSH_${datatype}` instead of using FLUSH_DATA or FLUSH_MICRODATA or else
  let HANDLE = HANDLE_DATA;
  if (R.equals(datatype, MICRODATA)) HANDLE = HANDLE_MICRODATA;

  const locale = yield select(getLocale);
  const freq = yield select(getFrequency);
  const dataflow = yield select(getDataflow);
  let options = { frequency: freq, locale, dataflowId: dataflow.dataflowId };
  if (freq === 'M') {
    options = R.assoc('timeFormat', R.path([locale, 'timeFormat'])(locales), options);
  }

  try {
    yield put(flushLog(LOG_ERROR_SDMX_DATA));
    yield put({
      type: HANDLE,
      data: R.prop('data', rules.v8Transformer(data.data, options)),
      range: parseDataRange(data),
    });

    /*
     * triggers historyMiddleware to update the url with existing layout and dimensions from data
     * complete layout in location state is correct
     * partial (compact) layout in location search can't be correct without dimensions from data request
     * the following empty action is used to update location search and have a synced url
     * note 1: if pathname is not provided, existing is kept
     * note 2: if payload is empty, not override will occur (do not mix with no payload which is a reset)
     */
    yield put({ type: CHANGE_LAYOUT, replaceHistory: { payload: {} } });
  } catch (error) {
    yield put(pushLog({ type: LOG_ERROR_SDMX_DATA, log: error }));
  }
}

export function* watchRequestData() {
  yield takeLatest(REQUEST_DATA, requestData);
  yield takeLatest(REQUEST_MICRODATA, requestData);
}

export function* watchRequestDataFile() {
  yield takeLatest(REQUEST_VIS_DATAFILE, requestVisDataFile);
  yield takeLatest(REQUEST_SEARCH_DATAFILE, requestSearchDataFile);
}

export function* watchParseData() {
  yield takeLatest(PARSE_DATA, parseData);
  yield takeLatest(PARSE_MICRODATA, parseData);
}
