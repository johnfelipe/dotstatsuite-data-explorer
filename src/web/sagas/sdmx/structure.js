import axios from 'axios';
import * as R from 'ramda';
import dateFns from 'date-fns';
import { select, put, call, takeLatest } from 'redux-saga/effects';
import {
  getActualContentConstraintsDefaultSelection,
  getDataflowExternalReference,
  parseStructure as sdmxjsParseStructure,
  getDataquery as sdmxjsGetDataquery,
  getRequestArgs as sdmxjsGetRequestArgs,
  getFrequencyArtefact,
  getExternalResources,
  parseExternalReference,
  getArtefactCategories as sdmxjsParseArtefactCategories,
  getDataflowDescription,
  getActualContentConstraintsArtefact,
} from '@sis-cc/dotstatsuite-sdmxjs';
import {
  getConstraints,
  getLocale,
  getPeriod,
  getDataquery,
  getTableLayout,
  getHasDataAvailability,
  getHighlightedConstraints,
  getLastNObservations,
  getDataflow,
} from '../../selectors/router';
import {
  HANDLE_STRUCTURE,
  REQUEST_STRUCTURE,
  PARSE_STRUCTURE,
  EXTERNAL_REFERENCE,
  HANDLE_EXTERNAL_RESOURCES,
  REQUEST_EXTERNAL_RESOURCES,
} from '../../ducks/sdmx';
import { setPending, pushLog, flushLog, LOG_ERROR_SDMX_STRUCTURE } from '../../ducks/app';
import { getDefaultSelection } from '../../lib/search/sdmx-constraints-bridge';
import {
  getDefaultRouterParams,
  getDefaultSelectionContentConstrained,
  getOnlyHasDataDimensions,
} from '../../lib/sdmx';
import {
  getSpaceFromDatasourceId,
  getSpaceFromUrl,
  sdmxPeriodBoundaries,
  isSpaceExternal,
} from '../../lib/settings';
import { dateWithoutTZ } from '../../utils/date';
import { renameKeys } from '../../utils';
import { getToken } from '../../selectors/app';

import { requestHierarchicalCodelists } from './hierarchical-codelists';
import { getHierarchicalCodelistsReferences } from '../../lib/sdmx/hierarchical-codelist';

const isDev = process.env.NODE_ENV === 'development';

function* requestExternalResources({ dataflow }) {
  const dataflowId = R.prop('dataflowId')(dataflow);
  const pendingId = `getExternalResources/${dataflowId}`;
  const locale = yield select(getLocale);
  const externalUrl = R.prop('externalUrl', dataflow);
  const externalReference = parseExternalReference(
    {
      isExternalReference: !R.isNil(externalUrl),
      links: [{ rel: 'external', href: externalUrl }],
    },
    'dataflow',
  );

  const space = R.isNil(externalReference)
    ? getSpaceFromDatasourceId(R.prop('datasourceId', dataflow))
    : getSpaceFromUrl(R.path(['datasource', 'url'], externalReference));

  const { url, headers } = sdmxjsGetRequestArgs({
    identifiers: R.defaultTo(
      R.pipe(
        R.pickAll(['agencyId', 'dataflowId', 'version']),
        renameKeys({ dataflowId: 'code' }),
      )(dataflow),
      R.prop('identifiers', externalReference),
    ),
    datasource: space || R.prop('datasource', externalReference),
    type: 'dataflow',
    withPartialReferences: true,
    locale,
  });

  try {
    yield put(setPending(pendingId, true));
    yield put(flushLog(LOG_ERROR_SDMX_STRUCTURE));
    if (isDev) console.info(`request: ${pendingId}`); // eslint-disable-line no-console
    const token = yield select(getToken);
    const authHeaders = R.when(
      R.always(!R.isNil(token) && space && !R.propOr(false, 'isExternal', space)),
      R.assoc('Authorization', `Bearer ${token}`),
    )(headers);
    const response = yield call(axios.get, url, { headers: authHeaders });
    const structure = response.data;
    yield put(setPending(pendingId, false));
    yield put({
      type: HANDLE_EXTERNAL_RESOURCES,
      externalResources: { [dataflowId]: getExternalResources(structure) },
    });
  } catch (error) {
    yield put(setPending(pendingId, false));

    if (error.response) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx
      const log = {
        status: error.response.status,
        statusText: error.response.statusText,
        url,
        headers,
      };
      yield put(pushLog({ type: LOG_ERROR_SDMX_STRUCTURE, log }));
    } else if (error.request) {
      // The request was made but no response was received
      // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
      // http.ClientRequest in node.js
      console.log(error.request); // eslint-disable-line no-console
      yield put(pushLog({ type: LOG_ERROR_SDMX_STRUCTURE, log: error }));
    } else {
      // Something happened in setting up the request that triggered an Error
      console.log('Error', error.message); // eslint-disable-line no-console
      yield put(pushLog({ type: LOG_ERROR_SDMX_STRUCTURE, log: error }));
    }
  }
}

function* requestStructure({ url, headers, params, datasourceId }) {
  const pendingId = 'getStructure';

  try {
    yield put(setPending(pendingId, true));
    yield put(flushLog(LOG_ERROR_SDMX_STRUCTURE));
    if (isDev) console.info(`request: ${pendingId}`); // eslint-disable-line no-console
    const token = yield select(getToken);
    const authHeaders = R.when(
      R.always(!R.isNil(token) && !isSpaceExternal(datasourceId)),
      R.assoc('Authorization', `Bearer ${token}`),
    )(headers);
    const response = yield call(axios.get, url, { headers: authHeaders, params });
    const structure = response.data;
    yield put(setPending(pendingId, false));
    yield put({ type: PARSE_STRUCTURE, structure });
  } catch (error) {
    yield put(setPending(pendingId, false));

    if (error.response) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx
      const log = {
        status: error.response.status,
        statusText: error.response.statusText,
        url,
        params,
        headers,
      };
      yield put(pushLog({ type: LOG_ERROR_SDMX_STRUCTURE, log }));
    } else if (error.request) {
      // The request was made but no response was received
      // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
      // http.ClientRequest in node.js
      console.log(error.request); // eslint-disable-line no-console
      yield put(pushLog({ type: LOG_ERROR_SDMX_STRUCTURE, log: error }));
    } else {
      // Something happened in setting up the request that triggered an Error
      console.log('Error', error.message); // eslint-disable-line no-console
      yield put(pushLog({ type: LOG_ERROR_SDMX_STRUCTURE, log: error }));
    }
  }
}

const getDataIncreaseAnnotation = structure =>
  R.pipe(
    R.pathOr([], ['data', 'dataflows', 0, 'annotations']),
    R.find(R.propEq('type', 'MAX_TABLE_DATA')),
  )(structure);

function* parseStructure({ structure }) {
  const locale = yield select(getLocale);
  const currentLayout = yield select(getTableLayout);
  const currentDataquery = yield select(getDataquery);
  const currentPeriod = yield select(getPeriod);
  const currentLastNObservations = yield select(getLastNObservations);
  const { dataflowId, version, agencyId } = yield select(getDataflow);
  try {
    const externalReference = getDataflowExternalReference(structure);
    const externalRefSpace = getSpaceFromUrl(R.path(['datasource', 'url'], externalReference));
    const enhancedExternalReference = R.when(
      R.always(externalRefSpace),
      R.assoc('datasource', externalRefSpace),
    )(externalReference);
    if (!R.isNil(externalReference)) {
      yield put({ type: EXTERNAL_REFERENCE, externalReference: enhancedExternalReference });
      yield put({
        type: REQUEST_STRUCTURE,
        ...sdmxjsGetRequestArgs({
          ...enhancedExternalReference,
          locale,
          type: 'dataflow',
          withPartialReferences: true,
        }),
        datasourceId: R.prop('id', externalRefSpace),
      });
      return;
    }
    const {
      dimensions,
      attributes,
      selection,
      timePeriod,
      externalResources,
      contentConstraints,
      layout,
      params,
      name,
      microdataDimensionId,
    } = sdmxjsParseStructure(structure);
    const constraintsArtefact = getActualContentConstraintsArtefact(structure);
    const constraints = yield select(getConstraints);
    const highlightedContraints = yield select(getHighlightedConstraints);
    // content constraints should not be apply if empty or undefined on the default selection
    const hasDA = yield select(getHasDataAvailability) && !R.isEmpty(contentConstraints || {});

    const getObservationCount = R.pipe(
      R.propOr([], 'annotations'),
      R.find(R.propEq('id', 'obs_count')),
      R.prop('title'),
    );

    const structureSelection = hasDA
      ? getActualContentConstraintsDefaultSelection({ selection, contentConstraints })
      : selection;

    const defaultSelection = hasDA
      ? R.pipe(
          getOnlyHasDataDimensions,
          dimensions =>
            getDefaultSelection(dimensions, structureSelection, constraints, highlightedContraints),
          getDefaultSelectionContentConstrained(contentConstraints),
          R.reject(R.isEmpty),
        )(dimensions)
      : getDefaultSelection(dimensions, structureSelection, constraints);

    const dataquery = R.isEmpty(currentDataquery)
      ? sdmxjsGetDataquery(dimensions, defaultSelection)
      : currentDataquery;

    const isTimePeriodDisable = R.or(R.not(R.prop('display', timePeriod)), R.isNil(timePeriod));

    const timePeriodContraints = isTimePeriodDisable
      ? null
      : R.prop(R.prop('id', timePeriod), contentConstraints);

    const timePeriodBoundaries = R.pipe(
      R.propOr(sdmxPeriodBoundaries, 'boundaries'),
      R.map(
        R.when(
          R.identity,
          R.pipe(date => dateFns.parse(date), dateWithoutTZ),
        ),
      ),
    )(timePeriodContraints);

    const { period } = isTimePeriodDisable
      ? { period: null }
      : getDefaultRouterParams({
          hasDataAvailability: hasDA,
          frequencyArtefact: getFrequencyArtefact({ dimensions, attributes }),
          params,
          dataquery,
          timePeriodBoundaries,
          inclusiveBoundaries: R.propOr([true, true], 'includingBoundaries', timePeriodContraints),
        });

    const defaultLastNObservations = R.prop('lastNObservations', params);

    let hierarchies = null;
    const hCodelistRefs = getHierarchicalCodelistsReferences(structure);
    if (!R.isEmpty(hCodelistRefs)) {
      hierarchies = yield call(requestHierarchicalCodelists, hCodelistRefs);
    }

    yield put(flushLog(LOG_ERROR_SDMX_STRUCTURE));

    const dataIncreaseAnnotation = getDataIncreaseAnnotation(structure);
    const dataRequestSize = R.isNil(dataIncreaseAnnotation)
      ? null
      : R.pipe(R.prop('title'), Number, v => (isNaN(v) ? null : v))(dataIncreaseAnnotation);

    yield put({
      type: HANDLE_STRUCTURE,
      structure: {
        dataRequestSize,
        dimensions,
        attributes,
        externalResources,
        hierarchies,
        microdataDimensionId,
        title: name,
        validFrom: R.prop('validFrom', constraintsArtefact),
        description: getDataflowDescription(structure),
        actualContentConstraints: contentConstraints,
        observationsCount: getObservationCount(constraintsArtefact),
        timePeriodArtefact: isTimePeriodDisable ? null : { ...timePeriod, timePeriodBoundaries },
        hierarchySchemes: sdmxjsParseArtefactCategories({
          artefactId: `${agencyId}:${dataflowId}(${version})`,
          data: structure.data,
        }),
      },
      replaceHistory: {
        pathname: '/vis',
        payload: {
          filter: null,
          period: isTimePeriodDisable ? null : R.defaultTo(period, currentPeriod),
          dataquery,
          layout: R.defaultTo(layout, currentLayout),
          lastNObservations: R.defaultTo(defaultLastNObservations, currentLastNObservations),
        },
      },
    });
  } catch (error) {
    yield put(pushLog({ type: LOG_ERROR_SDMX_STRUCTURE, log: error }));
  }
}

export function* watchRequestStructure() {
  yield takeLatest(REQUEST_STRUCTURE, requestStructure);
  yield takeLatest(REQUEST_EXTERNAL_RESOURCES, requestExternalResources);
}

export function* watchParseStructure() {
  yield takeLatest(PARSE_STRUCTURE, parseStructure);
}
