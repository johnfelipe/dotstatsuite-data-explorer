import axios from 'axios';
import * as R from 'ramda';
import { parseActualContentConstraints } from '@sis-cc/dotstatsuite-sdmxjs';
import { select, put, takeLatest, call } from 'redux-saga/effects';
import {
  REQUEST_AVAILABLE_CONSTRAINTS as REQUEST,
  PARSE_AVAILABLE_CONSTRAINTS as PARSE,
  HANDLE_AVAILABLE_CONSTRAINTS as HANDLE,
} from '../../ducks/sdmx';
import { setPending, pushLog, LOG_ERROR, LOG_ERROR_SDMX_DATA, flushLog } from '../../ducks/app';
import { getToken } from '../../selectors/app';
import { isSpaceExternal } from '../../lib/settings';

const isDev = process.env.NODE_ENV === 'development';

function* requestAvailableConstraints(args) {
  const pendingId = 'getAvailableConstraints';

  try {
    yield put(setPending(pendingId, true));
    if (isDev) console.info(`request: ${pendingId}`); // eslint-disable-line no-console

    const { url, headers, params, datasourceId } = args;
    const token = yield select(getToken);
    const authHeaders = R.when(
      R.always(!R.isNil(token) && !isSpaceExternal(datasourceId)),
      R.assoc('Authorization', `Bearer ${token}`),
    )(headers);

    const response = yield call(axios.get, url, { headers: authHeaders, params });

    yield put(setPending(pendingId, false));
    yield put({ type: PARSE, availableConstraints: response });
  } catch (error) {
    const log = error.response
      ? { errorCode: error.response.data.errorCode, statusCode: error.response.status }
      : { error };

    yield put(setPending(pendingId, false));
    yield put(pushLog({ type: LOG_ERROR, log }));
  }
}

function* parseAvailableConstraints({ availableConstraints }) {
  try {
    yield put(flushLog(LOG_ERROR_SDMX_DATA));
    yield put({
      type: HANDLE,
      availableConstraints: parseActualContentConstraints()(availableConstraints.data),
    });
  } catch (error) {
    yield put(pushLog({ type: LOG_ERROR_SDMX_DATA, log: error }));
  }
}

export function* watchRequestAvailableConstraints() {
  yield takeLatest(REQUEST, requestAvailableConstraints);
}

export function* watchParseAvailableConstraints() {
  yield takeLatest(PARSE, parseAvailableConstraints);
}
