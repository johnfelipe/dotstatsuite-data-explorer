import axios from 'axios';
import { call, select, put, takeLatest } from 'redux-saga/effects';
import * as R from 'ramda';
import { rules2 } from '@sis-cc/dotstatsuite-components';
import {
  TOGGLE_METADATA,
  DOWNLOAD_METADATA,
  requestMetadata,
  handleMetadata,
  REQUEST_METADATA_ERROR,
  DOWNLOAD_METADATA_SUCCESS,
  HANDLE_MSD,
} from '../../ducks/metadata';
import { getDisplay, getLocale } from '../../selectors/router';
import { getRawStructureRequestArgs, getTimePeriodArtefact } from '../../selectors/sdmx';
import { getCoordinates, getMSD, getMSDRequestArgs } from '../../selectors/metadata';
import { getDataDimensions, getTablePreparedData } from '../../selectors';
import { getToken } from '../../selectors/app';
import { downloadFile } from './data';

const getFilename = (dataquery, identifiers) => {
  return `${identifiers.agencyId}_${identifiers.code}_${identifiers.version}_${dataquery}_metadata`;
};

const getPartialDataquery = (indexedDimValIds, dimensions, timeArtefact) => {
  return R.pipe(
    R.filter(d => d.id !== timeArtefact.id),
    R.map(dim => {
      const values = dim.values;
      if (R.length(values) === 1) {
        return R.prop('id', R.head(values));
      }
      if (!R.has(dim.id, indexedDimValIds)) {
        return '*';
      }
      return R.prop(dim.id, indexedDimValIds);
    }),
    R.join('.'),
  )(dimensions);
};

const singleTimeSelection = value => `ge:${value}+le:${value}`;

const getMetadataTimePeriodSelection = (coordinates, timeArtefact) => {
  if (R.isNil(timeArtefact)) {
    return {};
  }

  const periodSelection = R.has(timeArtefact.id, coordinates)
    ? singleTimeSelection(R.prop(timeArtefact.id, coordinates))
    : null;

  if (R.isNil(periodSelection)) {
    return {};
  }

  return { [`c[${timeArtefact.id}]`]: periodSelection };
};

export const getMetadataRequestArgs = ({
  datasource,
  dataquery,
  identifiers,
  periodSelection,
  format,
  locale,
}) => {
  const customHeaders = R.propOr({}, 'headersv3', datasource);
  const acceptFormat =
    format === 'csv'
      ? R.pathOr(rules2.SDMX_3_0_CSV_DATA_FORMAT, ['metadata', format], customHeaders)
      : R.pathOr(rules2.SDMX_3_0_JSON_DATA_FORMAT, ['metadata', format], customHeaders);
  return {
    headers: {
      Accept: acceptFormat,
      'x-level': 'upperOnly',
      'accept-language': locale,
    },
    url: `${datasource.urlv3}/data/dataflow/${identifiers.agencyId}/${identifiers.code}/${identifiers.version}/${dataquery}`,
    params: { ...periodSelection, attributes: 'msd', measures: 'none' },
  };
};

function* requestMSDWorker() {
  const { url, headers, params } = yield select(getMSDRequestArgs);
  const { datasource } = yield select(getRawStructureRequestArgs);
  const token = yield select(getToken);
  const authHeaders = R.when(
    R.always(!R.isNil(token) && !R.propOr(false, 'isExternal', datasource)),
    R.assoc('Authorization', `Bearer ${token}`),
  )(headers);

  try {
    const response = yield call(axios.get, url, { headers: authHeaders, params });
    const msdInfos = rules2.getMSDInformations(response.data);
    yield put({ type: HANDLE_MSD, payload: { msd: msdInfos } });
  } catch (error) {
    console.log('request msd error', error); // eslint-disable-line no-console
  }
}

function* requestMetadataWorker(action) {
  const { coordinates, hasMetadata } = action.payload;
  const { datasource, identifiers } = yield select(getRawStructureRequestArgs);
  if (R.isNil(datasource.urlv3)) {
    return;
  }
  if (!hasMetadata) {
    const { metadataCoordinates } = yield select(getTablePreparedData());
    const hasUpperMetadata = R.pipe(
      R.find(_coordinates => {
        const mergedCoord = R.mergeLeft(_coordinates, coordinates);
        return R.equals(mergedCoord, coordinates);
      }),
      R.complement(R.isNil),
    )(metadataCoordinates);
    if (!hasUpperMetadata) {
      return;
    }
  }
  yield put(requestMetadata());
  const timePeriodArtefact = yield select(getTimePeriodArtefact);
  const locale = yield select(getLocale);
  const periodSelection = getMetadataTimePeriodSelection(coordinates, timePeriodArtefact);
  const dimensions = yield select(getDataDimensions());
  const dataquery = getPartialDataquery(coordinates, dimensions, timePeriodArtefact);
  const { url, headers, params } = getMetadataRequestArgs({
    dataquery,
    datasource,
    identifiers,
    periodSelection,
    format: 'json',
    locale,
  });
  const token = yield select(getToken);
  const authHeaders = R.when(
    R.always(!R.isNil(token) && !R.propOr(false, 'isExternal', datasource)),
    R.assoc('Authorization', `Bearer ${token}`),
  )(headers);

  try {
    const response = yield call(axios.get, url, { headers: authHeaders, params });
    const locale = yield select(getLocale);
    const display = yield select(getDisplay);
    let msd = yield select(getMSD);
    if (R.isNil(msd)) {
      yield call(requestMSDWorker);
      msd = yield select(getMSD);
    }
    const metadataSeries = rules2.parseMetadataSeries(response.data, {
      display,
      locale,
      dimensions,
      attributes: msd.attributes,
    });
    yield put(handleMetadata(metadataSeries));
  } catch (error) {
    yield put({ type: REQUEST_METADATA_ERROR, payload: { error } });
  }
}

function* downloadMetadataWorker() {
  const coordinates = yield select(getCoordinates);
  const { datasource, identifiers } = yield select(getRawStructureRequestArgs);
  if (R.isNil(datasource.urlv3)) {
    return;
  }
  const dimensions = yield select(getDataDimensions());
  const locale = yield select(getLocale);
  const timePeriodArtefact = yield select(getTimePeriodArtefact);
  const periodSelection = getMetadataTimePeriodSelection(coordinates, timePeriodArtefact);
  const dataquery = getPartialDataquery(coordinates, dimensions, timePeriodArtefact);
  const filename = getFilename(dataquery, identifiers);

  const { url, headers, params } = getMetadataRequestArgs({
    dataquery,
    datasource,
    identifiers,
    periodSelection,
    format: 'csv',
    locale,
  });

  const token = yield select(getToken);
  const authHeaders = R.when(
    R.always(!R.isNil(token) && !R.propOr(false, 'isExternal', datasource)),
    R.assoc('Authorization', `Bearer ${token}`),
  )(headers);
  const response = yield call(axios.get, url, {
    headers: authHeaders,
    params,
  });
  yield call(downloadFile, { response, filename });
  yield put({ type: DOWNLOAD_METADATA_SUCCESS });
}

export function* watchRequestMetadata() {
  yield takeLatest(TOGGLE_METADATA, requestMetadataWorker);
  yield takeLatest(DOWNLOAD_METADATA, downloadMetadataWorker);
}
