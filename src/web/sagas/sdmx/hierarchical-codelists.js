import axios from 'axios';
import * as R from 'ramda';
import { all, call, select } from 'redux-saga/effects';
import { getRequestArgs } from '@sis-cc/dotstatsuite-sdmxjs';
import { rules2 } from '@sis-cc/dotstatsuite-components';
import { getRawStructureRequestArgs } from '../../selectors/sdmx';

const requestCodelist = (reference, datasource) => {
  const { agencyId, code, version, hierarchy, codelistId } = reference;
  const { url, headers, params } = getRequestArgs({
    identifiers: { agencyId, code, version },
    datasource,
    type: 'hierarchicalcodelist',
  });

  return axios
    .get(url, { headers, params })
    .then(res => res.data)
    .then(json => {
      const parsed = rules2.parseHierarchicalCodelist(json, hierarchy);
      return { codelistId, hierarchy: parsed };
    })
    .catch(R.always({}));
};

export function* requestHierarchicalCodelists(references) {
  const { datasource } = yield select(getRawStructureRequestArgs);
  const hierarchies = yield all(
    R.map(ref => call(requestCodelist, ref, datasource), R.values(references)),
  );
  const results = R.reduce(
    (acc, { codelistId, hierarchy }) => {
      if (R.isNil(codelistId)) {
        return acc;
      }
      return R.assoc(codelistId, hierarchy, acc);
    },
    {},
    hierarchies,
  );
  return results;
}
