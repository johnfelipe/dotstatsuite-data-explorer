import * as R from 'ramda';
import { push, replace } from 'connected-react-router';
import {
  DOWNLOAD_EXCEL_SUCCESS,
  SHARE_SUCCESS,
  DOWNLOAD_PNG_SUCCESS,
  CHANGE_LAYOUT,
  CHANGE_VIEWER,
} from './ducks/vis';
import {
  requestData,
  REQUEST_SEARCH_DATAFILE,
  REQUEST_VIS_DATAFILE,
  HANDLE_STRUCTURE,
  REQUEST_DATA_PROPS,
} from './ducks/sdmx';
import {
  CHANGE_CONSTRAINTS,
  CHANGE_TERM,
  requestSearch,
  REQUEST_SEARCH_PROPS,
} from './ducks/search';
import { fromStateToSearch } from './utils/router';
import {
  getLocationState,
  getDataflow,
  getPathname,
  getLocale,
  getTerm,
  getViewer,
  getConstraints,
} from './selectors/router';
import { getDataflowName } from './selectors/sdmx';
import { getVisDataDimensions } from './selectors';
import { joinDataflowIds, joinConstraint, sendEvent } from './utils/analytics';
import { compactLayout } from './lib/layout';
import { CHANGE_LOCALE } from './ducks/app';
import { INTERACTIVE_USER_SIGNIN } from './ducks/user';
import { getUser } from './selectors/app';

const isDev = process.env.NODE_ENV === 'development';

// you can push and replace in the history but "pushHistory" will be taken first compared to "replaceHistory"
const historyActions = [
  ['pushHistory', push],
  ['replaceHistory', replace],
];
export const historyMiddleware = store => next => action => {
  const historyAction = R.find(
    R.pipe(R.head, R.flip(R.prop)(action), R.isNil, R.not),
    historyActions,
  );
  if (R.isNil(historyAction)) return next(action);
  const [key, method] = historyAction;
  const actionHistory = R.propOr({}, key, action);
  const prevState = getLocationState(store.getState());
  const pathname = R.defaultTo(getPathname(store.getState()), actionHistory.pathname);
  const state = R.ifElse(
    R.isNil,
    // reset if no payload with invariant id present (see app duck)
    R.always(R.pick(['locale', 'hasAccessibility', 'tenant'], prevState)),
    R.mergeRight(prevState),
  )(actionHistory.payload);

  // fromStateToSearch is an evolution centered on url
  // the following evolution relies on selectors (more generally on things in state)
  const nextState = R.evolve({
    layout: compactLayout(getVisDataDimensions()(store.getState())),
  })(state);

  store.dispatch(
    method({
      pathname,
      state,
      search: fromStateToSearch(nextState),
    }),
  );

  // eslint-disable-next-line no-console
  if (isDev) console.info(`[MDL] history ${action.type} -> ${pathname}`);

  return next(action);
};

export const requestMiddleware = store => next => action => {
  const nextAction = next(action);

  const request = R.prop('request', action);

  if (R.isNil(request)) return nextAction;

  switch (request) {
    case 'getSearch':
      store.dispatch(requestSearch());
      break;
    case 'getData':
      store.dispatch(requestData());
      break;
    case 'getStructure':
      store.dispatch(requestData({ shouldRequestStructure: true }));
      break;
    default:
      // eslint-disable-next-line no-console
      if (isDev) console.log(`[MDL] request unknown request ${request}`);
      return nextAction;
  }

  // eslint-disable-next-line no-console
  if (isDev) console.info(`[MDL] request ${action.type} -> ${request}`);

  return nextAction;
};

const LOCATION_CHANGE = '@@router/LOCATION_CHANGE';
const isLocationChange = R.propEq('type', LOCATION_CHANGE);
const isPopAction = R.pathEq(['payload', 'action'], 'POP');
const isPushAction = R.pathEq(['payload', 'action'], 'PUSH');
const isFirstRendering = R.path(['payload', 'isFirstRendering']);
const propsEq = (props, state) => R.whereEq(R.pick(props, state));
export const browserNavigationMiddleware = store => next => action => {
  if (R.not(isLocationChange(action))) return next(action);
  if (isPushAction(action)) return next(action);

  const prevPathname = getPathname(store.getState());
  const prevLocationState = getLocationState(store.getState());
  const future = next(action); // future to get search state
  const locationState = getLocationState(store.getState());
  const pathname = getPathname(store.getState());
  switch (pathname) {
    case '/':
      if (
        !isFirstRendering(action) &&
        propsEq(REQUEST_SEARCH_PROPS, locationState)(prevLocationState) &&
        !isPopAction(action)
      )
        return future;
      if (
        !isFirstRendering(action) &&
        propsEq(REQUEST_SEARCH_PROPS, locationState)(prevLocationState)
      )
        return future;
      // eslint-disable-next-line no-console
      if (isDev) console.info(`[MDL] browserNavigation ${LOCATION_CHANGE} -> getSearch`);
      store.dispatch(requestSearch());
      break;
    case '/vis':
      if (prevPathname !== pathname) return future;
      if (propsEq(REQUEST_DATA_PROPS, locationState)(prevLocationState)) return future;
      // eslint-disable-next-line no-console
      if (isDev) console.info(`[MDL] browserNavigation ${LOCATION_CHANGE} -> getData`);
      store.dispatch(requestData());
      break;
    default:
      // eslint-disable-next-line no-console
      if (isDev) console.log(`[MDL] browserNavigation pathname ${pathname}`);
      return future;
  }

  return future;
};

const analyticsActions = new Set([
  REQUEST_SEARCH_DATAFILE,
  REQUEST_VIS_DATAFILE,
  DOWNLOAD_EXCEL_SUCCESS,
  SHARE_SUCCESS,
  HANDLE_STRUCTURE,
  DOWNLOAD_PNG_SUCCESS,
  CHANGE_LOCALE,
  CHANGE_TERM,
  CHANGE_LAYOUT,
  CHANGE_VIEWER,
  CHANGE_CONSTRAINTS,
  INTERACTIVE_USER_SIGNIN,
]);

export const analyticsMiddleware = ({ getState }) => next => action => {
  if (!analyticsActions.has(action.type)) return next(action);

  const future = next(action); // need to be after for HANDLE_STRUCTURE

  // eslint-disable-next-line no-console
  if (isDev) console.info(`[MDL] analytics ${action.type}`);

  const { payload = {}, pushHistory = {} } = action;

  const getLabel = dataflow =>
    R.pipe(
      R.defaultTo(getDataflow(getState())),
      R.converge((name, ids) => R.join(' ', R.isNil(name) ? [ids] : [name, ids]), [
        R.propOr(getDataflowName(getState()), 'name'),
        joinDataflowIds,
      ]),
    )(dataflow);

  let dataLayer;
  switch (action.type) {
    case REQUEST_SEARCH_DATAFILE:
      dataLayer = {
        category: 'DOWNLOAD',
        action: payload.isDownloadAllData ? 'CSV_FULL' : 'CSV',
        label: getLabel(payload.dataflow),
        event: 'downloadCsv',
        customCategory: 'engagement',
        customAction: 'downloadContent',
        customLabel: 'csv',
        dataSetCode: getLabel(payload.dataflow),
      };
      break;
    case REQUEST_VIS_DATAFILE:
      dataLayer = {
        category: 'DOWNLOAD',
        action: payload.isDownloadAllData ? 'CSV_FULL' : 'CSV',
        label: getLabel(),
        event: 'downloadCsv',
        customCategory: 'engagement',
        customAction: 'downloadContent',
        customLabel: 'csv',
        dataSetCode: getLabel(),
      };
      break;
    case DOWNLOAD_EXCEL_SUCCESS:
      dataLayer = {
        category: 'DOWNLOAD',
        action: 'EXCEL',
        label: getLabel(),
        event: 'downloadExcel',
        customCategory: 'engagement',
        customAction: 'downloadContent',
        customLabel: 'excel',
        dataSetCode: getLabel(),
      };
      break;
    case DOWNLOAD_PNG_SUCCESS:
      dataLayer = {
        category: 'DOWNLOAD',
        action: 'PNG',
        label: getLabel(),
        event: 'downloadPng',
        customCategory: 'engagement',
        customAction: 'downloadContent',
        customLabel: 'png',
        dataSetCode: getLabel(),
      };
      break;
    case SHARE_SUCCESS:
      dataLayer = {
        category: 'SHARE',
        action: `SHARED_${R.toUpper(getViewer(getState()))}`,
        label: getLabel(),
        event: 'share',
        customCategory: 'engagement',
        customAction: 'share',
        customLabel: getLabel(),
        dataSetCode: getLabel(),
      };
      break;
    case HANDLE_STRUCTURE:
      dataLayer = {
        category: 'DATAFLOW',
        action: 'VIEWED_DATAFLOW',
        label: getLabel(),
        event: 'load_dataflow',
        customCategory: 'navigation',
        customAction: 'viewDataflow',
        customLabel: getLabel(),
        dataSetCode: getLabel(),
        nonInteraction: true,
      };
      break;
    case INTERACTIVE_USER_SIGNIN:
      dataLayer = {
        isGTM: true,
        event: 'login',
        customCategory: 'navigation',
        customAction: 'connection',
        customLabel: true,
      };
      break;
    case CHANGE_CONSTRAINTS:
      if (R.pipe(R.keys, R.length)(getConstraints(getState())) > 1) return;
      dataLayer = {
        isGTM: true,
        event: 'change_constraints',
        customCategory: 'navigation',
        customAction: 'browseBy',
        customLabel: joinConstraint(getConstraints(getState())),
      };
      break;
    case CHANGE_TERM:
      dataLayer = {
        isGTM: true,
        event: 'change_term',
        customCategory: 'navigation',
        customAction: 'internalSearchTerm',
        customLabel: getTerm(getState()),
      };
      break;
    case CHANGE_LOCALE:
      dataLayer = {
        isGTM: true,
        locale: getLocale(getState()),
      };
      break;
    case CHANGE_LAYOUT:
      if (R.isEmpty(pushHistory)) return;
      dataLayer = {
        isGTM: true,
        event: 'change_layout',
        customCategory: 'navigation',
        customAction: 'applyTableLayout',
        customLabel: getLabel(),
      };
      break;
    case CHANGE_VIEWER:
      dataLayer = {
        isGTM: true,
        event: 'change_viewer',
        customCategory: 'navigation',
        customAction: 'changeViewer',
        customLabel: getViewer(getState()),
        dataSetCode: getLabel(),
      };
      break;
    default:
      // eslint-disable-next-line no-console
      if (isDev) console.log(`[MDL] analytics unknown type ${action.type}`);
      return future;
  }
  sendEvent({ dataLayer: R.assoc('isLogged', !!getUser(getState()), dataLayer) });
  return future;
};
