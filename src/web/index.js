import '@babel/polyfill';
import 'fastestsmallesttextencoderdecoder';
import { Provider } from 'react-redux';
import React from 'react';
import ReactDOM from 'react-dom';
import { ConnectedRouter } from 'connected-react-router';
import * as R from 'ramda';
import CssBaseline from '@material-ui/core/CssBaseline';
import { getLocale } from './selectors/router';
import configureStore, { history } from './configureStore';
import { I18nProvider, initialize as initializeI18n } from './i18n';
import { initialize as initializeAnalytics, getToken } from './utils/analytics';
import { ThemeProvider } from './theme';
import searchApi from './api/search';
import * as Settings from './lib/settings';
import ErrorBoundary from './components/error';
import Helmet from './components/helmet';
import App from './components/app';
import meta from '../../package.json';
import { AuthPopUp as Auth } from './lib/oidc/auth';
import { OidcProvider } from './lib/oidc';
import { userSignedIn, userSignedOut, refreshToken } from './ducks/app';
import { getIsFirstRendering } from './selectors/app';

console.info(`${meta.name}@${meta.version}`); // eslint-disable-line no-console

const initAuth = (oidc = {}) => {
  if (R.isEmpty(oidc)) return;

  return Auth({
    authority: oidc.authority,
    client_id: oidc.client_id,
    redirect_uri: `${window.location.origin}/signed`,
    scope: 'openid email offline_access',
    autoRefresh: true,
  });
};

const run = async () => {
  const initialState = {};

  const token = getToken({
    gaToken: R.path(['CONFIG', 'gaToken'], window),
    gtmToken: R.path(['CONFIG', 'gtmToken'], window),
  });

  const store = configureStore(initialState, { hasToken: !R.isEmpty(token.id) });
  const auth = initAuth(window.CONFIG?.member?.scope?.oidc, store);
  const locale = getLocale(store.getState());

  searchApi.setConfig(Settings.search);
  initializeAnalytics({ token, dataLayer: { locale } });
  initializeI18n(Settings.i18n, locale);

  const render = () => {
    if (!getIsFirstRendering(store.getState())) return;

    ReactDOM.render(
      <ErrorBoundary isFinal>
        <Provider store={store}>
          <OidcProvider value={auth}>
            <ThemeProvider theme={Settings.theme}>
              <I18nProvider messages={window.I18N}>
                <ErrorBoundary>
                  <Helmet />
                  <CssBaseline />
                  <ConnectedRouter history={history}>
                    <App />
                  </ConnectedRouter>
                </ErrorBoundary>
              </I18nProvider>
            </ThemeProvider>
          </OidcProvider>
        </Provider>
      </ErrorBoundary>,
      document.getElementById('root'),
    );
  };

  if (auth) {
    auth.on('signedIn', ({ access_token, payload }) => {
      const user = {
        given_name: payload.given_name,
        family_name: payload.family_name,
        email: payload.email,
      };
      store.dispatch(userSignedIn(access_token, user));
      render();
    });
    auth.on('tokensRefreshed', ({ access_token }) => {
      store.dispatch(refreshToken(access_token));
    });
    auth.on('signedOut', () => {
      store.dispatch(userSignedOut());
    });
    auth.on('error', event => {
      console.log('error', event); // eslint-disable-line no-console
      render();
    });

    await auth.init();

    window.addEventListener(
      'message',
      event => {
        if (event.origin !== window.location.origin) return;
        if (event.data?.type === 'signed') {
          auth.getAccessTokenFromCode(event.data?.code);
        }
        if (event.data?.type === 'login_required') {
          render();
        }
      },
      false,
    );

    if (window.location.pathname === '/signed') {
      const searchParams = new URLSearchParams(window.location.search);
      const error = searchParams.get('error');
      const type = error ? error : 'signed';
      const code = searchParams.get('code');
      if (window.opener) {
        // popup
        window.opener.postMessage({ type, code }, window.location.origin);
        window.close();
      } else {
        // iframe
        window.parent.postMessage({ type, code }, window.location.origin);
      }

      return;
    }

    const _frame = window.document.createElement('iframe');

    // shotgun approach
    _frame.style.visibility = 'hidden';
    _frame.style.position = 'absolute';
    _frame.width = 0;
    _frame.height = 0;

    window.document.body.appendChild(_frame);

    _frame.src = `${auth.authorizationUrl}&prompt=none`;
  } else {
    render();
  }
};

run();
