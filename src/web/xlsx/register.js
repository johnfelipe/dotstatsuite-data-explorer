import * as R from 'ramda';
import { formatCellValue, formatFlags, getPosition } from './utils';
import { SHEET1, SHEET2 } from './constants';

const addStyle = (workbook, position, style) => {
  workbook
    .sheet(0)
    .cell(position)
    .style(style);

  workbook
    .sheet(1)
    .cell(position)
    .style(style);
};
const getNumberFormat = (value = '') =>
  R.pipe(
    R.toString,
    R.split('.'),
    R.nth(1),
    R.ifElse(R.isNil, R.always([]), R.identity),
    decimals => {
      if (R.isEmpty(decimals)) return '';
      return R.reduce(acc => R.concat(acc, '0'), '.')(decimals);
    },
    R.concat('#,##0'),
  )(value);

export const registerCell = (
  workbook,
  position,
  cell = {},
  style = {},
  sheetIndex = 0,
  isFit = true,
) => {
  addStyle(workbook, position, style);

  workbook
    .sheet(sheetIndex)
    .cell(position)
    .value(formatCellValue(cell))
    .style('numberFormat', getNumberFormat(R.propOr('', 'intValue')(cell)));
  workbook.sheet(sheetIndex).cell(position).contentShouldBeFitToCell = isFit;
  if (!R.isEmpty(R.propOr([], 'flags', cell))) {
    workbook
      .sheet(1)
      .cell(position)
      .value(formatFlags(cell.flags))
      .hyperlink(`'${SHEET1}'!${position}`);
    workbook
      .sheet(0)
      .cell(position)
      .style({ ...style, underline: true })
      .hyperlink(`'${SHEET2}'!${position}`)
      .value(
        R.ifElse(
          v => R.or(R.isNil(v), R.isEmpty(v)),
          R.always('*'),
          R.identity,
        )(
          workbook
            .sheet(sheetIndex)
            .cell(position)
            .value(),
        ),
      );
  }
};

export const registerRange = (
  workbook,
  startPosition,
  endPosition,
  cell = {},
  style = {},
  sheetIndex = 0,
  isFit = true,
) => {
  workbook
    .sheet(0)
    .range(`${startPosition}:${endPosition}`)
    .merged(true)
    .style(style);
  workbook
    .sheet(1)
    .range(`${startPosition}:${endPosition}`)
    .merged(true)
    .style(style);

  workbook
    .sheet(sheetIndex)
    .range(`${startPosition}:${endPosition}`)
    .merged(true)
    .value(formatCellValue(cell))
    .style('numberFormat', getNumberFormat(R.propOr('', 'intValue')(cell)));
  workbook.sheet(sheetIndex).cell(startPosition).contentShouldBeFitToCell = isFit;
  if (!R.isEmpty(R.propOr([], 'flags', cell))) {
    // need to place hyperlink first
    workbook
      .sheet(1)
      .cell(endPosition)
      .hyperlink(`'${SHEET1}'!${endPosition}`);

    workbook
      .sheet(1)
      .range(`${startPosition}:${endPosition}`)
      .merged(true)
      .value(formatFlags(cell.flags));

    // need to place hyperlink first
    workbook
      .sheet(0)
      .cell(endPosition)
      .hyperlink(`'${SHEET2}'!${endPosition}`);

    workbook
      .sheet(0)
      .range(`${startPosition}:${endPosition}`)
      .merged(true)
      .style({ ...style, underline: true })
      .value(
        R.ifElse(
          R.isNil,
          R.always('*'),
          R.identity,
        )(
          workbook
            .sheet(sheetIndex)
            .cell(startPosition)
            .value(),
        ),
      );
  }
};

export const registerSequenceInColumn = (
  workbook,
  row,
  col,
  values,
  style = {},
  sheet = 0,
  isFit,
) => {
  let index = 0;
  R.forEach(value => {
    const cellCount = R.propOr(1, 'cellCount', value);
    if (cellCount === 1) {
      registerCell(workbook, getPosition(row, col + index), value, style, sheet, isFit);
    } else {
      registerRange(
        workbook,
        getPosition(row, col + index),
        getPosition(row, col + index + cellCount - 1),
        value,
        style,
        sheet,
        isFit,
      );
    }
    index += cellCount;
  }, values);
};

export const registerSequenceInRow = (workbook, row, col, values, style = {}, sheet = 0, isFit) => {
  R.addIndex(R.forEach)((value, index) => {
    registerCell(workbook, getPosition(row + index, col), value, style, sheet, isFit);
  }, values);
};
