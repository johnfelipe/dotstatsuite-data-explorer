import { shouldPolyfill } from '@formatjs/intl-pluralrules/should-polyfill';

export default async locale => {
  if (shouldPolyfill()) {
    // Load the polyfill 1st BEFORE loading data
    await import('@formatjs/intl-pluralrules/polyfill');
  }

  if (Intl.PluralRules.polyfilled) {
    switch (locale) {
      default:
        await import('@formatjs/intl-pluralrules/locale-data/en');
        break;
      case 'ar':
        await import('@formatjs/intl-pluralrules/locale-data/ar');
        break;
      case 'de':
        await import('@formatjs/intl-pluralrules/locale-data/de');
        break;
      case 'es':
        await import('@formatjs/intl-pluralrules/locale-data/es');
        break;
      case 'fr':
        await import('@formatjs/intl-pluralrules/locale-data/fr');
        break;
      case 'it':
        await import('@formatjs/intl-pluralrules/locale-data/it');
        break;
      case 'km':
        await import('@formatjs/intl-pluralrules/locale-data/km');
        break;
      case 'nl':
        await import('@formatjs/intl-pluralrules/locale-data/nl');
        break;
      case 'th':
        await import('@formatjs/intl-pluralrules/locale-data/th');
        break;
    }
  }
};
