import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { IntlProvider } from 'react-intl';
import pluralPolyfill from './pluralPolyfill';
import { getLocale } from '../selectors/router';

const Provider = ({ localeId, messages, children }) => {
  pluralPolyfill(localeId);
  return (
    <IntlProvider locale={localeId} key={localeId} messages={messages[localeId]}>
      {React.Children.only(children)}
    </IntlProvider>
  );
};

Provider.propTypes = {
  localeId: PropTypes.string,
  messages: PropTypes.object,
  children: PropTypes.element.isRequired,
};

export default connect(createStructuredSelector({ localeId: getLocale }))(Provider);
