import * as R from 'ramda';

export const cleanDeadBranches = (flatlist, accessor = R.prop('isEnabled')) => {
  const getHId = v => R.prop('hierarchicalId', v) || R.prop('id', v);
  if (R.find(R.pipe(accessor, R.equals(true)))(flatlist)) {
    const grouped = R.groupBy(R.propOr('#ROOT', 'parentId'), flatlist);

    const filterEnabled = R.filter(v => {
      if (accessor(v)) {
        return true;
      }
      const id = getHId(v);
      const children = R.propOr([], id, grouped);
      const enabledChildren = filterEnabled(children);
      return !R.isEmpty(enabledChildren);
    });

    return filterEnabled(flatlist);
  }
  if (R.find(R.pipe(accessor, R.equals(false)))(flatlist)) return [];
  return flatlist;
};
