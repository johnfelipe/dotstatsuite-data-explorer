import { getHighlightedConstraints } from '../';

describe('getHighlightedConstraints', () => {
  test('perfect match - add create highlightedContraints', () => {
    const highlights = [
      ['dimensionLabel1', ['<em>valueLabel1</em>']],
      ['dimensionLabel2', ['<em>should</em> not appear']],
      ['dimensionLabel3', ['(<em>valueLabel3</em>)']],
      ['dimensionLabel4', ['<em>value</em>, <em>Label4</em>']],
      ['dimensionLabel5', ['<em>어이기</em>']],
      ['dimensionLabel6', ['[<em>어이기</em>]']],
      ['dimensionLabel7', ['어이기']],
      ['dimensionLabel8', ['<em>firstvalueLabel only</em>', '<em>lastValueLabel</em>']],
    ];
    const highlightedContraints = {
      dimensionLabel1: 'valueLabel1',
      dimensionLabel3: '(valueLabel3)',
      dimensionLabel4: 'value, Label4',
      dimensionLabel5: '어이기',
      dimensionLabel6: '[어이기]',
      dimensionLabel8: 'firstvalueLabel only',
    };
    expect(getHighlightedConstraints([])).toEqual({});
    expect(getHighlightedConstraints(highlights)).toEqual(highlightedContraints);
  });
});
