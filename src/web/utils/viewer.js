import { mapProps } from 'recompose';
import * as R from 'ramda';
import * as Settings from '../lib/settings';

export const getShareConfig = ({
  mode,
  sdmxUrl,
  requestArgs,
  localeId,
  dataflow,
  isTimeInverted,
  layoutIds,
  map,
  display,
}) => {
  return R.when(
    R.always(mode === 'latest'),
    R.merge({
      sdmxSource: R.dissoc('locale', requestArgs),
      sdmxUrl,
      display,
      customAttributes: Settings.customAttributes,
      units: Settings.units,
      unitDimension: { id: Settings.units.id },
      chart: { map },
      table: {
        isTimeInverted,
        layoutIds,
        limit: Settings.cellsLimit,
      },
    }),
  )({ dataflowId: dataflow.id, locale: localeId });
};

export const getShareData = (viewerProps, mode, config) => {
  const { isDefaultTitle, isDefaultSubtitle, isDefaultSourceLabel } = viewerProps;

  const title = isDefaultTitle ? null : R.path(['headerProps', 'title', 'label'], viewerProps);
  const subtitle = isDefaultSubtitle
    ? null
    : R.path(['headerProps', 'subtitle', 0, 'label'], viewerProps);
  const sourceLabel = isDefaultSourceLabel
    ? null
    : R.path(['footerProps', 'source', 'label'], viewerProps);

  const chartConfig = R.pathOr({}, ['chartData', 'share'], viewerProps);

  return R.pipe(
    R.pick([
      'chartData',
      'chartOptions',
      'tableProps',
      'headerProps',
      'footerProps',
      'hasAccessibility',
      'isRtl',
      'timeFormats',
      'type',
    ]),
    R.merge({ mode, config }),
    R.assocPath(['config', 'informations'], { title, subtitle, sourceLabel }),
    R.over(R.lensPath(['config', 'chart']), R.merge(chartConfig)),
    R.set(R.lensPath(['config', 'unitDimension']), R.path(['units', 'unitDimension'], viewerProps)),
    R.assocPath(['config', 'timeFormats'], R.propOr({}, 'timeFormats', viewerProps)),
    R.evolve({
      footerProps: R.pipe(
        R.when(R.prop('copyright'), R.assoc('withCopyright', true)),
        R.dissoc('copyright'),
      ),
    }),
    R.ifElse(
      R.propEq('type', 'table'),
      R.omit(['chartData', 'chartOptions']),
      R.pipe(R.dissoc('tableProps'), R.dissocPath(['config', 'table'])),
    ),
    R.ifElse(
      R.propEq('mode', 'latest'),
      R.omit(['chartData', 'tableProps']),
      R.evolve({ config: R.omit(['chart', 'informations']) }),
    ),
  )(viewerProps);
};

const getIsDefaultInformations = properties =>
  R.pipe(
    R.props(['title', 'subtitle', 'source']),
    R.map(prop => (R.isNil(prop) ? true : R.prop('isDefault', prop))),
    ([isDefaultTitle, isDefaultSubtitle, isDefaultSourceLabel]) => ({
      isDefaultTitle,
      isDefaultSubtitle,
      isDefaultSourceLabel,
    }),
  )(properties);

export const withVisDataProps = mapProps(
  ({
    chartData,
    chartOptions,
    errors,
    footerProps,
    headerProps,
    isLoadingData,
    isLoadingMap,
    isNonIdealState,
    isNonIdealMapState,
    isRtl,
    locale,
    properties,
    share,
    tableProps,
    timeFormats,
    type,
    units,
    hasSelection,
    hasAccessibility,
    onChange,
    visPageWidth,
  }) => {
    const viewerProps = {
      chartData,
      chartOptions,
      tableProps,
      getAxisOptions: onChange,
      getResponsiveSize: onChange,
      footerProps,
      headerProps,
      hasAccessibility,
      isNonIdealState,
      isNonIdealMapState,
      isRtl,
      locale,
      timeFormats,
      type,
      units,
      ...getIsDefaultInformations(properties),
    };

    return {
      errors,
      isLoadingData,
      isLoadingMap,
      toolsProps: {
        isRtl,
        properties,
        share: () => share(viewerProps),
      },
      viewerProps,
      hasSelection,
      hasAccessibility,
      visPageWidth,
    };
  },
);
