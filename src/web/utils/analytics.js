import ReactGA from 'react-ga';
import TagManager from 'react-gtm-module';
import * as R from 'ramda';

const isDev = process.env.NODE_ENV === 'development';
const GA = 'GA';
const GTM = 'GTM';

export const getToken = ({ gaToken, gtmToken }) => {
  if (!R.isEmpty(gtmToken)) return { id: gtmToken, type: GTM };
  if (R.isEmpty(gaToken)) return { id: '' };
  return { id: gaToken, type: GA };
};
//---------------------------------------------------------------------------------------initialize
export const initialize = ({ token, dataLayer }) => {
  switch (token.type) {
    case GTM:
      if (process.env.NODE_ENV !== 'production') {
        // eslint-disable-next-line no-console
        console.info(`google-tag-manager initialized with ${token.id}`);
      }
      TagManager.initialize({
        gtmId: token.id,
        dataLayer: {
          siteName: 'data-explorer',
          pageCategory: 'statistics',
          ...dataLayer,
        },
      });
      break;
    case GA:
      if (process.env.NODE_ENV !== 'production') {
        // eslint-disable-next-line no-console
        console.info(`google-analytics initialized with ${token.id}`);
      }
      ReactGA.initialize(token.id, { debug: isDev });
      break;
  }
};

// ---------------------------------------------------------------------------------------------lib
export const sendEvent = ({ dataLayer }) => {
  if (!R.isEmpty(R.path(['CONFIG', 'gtmToken'], window))) {
    TagManager.dataLayer({
      dataLayer: R.omit(['category', 'action', 'label'], dataLayer),
    });
  }
  if (R.prop('isGTM', dataLayer)) return;
  ReactGA.event(dataLayer);
};
// ----------------------------------------------------------------------------------------------GA
export const joinDataflowIds = R.pipe(
  R.props(['datasourceId', 'dataflowId', 'agencyId', 'version']),
  R.map(R.replace(/@/g, '[at]')),
  R.join('/'),
);

// ---------------------------------------------------------------------------------------------GTM
export const joinConstraint = R.pipe(R.values, R.head, R.values, R.join(' - '));
