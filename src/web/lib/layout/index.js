import * as R from 'ramda';
import {
  getRefAreaDimension,
  getTimePeriodDimension,
  isTimePeriodDimension,
} from '@sis-cc/dotstatsuite-sdmxjs';

const nbValues = 3; // specific rule, is it the number max of dimension values (table preview)

export const defaultLayoutBuilder = dimensions => {
  const flatDimensions = R.values(dimensions);
  const timeId = R.pipe(
    getTimePeriodDimension,
    R.when(R.identity, R.prop('id')),
  )({ dimensions: flatDimensions });
  const areaId = R.pipe(
    getRefAreaDimension,
    R.when(R.identity, R.prop('id')),
  )({ dimensions: flatDimensions });
  const helper = (omitIds, pickId) =>
    R.pipe(
      R.omit(omitIds),
      R.ifElse(R.has(pickId), R.prop(pickId), R.pipe(R.values, R.head)),
      R.prop('id'),
    );
  const x = helper([], areaId)(dimensions);
  const y = helper([areaId, x], timeId)(dimensions);
  const z = R.pipe(R.omit([x, y]), R.values, R.pluck('id'))(dimensions);

  return {
    rows: R.ifElse(R.isNil, R.always([]), R.flip(R.append)([]))(x),
    header: R.ifElse(R.isNil, R.always([]), R.flip(R.append)([]))(y),
    sections: z,
  };
};

export const defaultConcat = (array1 = []) => (array2 = []) => R.concat(array2, array1);
export const deleteItem = (key, prop) =>
  R.pipe(
    R.prop(key),
    R.filter(v => R.not(R.equals(v, prop))),
  );
export const getValuesFlat = R.pipe(R.values, R.flatten);

export const compactLayout = ({ many = {} } = {}) => layout => {
  if (R.isNil(layout)) return layout;
  const idsNotInMany = R.difference(getValuesFlat(layout), R.keys(many));
  return R.map(R.flip(R.difference)(idsNotInMany))(layout);
};

export const adjustment = R.curry((notAvailableIds, layout) => {
  const withoutNotAvailableIds = R.flip(R.difference)(notAvailableIds);
  if (R.pipe(R.prop('rows'), withoutNotAvailableIds, R.isEmpty)(layout)) {
    const pivot = R.pipe(R.prop('sections'), withoutNotAvailableIds, R.isEmpty)(layout)
      ? 'header'
      : 'sections';
    const id = R.pipe(R.prop(pivot), withoutNotAvailableIds, R.head)(layout);
    if (R.isNil(id)) return layout;
    return R.pipe(R.assoc('rows', R.concat([id], R.propOr([], 'rows')(layout))), layout => ({
      ...layout,
      [pivot]: deleteItem(pivot, id)(layout),
    }))(layout);
  }
  return layout;
});

export const isInvalid = (dimensionIds, layoutIds, layout) => {
  const hasInvalidIds = layoutIds => R.pipe(R.flip(R.difference)(layoutIds), R.isEmpty, R.not);
  return R.or(
    hasInvalidIds(dimensionIds)(layoutIds),
    R.pipe(R.values, R.flatten, R.isEmpty)(layout),
  );
};
const getValues = id =>
  R.pipe(
    R.take(nbValues),
    R.length,
    R.addIndex(R.times)((index, n) => ({ id: `${id}-${n}`, label: 'Xxxx' })),
  );

export const manyFormat = R.mapObjIndexed((dimension, id) => ({
  id,
  isTimePeriod: isTimePeriodDimension(dimension),
  value: id,
  name: R.prop('name')(dimension),
  values: getValues(id)(R.propOr([], 'values')(dimension)),
}));

export const oneFormat = R.mapObjIndexed((dimension, id) => ({
  id,
  value: id,
  isHidden: true,
}));

export const getDefaultLayout = (many, getMissingIds = []) => {
  const defaultLayout = defaultLayoutBuilder(many);
  const defaultMissingIds = R.pipe(getValuesFlat, getMissingIds)(defaultLayout);
  return R.over(R.lensProp('sections'), defaultConcat(defaultMissingIds))(defaultLayout);
};
export const getLayout = (missingIds, dimensionsIds) =>
  R.pipe(R.over(R.lensProp('sections'), defaultConcat(missingIds)), adjustment(dimensionsIds));
