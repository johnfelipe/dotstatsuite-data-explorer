import * as R from 'ramda';
import C from './constants';

export default R.pipe(
  R.replace(C.FACET_VALUE_MASK, ''),
  R.ifElse(R.pipe(R.match(/<(\/)?em>/), R.isEmpty), R.always(''), R.identity),
  R.split(C.FACET_LEVEL_SEPARATOR),
  R.ifElse(R.pipe(R.length, R.lt(1)), R.tail, R.identity),
  R.join(C.FACET_HIGHLIGHT_SEPARATOR),
);
