import * as R from 'ramda';

export { default as searchParser } from './searchParser';
export { default as configParser } from './configParser';
export { default as searchConstants } from './constants';

/*
 * a facet is irrelevant if:
 * - results count is equal to ALL values count
 */
export const rejectIrrelevantFacets = ({ count }) =>
  R.filter(
    R.pipe(
      R.prop('values'),
      R.any(value => value.count && R.lt(value.count, count)),
    ),
  );
