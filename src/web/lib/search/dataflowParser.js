import * as R from 'ramda';
import highlightParser from './highlightParser';
import C from './constants';

const emTag = /<(\/)?em>/g;

const parseHighlights = R.pipe(
  R.toPairs,
  R.reduce((memo, [field, highlight]) => {
    const items = R.map(highlightParser, highlight);
    if (R.all(R.isEmpty)(items)) return memo;
    return [
      ...memo,
      [
        R.cond([
          [R.equals(C.DATASOURCE_ID), R.always('Id')],
          [R.T, R.identity],
        ])(field),
        items,
      ],
    ];
  }, []),
);

const enhanceDimensions = (dimensions = [], highlights) =>
  R.pipe(R.map(R.replace(emTag, '')), R.difference(dimensions), R.concat(highlights))(highlights);

const getHightlight = key => R.pipe(R.propOr([], key), R.head);

export default options => data => {
  const { id, name, description, dimensions, ...dataflow } = data;
  const highlights = R.propOr({}, id, options.highlighting);

  // if the name is long, the highlight will truncate it
  const highlightedName = R.defaultTo(R.defaultTo(id, name), getHightlight('name')(highlights));

  // if <em> is used in description, false positive
  // description is html, highlighting description may break html
  const descriptionHighlight = getHightlight('description')(highlights);
  const highlightedDescription = R.ifElse(
    R.isNil,
    R.always(description),
    R.pipe(R.replace(emTag, ''), x => R.replace(x, descriptionHighlight, description)),
  )(descriptionHighlight);

  return {
    ...dataflow,
    id,
    name: highlightedName,
    description: highlightedDescription,
    dimensions: enhanceDimensions(dimensions, R.propOr([], 'dimensions')(highlights)),
    highlights: parseHighlights(R.omit(['name', 'description', 'dimensions'], highlights)),
  };
};
