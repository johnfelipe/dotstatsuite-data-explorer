import searchParser from '../searchParser';

describe('searchParser', () => {
  it('should pass (empty)', () => {
    const dataflows = [];
    const facets = {};
    const highlighting = {};
    const numFound = 0;
    expect(searchParser()({ dataflows, facets, highlighting, numFound })).toEqual({
      dataflows,
      facets: [],
      searchResultNb: numFound,
    });
  });
});
