import highlightParser from '../highlightParser';

describe('highlightParser', () => {
  it('should handle flat case', () => {
    expect(highlightParser('<em>industrial</em> machinery and equipment#C332#')).toEqual(
      '<em>industrial</em> machinery and equipment',
    );
  });
  it('should handle truncated flat case', () => {
    expect(highlightParser('<em>industrial</em> machinery and equipment')).toEqual(
      '<em>industrial</em> machinery and equipment',
    );
  });
  it('should handle truncated flat case', () => {
    expect(highlightParser('machinery and equipment#<em>industrial</em>#')).toEqual('');
  });
  it('should handle tree case', () => {
    expect(
      highlightParser('0|Percentage of employment in <em>industry</em>#EMP_PERC_IND#'),
    ).toEqual('Percentage of employment in <em>industry</em>');
  });
  it('should handle deep tree case', () => {
    expect(highlightParser('3|Y4lla#Y1#|Y5lla#Y2#|Y6 <em>lla</em> yalla#Y3#')).toEqual(
      'Y4lla > Y5lla > Y6 <em>lla</em> yalla',
    );
  });
  it('should handle truncated tree case', () => {
    expect(highlightParser('3|Y4lla#Y1#|Y5lla#Y2#|Y6 <em>lla</em>')).toEqual(
      'Y4lla > Y5lla > Y6 <em>lla</em>',
    );
  });
});
