import dataflowParser from '../dataflowParser';

describe('dataflowParser', () => {
  const dataflow = {
    id: 'SIS-CC-stable:DF_DROPOUT_RT',
    datasourceId: 'SIS-CC-stable',
    name: 'Dropout rate',
    dataflowId: 'DF_DROPOUT_RT',
    version: '1.0',
    agencyId: 'MA_545',
    indexationDate: '2020-01-28T11:05:56.155Z',
    dimensions: ['Reference area', 'Measure', 'Valuation', 'Unit of measure', 'Time period'],
  };
  const highlighting = {
    'SIS-CC-stable:DF_DROPOUT_RT': {
      name: ['Dropout <em>rate</em>'],
      Indicator: ['0|Completion <em>rate</em>#EDU_COMPLETION_RT#'],
    },
  };
  const expectedDataflow = {
    id: 'SIS-CC-stable:DF_DROPOUT_RT',
    datasourceId: 'SIS-CC-stable',
    name: 'Dropout <em>rate</em>',
    dataflowId: 'DF_DROPOUT_RT',
    version: '1.0',
    agencyId: 'MA_545',
    indexationDate: '2020-01-28T11:05:56.155Z',
    highlights: [['Indicator', ['Completion <em>rate</em>']]],
    dimensions: ['Reference area', 'Measure', 'Valuation', 'Unit of measure', 'Time period'],
  };
  it('should pass with highlighted name', () => {
    expect(dataflowParser({ highlighting })(dataflow)).toEqual(expectedDataflow);
  });
});
