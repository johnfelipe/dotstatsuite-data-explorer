import * as R from 'ramda';
import {
  sdmxPeriod as defaultSettingsPeriod,
  sdmxPeriodBoundaries,
  defaultFrequency,
} from '../settings';
import {
  applyFormat,
  getDatesFromSdmxPeriod,
  replaceUndefinedDates,
  parseInclusiveDates,
  getAscendentDates,
  getEndOfYear,
  getAjustedDate,
} from './frequency';
import { getDateInTheRange } from '../../utils/date';
import { FILENAME_MAX_LENGTH } from '../../utils/constants';
import { getRequestArgs } from '@sis-cc/dotstatsuite-sdmxjs/lib/getRequestArgs';

export const getFilename = R.pipe(
  R.converge(R.append, [
    R.ifElse(
      R.either(R.pipe(R.prop('dataquery'), R.isNil), R.prop('isFull')),
      R.always(''),
      R.prop('dataquery'),
    ),
    R.pipe(R.prop('identifiers'), R.props(['agencyId', 'code', 'version'])),
  ]),
  R.reject(R.isEmpty),
  R.join('_'),
  R.slice(0, FILENAME_MAX_LENGTH),
);

export const getDefaultRouterParams = ({
  params,
  dataquery,
  timePeriodBoundaries,
  inclusiveBoundaries,
  frequencyArtefact,
  hasDataAvailability,
}) => {
  const frequency = R.pipe(
    R.when(R.isNil, R.always('')),
    R.split('.'),
    R.view(R.lensIndex(R.prop('index')(frequencyArtefact))),
    R.ifElse(R.either(R.isEmpty, R.isNil), R.always(defaultFrequency), R.identity),
  )(dataquery);

  const periodBoundaries = hasDataAvailability
    ? R.pipe(
        replaceUndefinedDates(sdmxPeriodBoundaries),
        parseInclusiveDates(inclusiveBoundaries, frequency),
        getAscendentDates,
      )(timePeriodBoundaries)
    : sdmxPeriodBoundaries;

  const period = R.pipe(
    R.ifElse(
      R.anyPass([R.has('startPeriod'), R.has('endPeriod')]),
      R.converge((start, end) => getDatesFromSdmxPeriod(frequency)([start, end]), [
        R.prop('startPeriod'),
        R.prop('endPeriod'),
      ]),
      R.always(defaultSettingsPeriod),
    ),
    dates => [R.head(dates), R.pipe(R.last, getEndOfYear, getAjustedDate(frequency))(dates)],
    R.map(R.pipe(getDateInTheRange(periodBoundaries), applyFormat(frequency))),
  )(params);

  return {
    ...R.pick(['lastNObservations'], params),
    dataquery,
    period,
    frequency,
  };
};

export const getSelectedIdsIndexed = R.pipe(
  R.indexBy(R.prop('id')),
  R.map(
    R.pipe(
      R.propOr([], 'values'),
      R.reduce((acc, value) => {
        if (R.prop('isSelected', value)) return R.append(R.prop('id', value), acc);
        return acc;
      }, []),
    ),
  ),
);

export const setSelectedDimensionsValues = (dataquery, dimensions) => {
  const splitDataquery = R.split('.', dataquery);
  return R.addIndex(R.map)((dimension, index) => {
    if (R.isEmpty(dimension)) return {};
    const partialDataquery = R.nth(index)(splitDataquery);
    if (R.isNil(partialDataquery)) return {};
    const selectedIds = R.pipe(R.split('+'), ids => new Set(ids))(partialDataquery);
    return R.over(
      R.lensProp('values'),
      R.map(
        R.ifElse(
          R.pipe(R.prop('id'), id => selectedIds.has(id)),
          R.assoc('isSelected', true),
          R.identity,
        ),
      ),
    )(dimension);
  })(dimensions);
};

export const getDefaultSelectionContentConstrained = cc =>
  R.mapObjIndexed((values, dimensionId) => {
    if (R.not(R.has(dimensionId, cc))) return [];
    const dimension = R.prop(dimensionId)(cc);
    return R.filter(id => dimension.has(id))(values);
  });

export const getOnlyHasDataDimensions = R.map(dimension => {
  if (R.either(R.isNil, R.isEmpty, R.prop('isAttribute'))(dimension)) return dimension;
  if (R.propEq('hasData', false)(dimension)) return {};
  return R.over(R.lensProp('values'), R.reject(R.propEq('hasData', false)))(dimension);
});

export const rawDataRequestArgsWrapper = (args, dataquery, params, externalReference, range) => ({
  ...R.when(R.always(R.not(R.isNil(externalReference))), R.mergeLeft(externalReference))(args),
  dataquery,
  params,
  ...(R.path(['datasource', 'hasRangeHeader'], args) ? { range } : {}),
});

export const dataFileRequestArgsWrapper = isFull => (requestArgs, display) => {
  return R.converge(({ url, headers, params }, filename) => ({ url, headers, params, filename }), [
    args =>
      getRequestArgs({
        ...R.omit(R.when(R.always(isFull), R.concat(['dataquery', 'params']))(['range']), args),
        asFile: true,
        format: 'csv',
        type: 'data',
        labels: R.when(R.always(display === 'code'), R.always('code'))('both'),
      }),
    R.pipe(R.assoc('isFull', isFull), getFilename),
  ])(requestArgs);
};
