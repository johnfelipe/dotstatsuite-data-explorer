import { getDescendants } from '../microdata';

describe('Microdata (sdmx lib)', () => {
  it('should getDescendants', () => {
    expect(getDescendants()).toEqual([]);
    expect(
      getDescendants(100, [
        { id: 100 },
        { id: 200 },
        { id: 'A', parentId: 100 },
        { id: 'B', parentId: 100 },
        { id: 'C', parentId: 200 },
        { id: 'X', parentId: 'A' },
      ]),
    ).toEqual(['100', 'A', 'B', 'X']);
    expect(
      getDescendants(100, [
        { id: 200 },
        { id: 'B', parentId: 100 },
        { id: 'X', parentId: 'A' },
        { id: 100 },
        { id: 'A', parentId: 100 },
        { id: 'C', parentId: 200 },
      ]),
    ).toEqual(['100', 'B', 'A', 'X']);
    expect(
      getDescendants(100, [
        { id: 200 },
        { id: 'B', parentId: 1000 },
        { id: 'X', parentId: 'A' },
        { id: 100 },
        { id: 'A', parentId: 1000 },
        { id: 'C', parentId: 200 },
      ]),
    ).toEqual(['100']); // 2 recursive calls when no descendant
  });
});
