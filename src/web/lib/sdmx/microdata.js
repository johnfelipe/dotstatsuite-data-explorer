import { prepareData } from '@sis-cc/dotstatsuite-components/lib/rules/src';
import { getRefinedName } from '@sis-cc/dotstatsuite-sdmxjs';
import * as R from 'ramda';
import { units } from '../settings';

const getObservations = R.path(['dataSets', 0, 'observations']);
const mapObservations = iterator => R.pipe(R.toPairs, R.map(iterator));
const getArtefacts = type => R.path(['structure', type, 'observation']);
const getDimensions = getArtefacts('dimensions');
const getAttributes = getArtefacts('attributes');
const getAnnotationsByType = type =>
  R.converge(R.pipe(R.pickAll, R.values, R.filter(R.propEq('type', type))), [
    R.pathOr([], ['dataSets', 0, 'annotations']),
    R.pathOr([], ['structure', 'annotations']),
  ]);
const getValues = R.prop('values');
const parseKey = R.split(':');
const pick = R.pick(['id', 'name']);
const extractArtefactValues = reducer => R.addIndex(R.reduce)(reducer, {});
const extractArtefactValue = (artefacts, type) => (memo, valueIndex, artefactIndex) => {
  if (R.isNil(valueIndex)) return memo;
  const artefact = R.nth(artefactIndex, artefacts);
  const value = R.nth(valueIndex, getValues(artefact));
  return R.assoc(artefact.id, { type, artefact: pick(artefact), value: pick(value) }, memo);
};
const getValuedArtefacts = ({ useDisplay = true } = {}) =>
  R.reduce((memo, artefact) => {
    if (R.pipe(R.prop('values'), R.isEmpty)(artefact)) return memo;
    if (useDisplay && R.propEq('display', false, artefact)) return memo;
    return R.append(pick(artefact), memo);
  }, []);

const drilldownFork = (hasDrilldownConcepts, drilldownConcepts) =>
  R.ifElse(
    R.always(hasDrilldownConcepts),
    R.pick(R.pipe(R.pluck('title'), R.join(','), R.split(','))(drilldownConcepts)),
    R.identity,
  );

export const prepareTableColumns = (data, customAttributes) => {
  if (R.isNil(data)) return;
  const drilldownConcepts = getAnnotationsByType('DRILLDOWN_CONCEPTS')(data);
  const hasDrilldownConcepts = !R.isEmpty(drilldownConcepts);
  const dimensions = R.pipe(
    R.propOr({}, 'dimensions'),
    R.pick(['one', 'many']),
    R.values,
    R.mergeAll,
    drilldownFork(hasDrilldownConcepts, drilldownConcepts),
    R.values,
  )(prepareData({ data }, customAttributes, units));
  const attributes = R.pipe(
    getAttributes,
    R.indexBy(R.prop('id')),
    drilldownFork(hasDrilldownConcepts, drilldownConcepts),
    R.values,
  )(data);
  return [
    ...getValuedArtefacts({ useDisplay: !hasDrilldownConcepts })(dimensions),
    ...getValuedArtefacts({ useDisplay: !hasDrilldownConcepts })(attributes),
    { id: 'value', align: 'right' },
  ];
};

export const prepareTableRows = data => {
  if (R.isNil(data)) return;
  const observations = getObservations(data);
  const dimensions = getDimensions(data);
  const attributes = getAttributes(data);
  return mapObservations(([key, value]) => {
    return R.mergeAll([
      extractArtefactValues(extractArtefactValue(dimensions, 'dimension'))(parseKey(key)),
      extractArtefactValues(extractArtefactValue(attributes, 'attribute'))(R.tail(value)),
      { value: R.head(value), id: key },
    ]);
  })(observations);
};

export const getDescendants = (refId, flatList = []) => {
  const descendants = {};

  const recurse = list => {
    const nextList = R.reduce(
      (memo, node) => {
        const id = R.prop('id', node);
        const parentId = R.prop('parentId', node);
        if (R.equals(id, refId)) {
          descendants[id] = node;
          return memo;
        }
        if (R.isNil(parentId)) return memo;
        if (R.equals(parentId, refId)) {
          descendants[id] = node;
          return memo;
        }
        if (R.has(parentId, descendants)) {
          descendants[id] = node;
          return memo;
        }
        return R.append(node, memo);
      },
      [],
      list,
    );

    if (R.length(list) === R.length(nextList)) return;
    return recurse(nextList);
  };

  recurse(flatList);

  return R.keys(descendants);
};

export const displayAccessor = ({ display = 'label' } = {}) => (artefact = {}) => {
  const code = R.propOr('', 'id', artefact);
  const label = R.defaultTo('', getRefinedName(artefact));
  if (R.equals(display, 'code')) return code;
  if (R.equals(display, 'label')) return label;
  if (R.isEmpty(code)) return label;
  return `(${code}) ${label}`;
};
