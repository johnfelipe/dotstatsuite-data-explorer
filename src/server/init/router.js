import helmet from 'helmet';
import cors from 'cors';
import compression from 'compression';
import path from 'path';
import express from 'express';
import bodyParser from 'body-parser';
import { healthcheckConnector } from '../services/healthcheck/connector';
import errorHandler from '../middlewares/errors';
import tenant from '../middlewares/tenant';
import ssr from '../ssr';
import { HTTPError } from '../utils/errors';

const checkTenant = (req, res, next) => {
  if (!req.member) return next(new HTTPError(400, 'Tenant required'));
  next();
};

const init = ctx => {
  const app = express();
  const {
    services: { healthcheck },
    configProvider,
  } = ctx;

  app.use(cors({ origin: 'http://localhost' }));
  app.use(bodyParser.json({ limit: '50mb' }));
  app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
  app.get('/robots.txt', (req, res) => res.sendFile(path.resolve(__dirname, '../robots.txt')));
  app.use(compression());
  app.use(express.static(path.join(__dirname, '../../../public')));
  app.use(express.static(path.join(__dirname, '../../../build')));
  app.get('/api/healthcheck', healthcheckConnector(healthcheck));
  app.use(tenant(configProvider));
  app.use(
    helmet.contentSecurityPolicy({
      useDefaults: true,
      directives: {
        'upgrade-insecure-requests': null,
        'default-src': ["'none'"], // fallback to none
        'connect-src': ['*'], // enabling to connect to sfs, nsi, share...
        'script-src': [
          "'self'",
          "'unsafe-inline'", // app don't work
          'www.google-analytics.com',
          'https://www.googletagmanager.com',
        ],
        'style-src': [
          "'self'",
          "'unsafe-inline'", // app don't work
          'https://fonts.googleapis.com',
          'https://www.googletagmanager.com',
        ],
        'font-src': ["'self'", 'data:', 'fonts.gstatic.com'],
        'frame-src': [
          "'self'",
          req => {
            const authority = req?.member?.scope?.oidc?.authority;
            if (!authority) return;
            return new URL(authority).host;
          },
        ],
        'img-src': ['*', 'data:', 'www.googletagmanager.com'],
      },
    }),
  );
  app.use(helmet.hidePoweredBy());
  app.use(helmet.hsts());
  app.use(helmet.noSniff());
  app.use(helmet.referrerPolicy());
  app.use(helmet.xssFilter());
  app.use((req, res, next) => {
    res.setHeader('Permissions-Policy', '*=none');
    next();
  });
  app.use(checkTenant, ssr(ctx));
  app.use(errorHandler);

  return Promise.resolve({ ...ctx, app });
};

module.exports = init;
