import path from 'path';

const config = {
  appId: 'data-explorer',
  env: process.env.NODE_ENV || 'development',
  isProduction: process.env.NODE_ENV === 'production',
  gitHash: process.env.GIT_HASH || 'local',
  publicPath: path.join(__dirname, '../../../public'),
  buildPath: path.join(__dirname, '../../../build'),
  gaToken: process.env.GA_TOKEN || '',
  gtmToken: process.env.GTM_TOKEN || '',
  robotsPolicy: process.env.ROBOTS_POLICY || 'all',
};

module.exports = config;
