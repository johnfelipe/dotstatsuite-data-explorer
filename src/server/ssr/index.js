import '@babel/polyfill';
import { pathOr, zipObj, keys, pipe, pick, map, join } from 'ramda';
import htmlescape from 'htmlescape';
import debug from '../debug';

const renderHtml = ({ config, assets, i18n, settings, stylesheetUrl, app }) => `
    <!doctype html>
    <html lang="en">
      <head>
        <meta charset="utf-8">
        <meta name="robots" content="${config.robotsPolicy}">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        ${join(
          '\n',
          map(link => `<link href="${link}" rel="stylesheet" />`)(settings?.app?.linkTags || []),
        )}
        <link rel="shortcut icon" href="${app.favicon}">
        <link rel="stylesheet" href="/css/preloader.css">
        <style id="insertion-point-jss"></style>
        <style type="text/css">html, body, #root { height: 100%; }</style>
        <link rel="stylesheet" href="${stylesheetUrl}">
        <script> CONFIG = ${htmlescape(config)} </script>
        <script> SETTINGS = ${htmlescape(settings)} </script>
        <script> I18N = ${htmlescape(i18n)} </script>
      </head>
      <body>
        <noscript>
          You need to enable JavaScript to run this app.
        </noscript>
        <div id="root">
          <div class="loader">
            <svg class="circular" viewBox="22 22 44 44">
              <circle class="path" cx="44" cy="44" r="20.2" fill="none" stroke-width="3.6" />
            </svg>
          </div>
        </div>
        <script type="text/javascript" src="${assets.vendors}"></script>
        <script type="text/javascript" src="${assets.main}"></script>
      </body>
    </html>
  `;

const ssr = ({ config, assets, configProvider }) => async (req, res) => {
  const { member } = req;
  const settings = await configProvider.getSettings(member);
  const locales = pipe(pathOr([], ['i18n', 'locales']), keys)(settings);
  const i18n = await configProvider.getI18n(member, locales);

  const html = renderHtml({
    config: {
      ...pick(['env', 'gaToken', 'gtmToken', 'robotsPolicy'], config),
      member,
    },
    i18n: zipObj(locales, i18n),
    assets,
    settings,
    stylesheetUrl: settings.styles,
    app: { favicon: pathOr('/favicon.ico', ['app', 'favicon'], settings) },
  });
  res.set('X-Robots-Tag', config.robotsPolicy);
  res.send(html);
  debug.info(`render site for member '${member.id}' with scope '${member?.scope?.id}'`);
};

export default ssr;
