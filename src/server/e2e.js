require('@babel/register');

const { reduce } = require('ramda');
const initAssets = require('./init/assets');
const initRouter = require('./init/router');
const initHttp = require('./init/http');
const initServices = require('./services');
const initProxyServer = require('./init/proxyServer');

const ressources = [initAssets, initServices, initRouter, initHttp, initProxyServer];
const initRessources = ctx =>
  reduce((acc, initFn) => acc.then(initFn), Promise.resolve(ctx), ressources);

module.exports = config => initRessources(config);
